(:~
 : Page d'administration de la synchronisation entre transcriptions et base de données | Administration page of the synchronization between transcripts and database
 :
 : @author Jean-Christophe Carius, INHA https://www.inha.fr, ? License
 :)

module namespace synchros = 'Corpus-Capta/synchros';

import module namespace app = 'Corpus-Capta/app' at 'app.xqm';
import module namespace config = 'Corpus-Capta/config' at 'config.xqm';
import module namespace transkribus = 'Corpus-Capta/transkribus' at 'transkribus-api.xqm';

import module namespace Session = 'http://basex.org/modules/session';

(:~
 : Page de liste de fichiers à synchroniser | List page of files to synchronize
 : @query-param $collections text collections Transkribus à lister | Transkribus collection to list
 : @query-param $dateMaj  text date de modification minimale des fichiers à lister | minimum modification date of the files to list
 : @query-param $reconnect  int
 : @return page | page
 :)

declare
  %rest:path("CorpusCapta/synchros/{$bdd}")
  %rest:GET
  %rest:query-param("collections", "{$collections}")
  %rest:query-param("date-maj", "{$dateMaj}")
  %rest:query-param("reconnect", "{$reconnect}")
  %output:method('html')
  function synchros:Interface($collections,$bdd,$dateMaj,$reconnect) {

  if($reconnect) then transkribus:ReconnecterTranskribus(Session:get('corpus-capta'))

  else

  <html lang="fr">

    { app:Entetes() }

   <body>

   {
     if(not(app:EstAutorise(Session:get('corpus-capta'),$bdd))) then

  <div class="d-flex position-absolute justify-content-center align-items-center" style="top: 0; left: 0; bottom: 0; right: 0;">
    <div class="alert alert-danger">
      Ce compte n’est pas autorisé à consulter cette page.
    </div>
  </div>

   else

   <div>

      <div class="container" style="max-width: 100%;">

        { app:Menu($bdd,'admin') }

         <div class="d-flex justify-content-center mt-4">

           <form id="form-modifs" class="" method="get" action="">

              <div class="mb-2">

                <label class="d-none">Collection source</label>
                <select name="collections" class="form-control pr-2" style="max-width: 400px;">
                  <option value="" selected="selected">Collections Transkribus ?</option>
            {
              let $liste := transkribus:RequeteTranskribus('https://transkribus.eu/TrpServer/rest/collections/list', true(),Session:get('corpus-capta'))

              return if (count($liste/json/_)) then
                     let $projet := map:get($config:Projets,$bdd)
                     for $c at $p in $liste/json/_
                      let $colId := $c/colId/text()
                      where (array:size($projet) = 0 or (index-of($projet,number($colId)) > 0))
                      order by $c/colName/text()
                      let $texte := $c/colName/text()||' ('||$c/nrOfDocuments/text()||' doc.) '
                      return if(index-of($collections,$colId))
                            then <option selected="selected" value="{$colId}">{$texte}</option>
                            else <option value="{$colId}">{$texte}</option>

            }
              </select>

              {
                let $liste := transkribus:RequeteTranskribus('https://transkribus.eu/TrpServer/rest/collections/list', true(),Session:get('corpus-capta'))

                return <div class=""></div>

              }

              </div>

              <div class="mb-2 text-right">

                <span class="btn btn-sm btn-white text-secondary" onclick="$(this).next().toggleClass('d-none')">date ?</span>

                <div class="d-none">

                <label>Modifications depuis le</label>
                <input type="date" name="date-maj" class="form-control date-maj2 d-inline-block ml-2" style="max-width: 140px;" value="{if($dateMaj) then $dateMaj}" />

                </div>

              </div>

              <div class="mt-3 mb-2 border-top pt-3 d-flex justify-content-between align-items-center">
                <input type="submit" id="btn-submit" class="btn btn-small btn-primary py-1 btn-recherche-modifs" style="" value="→ Lister les données" />

                <a class="ml-3 text-secondary d-none" style="opacity: 0.6" href="/CorpusCapta/{$bdd}/synchros"><small class="d-none">X</small> ràz</a>

              </div>

           </form>

         </div>

        </div>

        <div class="container pb-5">

        { if($collections) then synchros:DonneesCollections($collections, $bdd, $dateMaj) }

        </div>

      </div>

    }

   </body>
</html>

};

declare function synchros:DonneesCollections2($collections,$bdd,$dateMaj){
  <div>
  <p class="d-none">{
    let $colId := tokenize($collections,',')[1]
    let $timestamp := 0
    return 'https://transkribus.eu/TrpServer/rest/actions/list?typeId=1&amp;start='||($timestamp * 1000)||'&amp;collId='||$colId||'&amp;nValues=300'
  }</p>
  <table id="tableur-documents" class="table table-bordered" align="center" style="">
    <thead>
      <tr>
        <th class="pl-1 py-0 align-middle border-0 text-nowrap">Id. Tkb</th>
        <th class="pl-1 py-0 align-middle border-0">Id. bdd</th>
        <th class="pl-1 py-0 align-middle border-0">Titre</th>
        <th class="pl-1 py-0 align-middle border-0 text-right">Date Tkb</th>
        <th class="pl-1 py-0 align-middle border-0 text-right">BDD</th>
        <th class="pl-1 py-0 align-middle border-0 text-right">Qtt</th>
        <th class="pl-1 py-0 align-middle border-0 text-right">Date TEI</th>
        <th class="pl-1 py-0 align-middle border-0 text-right"><button class="btn btn-sm btn-tout-maj">→ Tout màj</button></th>
      </tr>
    </thead>
    <tbody>
    {
      for $colId in tokenize($collections,',')
          let $d1 := if($dateMaj) then xs:dateTime($dateMaj||'T00:00:00-00:00')
          let $d2 := xs:dateTime('1970-01-01T00:00:00-00:00')
          let $timestamp := if($dateMaj) then ($d1 - $d2) div xs:dayTimeDuration('PT1S') else 0
          let $actions := transkribus:RequeteTranskribus('https://transkribus.eu/TrpServer/rest/actions/list?typeId=1&amp;nValues=300&amp;start='||($timestamp * 1000)||'&amp;collId='||$colId, true(),Session:get('corpus-capta'))
          let $listeDocs := transkribus:RequeteTranskribus('https://transkribus.eu/TrpServer/rest/collections/'||$colId||'/list',true(),Session:get('corpus-capta'))
          let $docs := $listeDocs/json
          for $act in $actions//_
              let $docId := $act/docId/text()
              let $colId := $act/colId/text()
              let $dateDoc := $act/time/text()
              let $titre := $act/docName/text()
              let $contributeur := $act/userName/text()
              let $nbrPages := $docs/_[docId = $docId]/nrOfPages/text()
              order by $dateDoc descending
              group by $docId
              let $externalId := $docs/_[docId = $docId]/externalId/text()
              let $nomFichier := if(string-length($externalId)) then $externalId else $colId[1]||'-'||$docId
              (:let $fichier := $config:Capta||'/'||$bdd||'/TEI/'||$nomFichier||'.tei.xml'
              let $dateFichier := if(file:exists($fichier)) then file:last-modified($fichier) else 0
              let $fmtDateFichier := if(file:exists($fichier)) then app:FmtDate($dateFichier) else 'pas de fichier':)

              let $document := if(db:exists($bdd,$nomFichier||'.xml')) then doc($bdd||'/'||$nomFichier||'.xml') else 0
              let $dateFichier := if($document) then data($document//*:TEI/@update-date) else 0
              let $dateFichier := if(matches(string($dateFichier),'\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+')) then $dateFichier else 0
              let $fmtDateFichier := if($dateFichier) then app:FmtDate($dateFichier) else 'pas de fichier'

              let $classeSynchro := if($dateFichier and $dateFichier > $dateDoc[1]) then 'text-success' else 'text-danger'
              return
                <tr>
                  <td class="text-warning">{$docId}</td>
                  <td class="text-info"><a href="{$config:Serveur}/{$bdd}/document/{$nomFichier[1]}">{$nomFichier[1]}</a></td>
                  <td class="">{$titre[1]}</td>

                  <td class="pl-2 text-nowrap date-tkb text-info" data-sort="{$dateDoc[1]}">
                    {app:FmtDate($dateDoc[1])} <span class="d-none"><br/>({$dateDoc[1]})</span>
                    <div class="text-secondary opacity-6 mt-2 border-top pt-1">
                      {$contributeur[1]}
                    </div>
                  </td>

                  <td class="text-nowrap" style="">{$bdd}</td>

                  <td class="text-nowrap text-right" style="">{$nbrPages[1]}</td>

                  <td class="text-nowrap date-fichier {$classeSynchro}" style="">{$fmtDateFichier} <span class="d-none"><br/>({$dateFichier})</span> </td>

                  <td class="text-nowrap"><a target="int" href="{$config:REST}/rest?run=CorpusCapta/synchro.xq&amp;colId={$colId[1]}&amp;docId={$docId[1]}&amp;bdd={$bdd[1]}" class="btn btn-light btn-sm py-0 px-3 text-secondary act-synchroniser position-relative" style="">→ mettre à jour</a></td>

                </tr>
    }
    </tbody>
  </table>
  </div>
};


declare function synchros:DonneesCollections($collections,$bdd,$dateMaj){
  <div>
  <p class="d-none">{
    let $colId := tokenize($collections,',')[1]
    let $timestamp := 0
    return 'https://transkribus.eu/TrpServer/rest/actions/list?typeId=1&amp;start='||($timestamp * 1000)||'&amp;collId='||$colId||'&amp;nValues=300'
  }</p>
  <table id="tableur-documents" class="table table-bordered" align="center" style="">
    <thead>
      <tr>
        <th class="pl-1 py-0 align-middle border-0 text-nowrap">Id. Tkb</th>
        <th class="pl-1 py-0 align-middle border-0">Id. bdd</th>
        <th class="pl-1 py-0 align-middle border-0">Titre</th>
        <th class="pl-1 py-0 align-middle border-0 text-right">Date Tkb</th>
        <th class="pl-1 py-0 align-middle border-0 text-right">BDD</th>
        <th class="pl-1 py-0 align-middle border-0 text-right">Qtt</th>
        <th class="pl-1 py-0 align-middle border-0 text-right">Date TEI</th>
        <th class="pl-1 py-0 align-middle border-0 text-right"><button class="btn btn-sm btn-tout-maj">→ Tout màj</button></th>
      </tr>
    </thead>
    <tbody>
    {
      for $colId in tokenize($collections,',')
          let $d1 := if($dateMaj) then xs:dateTime($dateMaj||'T00:00:00-00:00')
          let $d2 := xs:dateTime('1970-01-01T00:00:00-00:00')
          let $timestamp := if($dateMaj) then ($d1 - $d2) div xs:dayTimeDuration('PT1S') else 0
          let $actions := transkribus:RequeteTranskribus('https://transkribus.eu/TrpServer/rest/actions/list?typeId=1&amp;nValues=1000&amp;start='||($timestamp * 1000)||'&amp;collId='||$colId, true(),Session:get('corpus-capta'))
          let $listeDocs := transkribus:RequeteTranskribus('https://transkribus.eu/TrpServer/rest/collections/'||$colId||'/list',true(),Session:get('corpus-capta'))
          let $docs := $listeDocs/json
          for $doc in $docs/_
              let $docId := $doc/docId/text()
              (:let $colId := $doc//_[position()=1]/colId/text():)

              let $versions := for $a in $actions/json/_[docId = $docId]
                  let $d := $a/time
                  order by $d descending
                  return $a

              let $dateDoc := if(count($versions)) then $versions[1]/time/text() else dateTime(xs:date('2020-03-01'),xs:time('00:00:00-00:00'))

              let $titre := $doc/title/text()
              let $contributeur := if(count($versions)) then $versions[1]/userName/text() else ''
              let $nbrPages := $doc/nrOfPages/text()
              order by $docId descending
              let $externalId := $doc/externalId/text()
              let $nomFichier := if(string-length($externalId)) then $externalId else $colId||'-'||$docId
              (:let $fichier := $config:Capta||'/'||$bdd||'/TEI/'||$nomFichier||'.tei.xml'
              let $dateFichier := if(file:exists($fichier)) then file:last-modified($fichier) else 0
              let $fmtDateFichier := if(file:exists($fichier)) then app:FmtDate($dateFichier) else 'pas de fichier':)

              let $document := if(db:exists($bdd,$nomFichier||'.xml')) then doc($bdd||'/'||$nomFichier||'.xml') else 0
              let $dateFichier := if($document) then data($document//*:TEI/@update-date) else 0
              let $dateFichier := if(matches(string($dateFichier),'\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+')) then $dateFichier else 0
              let $fmtDateFichier := if($dateFichier) then app:FmtDate($dateFichier) else 'pas de fichier'

              let $classeSynchro := if($dateFichier and $dateFichier > $dateDoc[1]) then 'text-success' else 'text-danger'
              return
                <tr id="doc-{$docId}">
                  <td class="text-warning">{$docId}</td>
                  <td class="text-info"><a href="{$config:Serveur}/{$bdd}/document/{$nomFichier[1]}">{$nomFichier[1]}</a></td>
                  <td class="">{$titre[1]}</td>

                  <td class="pl-2 text-nowrap date-tkb text-info" data-sort="{$dateDoc[1]}">
                    {app:FmtDate($dateDoc[1])} <span class="d-none"><br/>({$dateDoc[1]})</span>
                    <div class="text-secondary opacity-6 mt-2 border-top pt-1">
                      {$contributeur[1]}
                    </div>
                  </td>

                  <td class="text-nowrap" style="">{$bdd}</td>

                  <td class="text-nowrap text-right" style="">{$nbrPages[1]}</td>

                  <td class="text-nowrap date-fichier {$classeSynchro}" style="">{$fmtDateFichier} <span class="d-none"><br/>({$dateFichier})</span> </td>

                  <td class="text-nowrap"><a target="int" href="{$config:REST}/rest?run=CorpusCapta/synchro.xq&amp;colId={$colId[1]}&amp;docId={$docId[1]}&amp;bdd={$bdd[1]}" class="btn btn-light btn-sm py-0 px-3 text-secondary act-synchroniser position-relative" style="">→ mettre à jour</a></td>

                </tr>
    }
    </tbody>
  </table>
  </div>
};
