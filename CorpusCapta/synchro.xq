(:~
 : Synchroniser via REST un document Transkribus => Basex | Transkribus => Basex synchronisation of a document via REST
 :
 : @author Jean-Christophe Carius, INHA https://www.inha.fr, License à venir
 :)

import module namespace app = 'Corpus-Capta/app' at 'app.xqm';
import module namespace config = 'Corpus-Capta/config' at 'config.xqm';
import module namespace transkribus = 'Corpus-Capta/transkribus' at 'transkribus-api.xqm';

import module namespace Session = 'http://basex.org/modules/session';

declare option output:method 'html';

declare variable $colId as xs:string external;
declare variable $docId as xs:string external;
declare variable $bdd as xs:string external;

let $mets := transkribus:RequeteTranskribus('https://transkribus.eu/TrpServer/rest/collections/'||$colId||'/'||$docId||'/mets',true(),Session:get('corpus-capta'))

let $id := $mets//trpDocMetadata/externalId/text()
let $id := if(string-length($id)) then $id else $colId||'-'||$docId

return

if(not($mets//trpDocMetadata)) then ((),update:output($colId||'-'||$docId||' introuvable'))

else

  let $xsl := doc($config:Local||'/ressources/xsl/page2tei-0-modif.xsl')

  let $tei := xslt:transform($mets/*:mets,$xsl)

  let $retour := app:FmtDate(data($tei/*:TEI/@update-date))

  return (db:put($bdd,$tei,$id||'.xml',map { 'stripws': false() }), update:output($retour))
