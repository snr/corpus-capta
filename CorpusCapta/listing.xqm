(:~
 : Page listing | Listing page
 :
 : @author Jean-Christophe Carius, INHA https://www.inha.fr, ? License
 :)
module namespace listing = 'Corpus-Capta/listing';

import module namespace config = 'Corpus-Capta/config' at 'config.xqm';
import module namespace app = 'Corpus-Capta/app' at 'app.xqm';

(:~
 : Page listing des documents d'une base de données | Listing page for the douments of a database
 : @param  $bdd : Nom de la base de donnnée | Database name
 : @param  $mode : mode d'affichage (fiches ou liste) | Display mode (cards or list)
 : @return HTML
 :)

declare
  %rest:path("CorpusCapta/listing/{$bdd}")
  %rest:GET
  %rest:query-param("mode", "{$mode}","fiches")
  %output:method('html')
  function listing:Interface($bdd,$mode) {
    let $test := db:exists($bdd)
    let $dateBdd := db:info($bdd)
    let $d1 := $dateBdd/databaseproperties/timestamp/text()
    let $d2 := $dateBdd/resourceproperties/inputdate/text()
    let $captaJson := $config:Local||'/json/'||$bdd||'.json'
    return
    if($test = false()) then 'Base de données introuvable.'
    else if(not(file:exists($captaJson))) then web:redirect($config:Serveur||'/maj-json/'||$bdd)
    else if(file:last-modified($captaJson) < $d1) then web:redirect($config:Serveur||'/maj-json/'||$bdd)
    else
    <html lang="fr">
     { app:Entetes() }
     <body>
       <div class="container" style="max-width: 100%;">
        { app:Menu($bdd,'listing') }
        <div class="d-flex justify-content-center"></div>
       </div>
       <div class="container corpus" style="margin-bottom: 100px; max-width: 100%;">
          <div class="row mx-0">
            <div class="col col-12 px-0">
            <div class="cache-tableau" style="position: absolute; background: #fff; left: 0; right: 0; z-index: 100; margin-top: 4px; ">
              <div class="bg-white position-relative" style="border-radius: 4px;height: 600px; margin-bottom: 100px">
              <div class="chargement-tableau">
                <div class="spinner"></div>
              </div>
            </div>
            </div>
            <div class="d-none filtres">
              <span class="bloc-select bloc-filtre ml-2">
                <select class="filtre custom-select py-1 ml-0 px-1 mb-1 mt-1 test" style="font-size: 12px; height: auto; min-width: 140px; width: auto; max-width: 140px; text-overflow: ellipsis;" data-champ="genre">
                  <option class="raz" value="">Tous les genres</option>
                </select>
              </span>
              <span class="bloc-select bloc-filtre ml-2">
                <select class="filtre custom-select py-1 ml-0 px-1 mb-1 mt-1 test" style="font-size: 12px; height: auto; min-width: 140px; width: auto; max-width: 140px; text-overflow: ellipsis;" data-champ="serie">
                  <option class="raz" value="">Toutes les séries</option>
                </select>
              </span>
              <span class="bloc-select bloc-filtre ml-2">
                <select class="filtre custom-select py-1 ml-0 px-1 mb-1 mt-1 test" style="font-size: 12px; height: auto; min-width: 140px; width: auto; max-width: 140px; text-overflow: ellipsis;" data-champ="auteur">
                  <option class="raz" value="">Tous les auteurs</option>
                </select>
              </span>
              <span class="bloc-select bloc-filtre ml-2">
                <select class="filtre custom-select py-1 ml-0 px-1 mb-1 mt-1 test" style="font-size: 12px; height: auto; min-width: 140px; width: auto; max-width: 140px; text-overflow: ellipsis;" data-champ="langues">
                  <option class="raz" value="">Ttes les langues</option>
                </select>
              </span>
              <div class="btn-maj">
              {
                let $dateBdd := db:info($bdd)
                let $d1 := $dateBdd/databaseproperties/timestamp/text()
                let $d2 := $dateBdd/resourceproperties/inputdate/text()
                let $captaJson := $config:Local||'/json/'||$bdd||'.json'
                let $texte := if(file:exists($captaJson))
                      then (
                          let $d3 := file:last-modified($captaJson)
                          return if($d1 > $d3)
                                 then <span>màj json</span>
                                 else
                                 <span>
                                   <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-braces" viewBox="0 0 16 16">
                                      <path d="M2.114 8.063V7.9c1.005-.102 1.497-.615 1.497-1.6V4.503c0-1.094.39-1.538 1.354-1.538h.273V2h-.376C3.25 2 2.49 2.759 2.49 4.352v1.524c0 1.094-.376 1.456-1.49 1.456v1.299c1.114 0 1.49.362 1.49 1.456v1.524c0 1.593.759 2.352 2.372 2.352h.376v-.964h-.273c-.964 0-1.354-.444-1.354-1.538V9.663c0-.984-.492-1.497-1.497-1.6zM13.886 7.9v.163c-1.005.103-1.497.616-1.497 1.6v1.798c0 1.094-.39 1.538-1.354 1.538h-.273v.964h.376c1.613 0 2.372-.759 2.372-2.352v-1.524c0-1.094.376-1.456 1.49-1.456V7.332c-1.114 0-1.49-.362-1.49-1.456V4.352C13.51 2.759 12.75 2 11.138 2h-.376v.964h.273c.964 0 1.354.444 1.354 1.538V6.3c0 .984.492 1.497 1.497 1.6z"/>
                                    </svg>
                                  </span>
                      )
                      else <span style="text-decoration: line-through;">json introuvable</span>
                return <a class="ml-2 rounded text-secondary text-decoration-none" style="" href="{$config:Serveur}/maj-json/{$bdd}">{$texte}</a>
              }
              </div>
              <div class="mode-vue btn-group">
                <a href="?mode=fiches" class="btn border">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-grid" viewBox="0 0 16 16">
                    <path d="M1 2.5A1.5 1.5 0 0 1 2.5 1h3A1.5 1.5 0 0 1 7 2.5v3A1.5 1.5 0 0 1 5.5 7h-3A1.5 1.5 0 0 1 1 5.5v-3zM2.5 2a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3zm6.5.5A1.5 1.5 0 0 1 10.5 1h3A1.5 1.5 0 0 1 15 2.5v3A1.5 1.5 0 0 1 13.5 7h-3A1.5 1.5 0 0 1 9 5.5v-3zm1.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3zM1 10.5A1.5 1.5 0 0 1 2.5 9h3A1.5 1.5 0 0 1 7 10.5v3A1.5 1.5 0 0 1 5.5 15h-3A1.5 1.5 0 0 1 1 13.5v-3zm1.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3zm6.5.5A1.5 1.5 0 0 1 10.5 9h3a1.5 1.5 0 0 1 1.5 1.5v3a1.5 1.5 0 0 1-1.5 1.5h-3A1.5 1.5 0 0 1 9 13.5v-3zm1.5-.5a.5.5 0 0 0-.5.5v3a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-3a.5.5 0 0 0-.5-.5h-3z"/>
                  </svg>
                </a>
                <a href="?mode=liste" class="btn border">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-list-ul" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm-3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                  </svg>
                </a>
              </div>
              <div class="champ-transcriptions custom-control d-inline-block custom-switch btn-secondary rounded mr-2 ml-2" style="line-height: 1.9; padding-left: 2.55rem; padding-right: 1.2rem; padding-top: 0.1rem; padding-bottom: 0.1rem;">
                <input type="checkbox" class="custom-control-input" id="ctranscriptions" />
                <label class="custom-control-label" for="ctranscriptions">Transcriptions</label>
              </div>
            </div>
            <div class="message d-none alert alert-warning mx-auto" style="max-width: 300px;"></div>
             <table id="tableur-fiches" class="" style="width:100%; margin-left: -2px;">
                 <thead>
                     <tr>
                         <th class="fmt_vignettes">vignettes</th>
                         <th class="fmt_textes">textes</th>
                         <th class="titre">titre</th>
                         <th class="description">description</th>
                         <th class="identifiants">id</th>
                         <th class="dates">dates</th>
                         <th class="auteur">auteur</th>
                         <th class="genre">genre</th>
                         <th class="langues">langues</th>
                         <th class="series">series</th>
                     </tr>
                 </thead>
                 <tbody></tbody>
             </table>
           </div>
          </div>
        </div>
       <script type="text/javascript">
         var Table, Mode, Maj, BASEURL, Vue, PageDoc, PENSE_BASEURL, Bdd, Json;
         BASEURL = '';
         PENSE_BASEURL = '{$config:Serveur}';
         Mode = '{$mode}';
         Bdd = '{$bdd}';
         Json = {if(file:exists($config:Local||'/json/'||$bdd||'.json'))
         then '"'||$config:Serveur||'/json/'||$bdd||'.json"' else 'false'};
         $(document).ready(function(){{
            if(Json) {{ InitTableurCorpus() }}
            else {{ Message('Json') }}
          }})
       </script>
       <script src="{$config:Serveur}/ressources/biblio/openseadragon/openseadragon.min.js"></script>
       <script src="{$config:Serveur}/ressources/js/listing.js"></script>
       <script src="{$config:Serveur}/ressources/js/visionneuse.js"></script>
       { app:Modal() }
     </body>
  </html>
};
