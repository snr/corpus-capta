(:~
 : Page d’informations du compte Transkribus | Transkribus account information page
 :
 : @author Jean-Christophe Carius, INHA https://www.inha.fr, License à venir
 :)

module namespace compte = 'Corpus-Capta/compte';

import module namespace app = 'Corpus-Capta/app' at 'app.xqm';
import module namespace config = 'Corpus-Capta/config' at 'config.xqm';
import module namespace transkribus = 'Corpus-Capta/transkribus' at 'transkribus-api.xqm';

import module namespace Session = 'http://basex.org/modules/session';

(:~
 : Page compte utilisateur | User account page
 : @return page (html)
 :)
declare
  %rest:path("CorpusCapta/compte")
  %rest:GET
  %output:method('html')
  function compte:Interface() {
    let $compte := compte:Compte(Session:get('corpus-capta'))
    return
    <html lang="fr">
      { app:Entetes() }
     <body>
      <div class="container" style="max-width: 100%;">
        { app:Menu('','compte') }
        <div class="mt-5 mx-auto" style="width: 100%; max-width: 300px;">
          <h3>{Session:get('corpus-capta')}</h3>
          <form class="d-block" method="post" action="{$config:Serveur}/compte/maj">
            <input type="hidden" class="form-control" name="compte-basex" value="{Session:get('corpus-capta')}" />
            <div class="mb-3">
              <label for="compte-transkribus" class="form-label">Identifiant Transkribus</label>
              <input type="text" class="form-control" name="compte-transkribus" id="compte-transkribus" value="{$compte/@compte}" />
            </div>
            <div class="mb-3">
              <label for="mdp-transkribus" class="form-label">Mot de passe Transkribus</label>
              <input type="password" class="form-control" name="mdp-transkribus" id="mdp-transkribus" value="{$compte/@mdp}" />
            </div>
            <button class="btn btn-primary" type="submit">enregistrer les informations</button>
          </form>
          <div class="text-right border-top pt-4">
            <a class="btn btn-sm btn-danger" type="submit" href="{$config:Serveur}/logout">se déconnecter</a>
          </div>
        </div>
      </div>
     </body>
    </html>
};

(:~
 : Action de mise à jour du compte utilisateur | User account information updating action
 : @param  $compteTranskribus Nom du compte Transkribus | Transkribus account username
 : @param  $mdpTranskribus Mot de passe Transkribus | Transkribus password
 : @param  $compteBasex Nom du compte Basex | Basex account username
 : @return redirection vers ppage compte | redirect to user account page
 :)

declare
  %rest:path("CorpusCapta/compte/maj")
  %rest:POST
  %rest:form-param("compte-transkribus","{$compteTranskribus}")
  %rest:form-param("mdp-transkribus","{$mdpTranskribus}")
  %rest:form-param("compte-basex","{$compteBasex}")
  %output:method('xml')
function compte:Maj($compteTranskribus,$mdpTranskribus,$compteBasex) {
  let $admins := doc($config:Local||'/comptes.xml')/admins
  let $mdp := $mdpTranskribus
  let $comptes :=
  <admins>
    <admin nom="{$compteBasex}" compte="{$compteTranskribus}" mdp="{$mdp}" />
    {$admins/admin[not(@nom = $compteBasex)]}
  </admins>
  let $sauv := file:write($config:Local||'/comptes.xml',$comptes)
  return web:redirect('/CorpusCapta/compte' )
};

(:~
 : Informations du compte utilisateur | User account information
 : @param  $compte Nom du compte | Account username
 : @return admin (xml) | admin (xml)
 :)

declare function compte:Compte($compte) {
  let $admins := doc($config:Local||'/comptes.xml')/admins
  let $admin := $admins/admin[@nom = $compte]
  return <admin nom="{$compte}" compte="{$admin/@compte}" mdp="{$admin/@mdp}" />
};
