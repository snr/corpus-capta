(:~
 : JSON de la base de données | Database JSON data
 :
 : @author Jean-Christophe Carius, INHA https://www.inha.fr, ? License
 :)
module namespace listingJson = 'Corpus-Capta/listing-json';

import module namespace config = 'Corpus-Capta/config' at 'config.xqm';

(:~
 : Generer et enregistrer dans un fichier la représentation JSON de la base de données | Generate and save as file the JSON representation of the database
 : @param  $bdd : Nom de la base de donnnée | Database name
 : @param  $retour : Page à afficher en retour d'opération | Page to display after the operation
 : @return JSON
 :)

declare
  %rest:path("CorpusCapta/maj-json/{$bdd}")
  %rest:GET
  %rest:query-param("retour", "{$retour}","listing")
  %output:method('text')
  function listingJson:MajDonneesJson($bdd,$retour) {
    let $documents := for $document in collection($bdd)//*:TEI
        let $titre := normalize-space($document//*:titleStmt/*:title/text())
        let $id := $document//*:sourceDesc/*:msDesc//*:idno/text()
        let $cote := normalize-space($id)
        let $desc := $document//*:sourceDesc/*:p//text()
        let $serie := $document//*:seriesStmt/*:title/text()
        let $date := $document//*:profileDesc//*:date
        let $dateBrute := if($date/@when) then data($date/@when) else if($date/@from) then data($date/@from)||'-'||data($date/@to) else ''
        let $fmtDate := if($date/@when) then 'le '||listingJson:FmtDate(data($date/@when)) else if($date/@from) then 'du '||listingJson:FmtDate(data($date/@from))||' au '||listingJson:FmtDate(data($date/@to)) else '<i class="text-secondary">non daté précisément</i>'
        let $annee := tokenize($date/@*[local-name() = 'when' or local-name() = 'from'],'-')[1]
        let $annee := if(string-length($annee)) then $annee else 'non daté'
        let $auteur := $document//*:titleStmt/*:author/text()
        let $genre := $document//*:physDesc//*:p//*:objectType/text()
        let $autorite := $document//*:publicationStmt/*:publisher
        let $langues := string-join($document//*:profileDesc/*:langUsage/*:language/text(),', ')
        let $series := $document//*:seriesStmt/*:title/text()
        let $nbr_pages := $document//*:msDesc//*:objectDesc//*:measure/text()
        let $totalPages := count($document//*:surface)
        let $horizontales := 0
        let $vignettes := for $page in $document//*:surface
          let $pos := number(replace($page/@xml:id,'facs_',''))
          where $pos <= 8
          let $totalPages := if($totalPages>8) then 8 else $totalPages
          let $n := $pos - 1
          let $vignette := replace($cote||'-'||$pos,'ARCHIVES_','')||'.jpg'
          let $hd := $page/url/text()
          let $larg := data($page/@lrx)
          let $haut := data($page/@lry)
          let $o := if($larg > $haut) then 'H' else 'V'
          let $horizontales := if($larg > $haut) then $horizontales+1 else $horizontales
          let $decal := if($horizontales > 0) then 4 + $horizontales + 0.3 else 4
          let $marges := ((16+($decal*$totalPages))*-1)
          let $l := if($o = 'H') then 120 else round($larg div ($haut div 120))
          let $h := if($o = 'V') then 120 else round($haut div ($larg div 120))
          let $url := data($page/*:graphic/@url)
          let $url := replace($url,'fileType=view','fileType=thumb')
          let $img := '<img src="'||$url||'" class="d-inline-block bg-light shadow-2 position-relative orient-'||$o||'"  style="margin: 0 '||$marges||'px; z-index: '||$totalPages - $n||'; height: '||$h||'px; width: '||$l||'px; "/>'
        return $img
        let $xsl := $config:Local||"/ressources/xsl/Transcription-Tei-vers-HTML-2.xsl"
        let $transcription := xslt:transform-text($document,$xsl)
        let $lien := $config:Serveur||'/'||$bdd||'/document/' || $cote
        let $lienTei := $config:Serveur||'/'||$bdd||'/TEI/' || $cote ||'.tei.xml'
        let $map := map {
            'fmt_vignettes' :
                '<div class="d-flex carte-doc-vignettes justify-content-center align-items-center" style="background: #262b2e;">'
                  ||'<a target="int" class="carte-doc-vignettes-lien" href="'||$lien||'">'||string-join($vignettes)|| '</a>'
                ||'</div>'
            ,'fmt_infos' :
                '<div class="position-relative pb-4 flex-grow-1 d-flex flex-column justify-content-start">'
                  ||'<span class="resume-info info-titre">'||replace($titre,'^[\d_]+ ','')|| '</span>'
                  ||'<span class="resume-info info-dates">'||$fmtDate|| '</span>'
                  ||'<span class="resume-info info-auteur">'||$auteur|| '</span>'
                  ||'<span class="resume-info info-genre">'||$genre||'</span>'
                  ||'<span class="resume-info info-langues">'||$langues||'</span>'
                  ||'<span class="resume-info info-autorite">'||$autorite||'</span>'
                  ||'<span class="resume-info info-series">'||$series||'</span>'
                  ||'<span class="resume-info info-cotes border-0"><a target="int" href="'||$lienTei||'" style="color: #333;">'||$cote||'</a></span>'
                  ||'<a href="'||$lienTei||'" class="resume-info info-lien-tei" target="tei">TEI</a>'
                ||'</div>'
            ,'fmt_textes': '<div class="carte-doc-textes p-3 border flex-grow-1"><div class="bloc-page-transcription "><div class="position-relative" style="padding-right: 0 0.5rem !important;">'||$transcription||'</div></div></div>'
            ,'titre': $titre
            ,'description' : $titre
            ,'identifiants' : $cote
            ,'serie' : $series
            ,'dates' : $fmtDate
            ,'auteur' : $auteur
            ,'genre' : $genre
            ,'langues' : $langues
        }
        return json:serialize($map, map { 'format': 'direct' })
     let $json := '{"data":['||string-join($documents,',')||']}'
     let $dossier := $config:Local||'/json'
     let $creerDossier := if(not(file:exists($dossier))) then file:create-dir($dossier)
     let $sauv := file:write-text($dossier||'/'||$bdd||'.json',$json)
     return if($retour = 'json')
            then $json
            else web:redirect($config:Serveur||'/listing/'||$bdd)
};

(:~
 : Formater les dates pourle JSON | Format date for JSON
 : @param  $s : date aaaa-mm-jj | date yyyy-mm-dd
 : @return $date string
 :)

declare function listingJson:FmtDate($d){
  let $d := tokenize($d,'-')
  let $moisFR := ('jan.','fév.','mars','avr.','mai','juin','juil.','août','sept.','oct.','nov.','déc.')
  let $d2 := $moisFR[number($d[2])]
  return $d[3]||' '||$d2||' '||$d[1]
};
