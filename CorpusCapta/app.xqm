(:~
 : Page d'accueil de l'appli + divers utilitaires | App home page + various utilities
 :
 : @author Jean-Christophe Carius, INHA https://www.inha.fr, License à venir
 :)

module namespace app = 'Corpus-Capta/app';

import module namespace Session = 'http://basex.org/modules/session';
import module namespace config = 'Corpus-Capta/config' at 'config.xqm';
import module namespace transkribus = 'Corpus-Capta/transkribus' at 'transkribus-api.xqm';

(:~
 : Page d'accueil | Home page
 : @param  $reconnect Reconnecter à Transkribus | Reconnect to Transkribus
 : @return page (html)
 :)
declare
  %rest:path("CorpusCapta")
  %rest:GET
  %rest:query-param("reconnect", "{$reconnect}")
  %output:method('html')
  function app:Accueil($reconnect) {

  if($reconnect) then transkribus:ReconnecterTranskribus(Session:get('corpus-capta'))

  else

  <html lang="fr">

    { app:Entetes() }

  	<body style="padding: 20px 0;">
  		<div class="container">
  			<div class="d-flex justify-content-center">
  				<div class="">
  					<div class="mb-3">
  						<a class="text-secondary" href="//{request:hostname()}" style="text-decoration: none !important;">
  							<img class="text-middle" src="https://www.inha.fr/_attachments/visuels-pour-la-presse-ressources/couleur_contour_4lignes_web.jpg" style="width: 90px; background: #999; position: relative; top: -3px;" />
  						</a>
  						<a class="text-secondary d-none" href="//{request:hostname()}">
  							<span class="text-middle ml-2">Système de gestion de bases de données XML de l’INHA</span>
  						</a>
  					</div>
  					<h2 class="py-2" style="max-width: 580px; font-size: 140%;">Corpus Capta : Module d’importation et de visualisation de corpus enrichis</h2>
            <div class="mb-2 text-right">
              {
                if(Session:get('corpus-capta')) then
                  <a class="btn btn-sm btn-light" href="{$config:Serveur}/compte">{  Session:get('corpus-capta') }</a>
                else
                  <a class="btn btn-sm btn-light" href="{$config:Serveur}/login?_page={$config:Serveur}">se connecter</a>
                }
            </div>
            <div class="border-top bloc-arbo-bottom" style=";">
              <div class="liste-arbo" style="">
                {
                  let $admins := app:ListeAdmins()
                  let $listeBdd := for $b in app:ListeBases()
                      let $baseExiste := index-of(db:list-details(),$b)
                      order by $b
                      return if($baseExiste) then
                            let $adm := $admins/admin[matches(@bdd,''||$b||'')]
                            return <div class="elm-arbo"><a class="" href="{$config:Serveur}/listing/{$b}">{$b}</a> <span class="text-muted"> - {data($adm/@nom)}</span></div>
                      else <div class="elm-arbo"><span class="text-secondary">
                        <span>{$b}</span>
                        <small>
                          <i> Base non trouvée</i>
                        </small>
                      </span></div>
                  return $listeBdd
                }
              </div>
            </div>
  				</div>
  			</div>
  		</div>
  		<div class="bg-white p-2">
  			<div class="text-center mt-0 mb-5">
  				<span class="d-inline-block rounded" style="border: 3px solid #fff;">
  					<img class="border larg580px rounded schema-cc" src="{$config:Serveur}/ressources/images/schema-2.png" onclick="$(this).toggleClass('larg580px')" style="cursor: pointer;" />
  				</span>
  			</div>
  		</div>
  	</body>
  </html>
};


(:~
 : Entêtes HTML | HTML headers
 : @return HTML
 :)
declare function app:Entetes(){
  <head>
     <meta charset="UTF-8" />
     <title>CorpusCapta {
       let $titre := reverse(tokenize(request:path(),'/'))
       return ' | '||$titre[1]||' / '||$titre[2]
       }</title>
     <link rel="stylesheet" href="{$config:Serveur}/ressources/biblio/bootstrap.min.css" />
     <link rel="stylesheet" href="{$config:Serveur}/ressources/biblio/datatables/dataTables.bootstrap4.min.css" />
     <link rel="stylesheet" href="{$config:Serveur}/ressources/css/styles.css" />
    <script src="{$config:Serveur}/ressources/biblio/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="{$config:Serveur}/ressources/biblio/bootstrap.bundle.min.js"></script>
    <script src="{$config:Serveur}/ressources/biblio/datatables/datatables.min.js"></script>
    <script src="{$config:Serveur}/ressources/biblio/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="{$config:Serveur}/ressources/biblio/datatables/diacritics-sort.js"></script>
    <script src="{$config:Serveur}/ressources/biblio/datatables/jquery.highlight.js"></script>
    <script src="{$config:Serveur}/ressources/biblio/datatables/dataTables.searchHighlight.min.js"></script>
    <script src="{$config:Serveur}/ressources/js/scripts.js"></script>
 </head>
};

(:~
 : Menu de navigation | Navigation menu
 : @param  $bdd Base de données consultée | Database searched
 : @return HTML
 :)
declare function app:Menu($bdd,$page){
  <div>
    <div class="d-flex mt-1 align-items-center">
      <a class="btn btn-outline-secondary mr-1 btn-accueil" href="{$config:Serveur}" style="width: 30px; height: 30px;"><span class="d-none">Accueil</span></a>
      {
        let $listeBdd := for $b in app:ListeBases()
            where Session:get('corpus-capta') and app:EstAutorise(Session:get('corpus-capta'),$b)
            let $baseExiste := index-of(db:list-details(),$b)
            return if($baseExiste) then '<div class="border-bottom py-1"><a class="d-block" class="" href="'||$config:Serveur||'/listing/'||$b||'">'||$b||'</a></div>'
            else '<div class="border-bottom py-1 font-italic"><span class="d-block text-secondary">/CorpusCapta/listing/'||$b||'</span></div>'
        let $retourAccueil := '<div class="border-bottom pb-2"><a class="d-block text-success" href="'||$config:Serveur||'">Accueil</a></div>'
        let $deconnecter := '<div class=" pt-2"><a class="d-block text-warning" href="'||$config:Serveur||'?reconnect=1">Reconnecter</a></div>'
        let $deconnecter2 := '<div class=" pt-2"><a class="d-block text-danger" href="'||$config:Serveur||'/logout">Déconnecter BaseX</a></div>'
        let $compte := if(Session:get('corpus-capta')) then '<div class=" pt-2"><a class="d-block text-info border-bottom pb-2" href="'||$config:Serveur||'/compte">Compte</a></div>' else ''
        let $boutonConnecter := if(Session:get('corpus-capta')) then '<div class=" pt-2"><a class="d-block text-danger" href="'||$config:Serveur||'/logout">Déconnecter</a></div>' else '<div class=" pt-2"><a class="d-block text-danger" href="'||$config:Serveur||'/login">Se connecter</a></div>'
        return
        <div>
          <a tabindex="0" class="btn btn-sm btn-white btn-bdd vertical-middle border border-dark" style="border-width: 2px !important;
height: 29px; padding: 4px 10px; padding-left: 10px; line-height: 1;" role="button">{if(Session:get('corpus-capta')) then Session:get('corpus-capta') else 'se connecter'}</a>
          <script type="text/javascript">
            $(document).ready(function(){{
              $('.btn-bdd').popover({{
                  trigger: 'focus',
                  placement: 'bottom',
                  content:  function(){{
                    var a = '{ $retourAccueil }';
                    var l = '{ $listeBdd }';
                    var d = '{ $compte }';
                    var d2 = '{ $boutonConnecter }';
                    return l+d+d2;
                  }},
                  html: true,
              }})
            }})
          </script>
        </div>
      }
      {
        if(string-length($bdd)) then
        <div class=" mx-3 flex-grow-1">
          <h4 class="my-0"><a class="text-body" href="{$config:Serveur}/listing/{$bdd}">{$bdd}</a></h4>
          {if(db:exists($bdd) = false()) then <span class="alert alert-warning p-1 mt-2" style="font-size: 12px;">Base de données introuvable !</span>}

        </div>
        else ''
      }
      {
      if(string-length($bdd) and Session:get('corpus-capta') and app:EstAutorise(Session:get('corpus-capta'),$bdd)) then
      <div class="text-center">
        <a href="{$config:Serveur}/synchros/{$bdd}" class="btn btn-sm btn-outline-secondar text-primary">
        Mettre à jour
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cloud-download" viewBox="0 0 16 16">
            <path d="M4.406 1.342A5.53 5.53 0 0 1 8 0c2.69 0 4.923 2 5.166 4.579C14.758 4.804 16 6.137 16 7.773 16 9.569 14.502 11 12.687 11H10a.5.5 0 0 1 0-1h2.688C13.979 10 15 8.988 15 7.773c0-1.216-1.02-2.228-2.313-2.228h-.5v-.5C12.188 2.825 10.328 1 8 1a4.53 4.53 0 0 0-2.941 1.1c-.757.652-1.153 1.438-1.153 2.055v.448l-.445.049C2.064 4.805 1 5.952 1 7.318 1 8.785 2.23 10 3.781 10H6a.5.5 0 0 1 0 1H3.781C1.708 11 0 9.366 0 7.318c0-1.763 1.266-3.223 2.942-3.593.143-.863.698-1.723 1.464-2.383z"/>
            <path d="M7.646 15.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 14.293V5.5a.5.5 0 0 0-1 0v8.793l-2.146-2.147a.5.5 0 0 0-.708.708l3 3z"/>
          </svg>
        </a>
        {
          <div class="btn-group d-none" role="group" aria-label="Basic example">
            <a href="{$config:Serveur}/listing/{$bdd}" class="d-none btn btn-sm {if($page = 'listing') then 'btn-primary' else 'btn-outline-secondary'}">Listing</a>
          </div>
        }
      </div>
      else ''
    }
    </div>
    <hr class="mt-1 mb-0" />
  </div>
};

(:~
 : Retourne la liste des bases | Return databases list
 : @return $bases sequence
 :)
declare function app:ListeBases(){
  let $bases := db:list-details()
  return
         if (count($config:Projets))
         then for $projet in map:keys($config:Projets)
              return $projet
         else if(count($bases))
         then for $b in $bases
           where not(count($config:Projets)) or (count($config:Projets) and map:contains($config:Projets,$b/text()))
            return $b/text()
};

(:~
 : Génère le code HTML de la fenêtre modale Bootstrap | Generate the HTML code of the Bootstrap modal window
 : @return HTML
 :)

declare function app:Modal(){
  <div>
      <div class="modal modal-document" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-fullscreen" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title"></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="width: 35px; height: 35px;">
              </button>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer d-none">
              <button type="button" class="btn btn-fermer btn-sm btn-light" data-dismiss="modal">Fermer</button>
            </div>
          </div>
        </div>
      </div>
      <script type="text/javascript">
        $('.modal').on('hidden.bs.modal', function (e) {{
          var m = $(this);
          m.find('.modal-body').html('');
        }});
      </script>
  </div>
};


(:~
 : Formater un horodatage en date | Format timestamp as a date
 : @param dateTime
 : @return date en HTML | date in HTML
 :)

declare
%output:indent('yes')
%output:method('html')
function app:FmtDate($date){
  let $mois := ('jan','fév','mars','avril','mai','juin','juil',"août",'sept','oct','nov','déc')
  let $heures := hours-from-dateTime($date)
  let $minutes := minutes-from-dateTime($date)
  let $secondes := round(seconds-from-dateTime($date))
  let $heures := if(string-length(string($heures)) = 1) then '0'||$heures else $heures
  let $minutes := if(string-length(string($minutes)) = 1) then '0'||$minutes else $minutes
  let $secondes := if(string-length(string($secondes)) = 1) then '0'||$secondes else $secondes
  return
<span>
	<u>
		<span>{day-from-dateTime($date)} </span>
		<span class="mx-1">{$mois[month-from-dateTime($date)]} </span>
		<span class="date-annee">{year-from-dateTime($date)} </span>
	</u>
	<span class="date-heure"> à
		<span></span>{$heures}:{$minutes}:{$secondes}
	</span>
</span>
};


(:~
 : Afficher la page d'un fichier XML-TEI | Display the page of a XML-TEI file
 : @return page XML-TEI | XML-TEI page
 :)

declare
  %rest:path('/CorpusCapta/{$bdd}/TEI/{$file=.+}')
  %output:method('xml')
  %perm:allow('public')
function app:fichierTEI(
  $file,$bdd
) as item()+ {
  let $file := replace($file,'^ARCHIVES_','')
  let $file := replace($file,'\.tei\.xml$','.xml')
  let $doc := doc($bdd||'/'||$file)
  return $doc
};


(:~
 : Servir un fichier de type js,css,svg,png,json | Serve a file of type js,css,svg,png,json
 : @return fichier brut | Raw file
 :)

declare
  %rest:path('/CorpusCapta/{$file=.+\.(js|css|svg|png|json)(\?.+?=.+?)?}')
  %output:method('basex')
  %perm:allow('public')
function app:file(
  $file  as xs:string
) as item()+ {
  let $path := file:base-dir() ||  $file
  return (
    web:response-header(
      map { 'media-type': web:content-type($path) },
      map { 'Cache-Control': 'max-age=0,public', 'Content-Length': file:size($path) }
    ),
    file:read-binary($path)
  )
};


(:~
 : Permissions: Admin area.
 : Checks if the current user is admin; if not, redirects to the main page.
 : @param $perm  map with permission data
 :)
declare %perm:check('CorpusCapta/synchros','{$perm}') function app:check-admin($perm) {
  let $path := $perm?path
  let $allow := $perm?allow
  let $user := Session:get('corpus-capta')
  where empty($user)
  let $page := replace($path, '^.*Corpus-Capta/?', '')[.]
  return web:redirect('/CorpusCapta/login',app:parameters(map { 'page': $page }))
};

(:~
 : Login page.
 : @param  $name   user name (optional)
 : @param  $error  error string (optional)
 : @param  $page   page to redirect to (optional)
 : @return login page or redirection to main page
 :)
declare
  %rest:path('/CorpusCapta/login')
  %rest:query-param('_name',  '{$name}')
  %rest:query-param('_error', '{$error}')
  %rest:query-param('_page',  '{$page}')
  %output:method('html')
  %perm:allow('public')
function app:login(
  $name   as xs:string?,
  $error  as xs:string?,
  $page   as xs:string?
) as element() {
  (: user is already logged in: redirect to main page :)
  if(session:get($config:SESSION-KEY)) then web:redirect('/CorpusCapta') else
  <html lang="fr">
    { app:Entetes() }
   <body class="bg-light" style="padding: 20px 0;">
    <div class="position-absolute d-flex justify-content-center align-items-center" style="left: 0; right: 0; top: 0; bottom: 0;">
        <form class="border position-relative mx-auto p-2 rounded bg-white" style="" action="login-check" method="post">
          <p class="text-secondary">
            Connexion requise.
          </p>
          {
            if(string-length($error)) then <div class="alert alert-danger">{$error}</div>
          }
          <input name="_page" value="{ $page }" type="hidden"/>
          {
            map:for-each(app:parameters2(), function($key, $value) {
              <input name="{ $key }" value="{ $value }" type="hidden"/>
            })
          }
          <label>Identifiant</label>
          <input class="form-control" name="_name" value="{ $name }" id="user" size="30"/>
          <label class="mt-2">Mot de passe</label>
          <input class="form-control" name="_pass" type="password" size="30"/>
          <input class="btn btn-primary mt-2" type="submit" value="Identification Basex"/>
        </form>
      </div>
     </body>
    </html>
};

(:~
 : Checks the user input and redirects to the main page, or back to the login page.
 : @param  $name  user name
 : @param  $pass  password
 : @param  $page  page to redirect to (optional)
 : @return redirection
 :)
declare
  %rest:POST
  %rest:path('/CorpusCapta/login-check')
  %rest:query-param('_name', '{$name}')
  %rest:query-param('_pass', '{$pass}')
  %rest:query-param('_page', '{$page}')
  %perm:allow('public')
function app:login-check(
  $name  as xs:string,
  $pass  as xs:string,
  $page  as xs:string?
) as element(rest:response) {
  try {
    user:check($name, $pass),
    if(not(matches(user:list-details($name)/@permission,'(read|admin|create)'))) then (
      app:reject($name, 'Compte administrateur nécessaire', $page)
    ) else (
      app:accept($name, $page)
    )
  } catch user:* {
    app:reject($name, 'Identifiant ou mot de passe incorrect', $page)
  }
};

(:~
 : Ends a session and redirects to the login page.
 : @return redirection
 :)
declare
  %rest:path('/CorpusCapta/logout')
function app:logout(
) as element(rest:response) {
  let $user := session:get($config:SESSION-KEY)
  return (
    (: write log entry, redirect to login page :)
    admin:write-log('Logout: ' || $user, 'Corpus-Capta'),
    web:redirect('/CorpusCapta', map { '_name': $user })
  ),
  (: deletes the session key :)
  session:delete($config:SESSION-KEY)
};

(:~
 : Registers a user and redirects to the main page.
 : @param  $name  entered user name
 : @param  $page  page to redirect to (optional)
 : @return redirection
 :)
declare %private function app:accept(
  $name  as xs:string,
  $page  as xs:string?
) as element(rest:response) {
  (: register user, write log entry :)
  session:set($config:SESSION-KEY, $name),
  admin:write-log('Login: ' || $name, 'Corpus-Capta'),

  (: redirect to supplied page or main page :)
  web:redirect(if($page) then $page else $config:Serveur||'/', app:parameters2())
};

(:~
 : Rejects a user and redirects to the login page.
 : @param  $name   entered user name
 : @param  $error  error message
 : @param  $page   page to redirect to (optional)
 : @return redirection
 :)
declare %private function app:reject(
  $name   as xs:string,
  $error  as xs:string,
  $page   as xs:string?
) as element(rest:response) {
  (: write log entry, redirect to login page :)
  admin:write-log('Login denied: ' || $name, 'Corpus-Capta'),
  web:redirect(
    'login',
    app:parameters(map { 'name': $name, 'error': $error, 'page': $page })
  )
};

(:~
 : Creates a new map with the current query parameters.
 : @return map with query parameters
 :)
declare function app:parameters2() as map(*) {
  map:merge(
    for $param in request:parameter-names()[not(starts-with(., '_'))]
    return map { $param: request:parameter($param) }
  )
};

(:~
 : Creates a new map with query parameters. The returned map contains all
 : current query parameters, : and the given ones, prefixed with an underscore.
 : @param  $map  predefined parameters
 : @return map with query parameters
 :)
declare function app:parameters(
  $map  as map(*)?
) as map(*) {
  map:merge((
    app:parameters2(),
    map:for-each($map, function($name, $value) {
      map:entry('_' || $name, $value)
    })
  ))
};

declare function app:EstAutorise($u,$bdd){
  let $a := for $b in user:list-details($u)/database[@permission='write']
      where matches($bdd,$b/@pattern)
      return $b
  return count($a)
};

declare function app:ListeAdmins(){
    let $admins := for $u in user:list()
        where not($u = 'admin')
        let $u := user:list-details($u)
        return <admin nom="{$u/@name}" bdd="{string-join($u/database[@permission='write']/@pattern,', ')}" />
    return <admins>{$admins}</admins>
};
