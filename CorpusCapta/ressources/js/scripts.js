

$(document).ready(function(){

  $('body').on('click','.act-synchroniser',function(e){
    e.preventDefault();
    var b = $(this);
    var id = b.closest('tr').attr('id');
    document.getElementById(id).scrollIntoView({behavior:'smooth',block: "end", inline: "nearest"})
    var requete = b.attr('href');
    console.log(requete);
    $.ajax({
      url: requete,
      beforeSend: function(){
        var l = $('<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>');
        b.toggleClass('btn-white btn-light text-white text-secondary');
        b.prop('disabled',true).prepend(l);
      },
      success: function(r){
        b.closest('tr').addClass('maj').find('td.date-fichier').addClass('text-success').removeClass('text-danger').html(r);
      },
      error: function(jqXHR, textStatus, errorThrown ){
        console.log(textStatus);
        alert(jqXHR.responseText);
      },
      complete: function(){
        b.toggleClass('btn-white btn-light text-white text-secondary');
        b.prop('disabled',false).find('.lds-ellipsis').remove();
        if(MajAuto !== false){
          if(MajAuto < $('.act-synchroniser').length)
          {
            $('.act-synchroniser').eq(MajAuto).click();
            MajAuto++;
          }else{
            MajAuto = false;
            $('.btn-tout-maj').removeClass('mode-stop').text('→ Tout màj');
          }
        }
      }
    })
  });

  var MajAuto = false;

  $('body').on('click','.btn-tout-maj',function(e){
    e.preventDefault();
    var b = $(this);
    if(b.is('.mode-stop')){
      MajAuto = false;
      b.removeClass('mode-stop').text('→ Tout màj');
    }else{
      MajAuto = 1;
      $('.act-synchroniser').first().click();
      b.addClass('mode-stop').text('Pause');
    }
  });

  Table = $('#tableur-documents').DataTable({
    pageLength: 50,
    lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "Tout"] ],
    order: [[4,'desc']],
    dom:
        "<'row mb-2'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>"
         +"<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6 text-right'p>>"
         +"<'row'<'col-sm-12'tr>>"
         +"<'row'<'col-sm-12 col-md-5'><'col-sm-12 col-md-7'p>>",
     columnDefs: [
         { targets: [7], sortable: false, searchable: false },
     ]
  })

});
