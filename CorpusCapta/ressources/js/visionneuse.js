
var Visionneuse = false;

function PaginationTranscription(page){
  $('.page-document .bloc-page-id').each(function(i,elm){
    var n = i+1;
    var li = '<a class="nav-link text-center border-0 py-1 text-secondary" onclick="Visionneuse.goToPage('+i+')" style="min-width: 88px;" id="nav-page-'+n+'-tab" data-toggle="tab" href="#page-'+n+'" role="tab">page '+n+'</a>';
    $('.menu-pages-doc').append(li);
  });

  $('.page-document .bloc-page-transcription').each(function(i,elm){
    var n = i+1;
    var b = $('<div class="tab-pane fade transcription" id="page-'+n+'" role="tabpanel"><div class="d-inline-block mb-2 mt-0 mx-1 p-3 text-left bg-white" style="min-width: 80%;"></div></div>');
    b.find('div').append($(this));
    $('.tab-content').append(b);
  });

  $('.bloc-transcription').hide();
  $('.nav-link').eq((page-1)).addClass('active');
  $('.tab-pane').eq((page-1)).addClass('active show');

}

function InitVisionneuse(sources,page){
  Visionneuse = OpenSeadragon({
      id: "visio",
      prefixUrl: PENSE_BASEURL+"/ressources/biblio/openseadragon/images-2/",
      tileSources: sources,
      sequenceMode: true,
      initialPage: page-1 ,
  });

  Visionneuse.addHandler("page", ChangementPage, {});
  Visionneuse.addHandler("full-screen", ChangementPleinEcran, {pageCourante: Visionneuse.currentPage()});

  var isFullyLoaded = false;

  Visionneuse.world.addHandler('add-item', function(event) {
    var tiledImage = event.item;
    tiledImage.addHandler('fully-loaded-change', function() {
      var newFullyLoaded = areAllFullyLoaded();
      if (newFullyLoaded !== isFullyLoaded) {
        isFullyLoaded = newFullyLoaded;
        // Raise event
      }
      $('#visio').addClass('chargee');
      $('#visio .alert-chargement').remove();
    });
  });

  function ChangementPage(event, obj){
    var courant = event.page;
    $('#menu-visionneuse .nav-link').removeClass('active').eq(courant).addClass('active');
    $('.menu-pages-doc .nav-link').eq(courant).tab('show');
    $('#visio').removeClass('chargee');
  }

  function ChangementPleinEcran(eventSource, fullScreen, userData){
    var courant = Visionneuse.currentPage();
    $('#menu-visionneuse .nav-link').removeClass('active');
    $('#menu-visionneuse .nav-link').eq(courant).addClass('active');
    $('.menu-pages-doc .nav-link').eq(courant).tab('show');
  }

  function areAllFullyLoaded() {
    var tiledImage;
    var count = Visionneuse.world.getItemCount();
    for (var i = 0; i < count; i++) {
      tiledImage = Visionneuse.world.getItemAt(i);
      if (!tiledImage.getFullyLoaded()) {
        return false;
      }
    }
    return true;
  }

}

function InitBulleInfo(contexte){
  $(contexte+' [data-toggle="popover"]').popover({
    trigger: 'focus',
    placement: 'top',
    html: true,
    template: '<div class="popover popover-objet" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body p-0"></div></div>',
    customClass: '',
    content: function(){
      var b = $(this);
      var l = b.attr('data-wikidata');
      var t = b.attr('data-title');
      var type = b.attr('data-type');
      var labels = { 'persName': 'Personnes', 'orgName': "Organisations", 'event': 'Evenements', 'placeName': 'Lieux' }
      var tgt = contexte == 'body' ? '_self' : 'int';
      return ''
    },
    title: function(){
      var b = $(this);
      var l = b.attr('data-wikidata');
      var t = b.attr('data-title');
      var type = b.attr('data-type');
      var labels = { 'persName': 'Personnes', 'orgName': "Organisations", 'event': 'Evenements', 'placeName': 'Lieux' }
      return '<a class="" href="'+BASEURL+'/Indexation/'+labels[type]+'?e='+encodeURIComponent(t)+'" target="ext">'+t+'</a>'
    },
    customClass: function(){
      console.log(this);
    }
  });


  $('[data-toggle="tooltip"]').tooltip({
    html: true,
    trigger: 'hover focus',
    placement: 'bottom'
  })

}
