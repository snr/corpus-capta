

function InitTableurCorpus(){

  $('#ctranscriptions').prop('checked',false);

  // action voir transcriptions
  $('body').on('change','#ctranscriptions',function(){
    var b = $(this);
    if(b.is(':checked')){
      $('.liste-fiches').removeClass('sans-transcriptions');
      b.closest('.champ-transcriptions').toggleClass('text-primary border border-primary bg-bleuclair btn-secondary');
    }
    else{
      $('.liste-fiches').addClass('sans-transcriptions');
      b.closest('.champ-transcriptions').toggleClass('text-primary border border-primary bg-bleuclair btn-secondary');
    }
    b.blur();
  });

  $('body').on('click','.carte-doc-vignettes-lien',function(e){
    e.preventDefault();
    var b = $(this);
    var m = $('.modal');
    var href = b.attr('href');
    var p = href.match(/\/(\d+)/);
    p = p ? p[1] : 1;
    Sequence = [];
    m.modal();
    menuDocs(b);
    $.ajax({
      url: href+'?mode=ajax',
      success: function(html){
        var b = $('<div>'+html+'<div>').find('.page-document');
        var h = $('.modal-body').height();
        b.find('.numerisation').css({'min-height':'0','height':h+'px'});
        b.css({'height':h+'px'});
        m.find('.modal-body').append(b);
        var sources = b.find('.data-sources').attr('data-sources');
        $.each(sources.split(','),function(n,elm){
          var im = elm.replace(/fileType=thumb/,'fileType=view')
          var obj = new Object();
          obj.type = 'image'
          obj.url = im;
          Sequence.push(obj);
        });
        m.find('.modal-title').html('').append($('<div class="py-2 pl-2"><a class="text-body" href="'+href+'" target="int">'+$('.titre-document h5').text()+'</div>'));
        PaginationTranscription(p);
        InitVisionneuse(Sequence,p);
        InitBulleInfo('.modal-body');
      },
    });
  });

  $('body').on('click','.menu-doc',function(e){
    e.preventDefault();
    var b = $(this);
    var c = b.attr('href');
    var v = $('.liste-fiches .carte-doc-vignettes-lien[href="'+c+'"]');
    if(v.length){
      v.click();
    }else{
      var p = $('#tableur-fiches_paginate .active')
      if(b.is('.menu-doc-suiv')){
        p = p.next().find('a')
      }
      if(b.is('.menu-doc-prec')){
        p = p.prev().find('a')
      }
      p.click();
      var v = $('.liste-fiches .carte-doc-vignettes-lien[href="'+c+'"]');
      v.click();
    }
  });

  $('body').on('change','select.filtre',function(){
    var s = $(this);
    var c = s.attr('data-champ');
    var v = s.val();
    if(!v.length) v = false;
    if(v){
      s.addClass('text-primary bg-bleuclair')
      var b = $('<button type="button" class="close position-absolute" style="right: 1px; top: 5px; height: 26px; width: 22px; background: #fff; opacity: 1; border-radius: 0 3px 3px 0; float: none; outline: 0 none;"><span style="font-size: 16px; font-weight: normal; display: block;">&times;</span></button>');
      s.closest('.bloc-select').addClass('select-actif').append(b);
    }
    else{
      s.removeClass('text-primary bg-bleuclair');
      s.closest('.bloc-select').removeClass('select-actif').find('.close').remove();
    }
    ChercherTableau(c,v);
    s.blur();
  });

  var requete = Json;

  Table = $('#tableur-fiches').DataTable({
    ajax: requete,
    columns: [
      {data:'fmt_vignettes',className:'td-vignettes', visible: true, searchable: false,name:'vignettes' },
      {data:'fmt_textes',className:'td-textes', visible: true, name:'textes' },
      //{data:'fmt_infos',className:'td-infos'},
      {data:'titre',className:'td-titre', type: 'diacritics-modifie',name:'titre',
      "render": function(data, type, row, meta){
            return data.replace(/^[\d_]+ /,'');
      }},
      {data:'description',className:'td-description', type: 'diacritics-modifie', visible: false, name:'description' },
      {data:'identifiants',className:'td-identifiants',name:'identifiants'},
      {data:'dates',className:'td-dates',name:'dates',
            "render": function(data, type, row, meta){
                if('sort'==type){
                  return $(data).attr('data-date');
                }
                return data;
            }
      },
      //{data:'annee',className:'td-annee' },
      {data:'auteur',className:'td-auteur', type: 'diacritics-modifie',name:'auteur'},
      {data:'genre',className:'td-genre',name:'genre'},
      {data:'langues',className:'td-langues',name:'langues'},
      //{data:'nbr_pages',className:'td-nbr_pages'},
      {data:'serie',className:'td-serie',name:'serie'},
    ],
    pageLength: 50,
    lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "Tous"] ],
    processing: true,
    dom: "<'bloc-tableur'"
            +"<'filtres-1 d-flex justify-content-start border-bottom align-items-center mt-1 pb-1'"
              +"<'nav-gauche d-flex flex-wrap align-items-center'f<'filtres-selects'>>"
              +"<'nav-droite d-flex flex-wrap align-items-center justify-content-end'>"
            +">"
            +"<'filtres-2 d-flex justify-content-between border-bottom align-items-center mt-1 pb-1'"
              +"<'nav-gauche d-flex flex-wrap align-items-center'i>"
              +"<'nav-droite d-flex flex-wrap align-items-center justify-content-end'lp>"
            +">"
            +"<'liste-fiches sans-transcriptions sans-descriptions'>"
            +"<'mt-1 rangee-tableau'"
              +"<''tr>"
            +">"
            +"<'row mx-0'"
              +"<'col-sm-2 col-md-2 px-0'i>"
              +"<'nav-tableur col-sm-10 col-md-10 pr-0'p>"
            +">"
          +'>'
          ,
    columnDefs: [
        { targets: '_all', className: 'border' }
    ],
    "initComplete": function( settings, json ) {
      $('.champ-transcriptions').insertAfter($('.filtres-selects'));
      $('.filtres-1 .nav-droite').append($('.mode-vue'));
      $('.filtres-1 .nav-droite').append($('.btn-maj'));
      $('.filtres .bloc-filtre').each(function(n,f){
        f = $(this);
        InitFiltresCorpus(f.clone());
      });
      $('.cache-tableau').remove();
    },
    language: {
      decimal:"",
      emptyTable:"Aucune donnée.",
      info:"_START_ - _END_ / _TOTAL_ pièces",
      infoEmpty: "0 document",
      infoFiltered: "(filtré d'un total de _MAX_ pièce(s))",
      infoPostFix: "",
      thousands:",",
      lengthMenu:"Afficher _MENU_ ",
      loadingRecords: "Chargement...",
      processing:"<div style=\"\">Opération en cours...</div>",
      search:"",
      searchPlaceholder:"Filtrer par texte",
      zeroRecords:"<div class=\"alert-secondary mx-2 p-5\">Aucun résultat</div>",
      paginate: { previous: '‹', next:'›' },
    },
  });

  Table.on( 'draw', function () {
    SurlignerRecherche(Table.table().body());
    if('fiches' == Mode){
      MajFiches();
    }
  });

  // màj fiches
  function MajFiches(){

    $('.rangee-tableau').hide();
    var liste = $('.liste-fiches');
    liste.show();
    liste.html('');

    var table = $('#tableur-fiches').DataTable();
    var div = $('<div class="d-flex flex-wrap cartes-docs justify-content-center" />');

    var lignes = table.rows({page:'current',order:'current',search:'applied'}).data();

    for(i=0;i<lignes.length;i++){
      var ligne = $(lignes[i])[0];
      var fiche = $('<div class="carte-doc d-flex justify-content-center"><div class="carte-doc-vignettes-infos d-flex flex-column"></div></div>');

      var vignettes = $(ligne.fmt_vignettes);
      var infos = $(ligne.fmt_infos);

      fiche.find('.carte-doc-vignettes-infos')
        .append(vignettes)
        .append(infos);

      var textes = $(ligne.fmt_textes);
      textes.addClass('flex-grow-1');

      fiche.append(textes);
      div.append(fiche);

    }

    liste.append(div);

    if(lignes.length == 0){
      var b = $('.btn-page-importer').clone();
      var d = $('<div class="text-light text-center"><div class="mb-3">Aucune donnée trouvée.</div></div>');
      d.append(b);
      liste.find('.cartes-docs').css({'height':'450px'}).addClass('align-items-center').append(d);
    }

    SurlignerRecherche(div);

    InitBulleInfo('.liste-fiches');

  }

}

function InitFiltresCorpus(f){

  var c = f.find('select').attr('data-champ');
  var valeurs = Table.column(c+':name').data();
  if(!valeurs){ return; }
  var liste = valeurs.filter((v, i, a) => a.indexOf(v) === i);
  var nuls = liste.indexOf(null);
  var vide = liste.indexOf('');
  liste = liste.filter((v, i, a) => v != null && v != '');
  liste.sort(function (a, b) { return a.localeCompare(b); });
  var options = [];
  $.each(liste,function(i,t){
    var r = Table.column(c+':name').data().filter(function(v,i){return v == t ? true:false})
    var nbr = r.length;
    options.push('<option value="'+t+'">'+t+' ('+nbr+')</option>');
  });
  if(nuls>-1){
    var nbr = Table.column(c+':name').data().filter(function(v,i){return v == null ? true:false}).length;
    options.push('<option value="?">? ('+nbr+')</option>');
  }
  if(vide>-1){
    var nbr = Table.column(c+':name').data().filter(function(v,i){return v == '' ? true:false}).length;
    options.push('<option value="">? ('+nbr+')</option>');
  }
  f.find('select').append(options.join(''))
  $('.filtres-selects').append(f);

}

function Message(type){

  $('#tableur-fiches').hide();
  $('.message').removeClass('d-none').html('<a href="listing/maj-json">Générer le fichier Json</a>')

}

function ChercherTableau(c,v){

  var cols = Table.settings().init().columns;
  if(v){
    var rg = true;
    if(v == '?'){
      rg = true;
      v =  '^$';
    }
    Table.column(c+':name').search('^'+v+'$',rg).draw();
  }else{
    Table.column(c+':name').search('').draw();
  }
}


function InitBulleInfo(contexte){

  $(contexte+' [data-toggle="popover"]').popover({
    trigger: 'focus',
    placement: 'top',
    html: true,
    template: '<div class="popover popover-objet" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body p-0"></div></div>',
    customClass: '',
    content: function(){
      var b = $(this);
      var l = b.attr('data-wikidata');
      var t = b.attr('data-title');
      var type = b.attr('data-type');
      var labels = { 'persName': 'Personnes', 'orgName': "Organisations", 'event': 'Evenements', 'placeName': 'Lieux' }
      var tgt = contexte == 'body' ? '_self' : 'int';
      return ''
    },
    title: function(){
      var b = $(this);
      var l = b.attr('data-wikidata');
      var t = b.attr('data-title');
      var type = b.attr('data-type');
      var labels = { 'persName': 'Personnes', 'orgName': "Organisations", 'event': 'Evenements', 'placeName': 'Lieux' }
      return '<a class="" href="'+BASEURL+'/Indexation/'+labels[type]+'?e='+encodeURIComponent(t)+'" target="ext">'+t+'</a>'
    },
    customClass: function(){

    }
  });

  $('[data-toggle="tooltip"]').tooltip({
    html: true,
    trigger: 'hover focus',
    placement: 'bottom'
  })

}

function SurlignerRecherche(contexte){
  var body = $(contexte);
  body.unhighlight();
  $('.resume-info.rech-result').removeClass('rech-result');
  var recherche = Table.search();
  recherche = $.trim(recherche);
  if ( '' != recherche ){
      var termes = Table.search().split(' ');
      for(i=0;i<termes.length;i++){
        var terme = termes[i];
        body.highlight( terme );
      };
      $('.resume-info').each(function(){
        var b = $(this);
        if(b.find('.highlight').length){
          b.addClass('rech-result');
        }
      })
  }
}

function menuDocs(b){
  var b = $(b);
  var cote = b.attr('href').split('/');
  cote = cote[(cote.length-1)];
  var trs = $('#tableur-fiches').find('tr');
  var tr;
  trs.each(function(n,t){
    t = $(t);
    if(t.find('.td-identifiants').text() == cote){
      tr = t;
      return false;
    }
  });
  var rangees = Table.rows( { search: 'applied' } )[0];
  var num = Table.row( tr ).index();
  var index = rangees.indexOf(num);
  var prec = rangees[(index-1)];
  var suiv = rangees[(index+1)];
  prec = index > 0
         ? PENSE_BASEURL+'/'+Bdd+'/document/'+Table.rows(rangees[(index-1)]).data()[0]['identifiants'] : '#';
  suiv = index < rangees.length-1
         ? PENSE_BASEURL+'/'+Bdd+'/document/'+Table.rows(rangees[(index+1)]).data()[0]['identifiants'] : '#';
  var classePrec = index > 0 ? '' : 'text-light';
  var classeSuiv = index < rangees.length-1 ? '' : 'text-light';

  var m = $('<div class="menu-docs mr-5 mr-5 d-flex justify-content-between align-items-center flex-nowrap mt-2" style="padding-top: 1px;"><a target="int" class="'+classePrec+' text-decoration-none mx-2 ml-4 menu-doc menu-doc-prec" style="font-size: 120%" href="'+prec+'">←</a> <a target="int" class="text-body text-decoration-none" href="'+PENSE_BASEURL+'/'+Bdd+'/document/'+cote+'">'+cote+'</a> <a target="int" class="'+classeSuiv+' text-decoration-none mx-2 menu-doc menu-doc-suiv" style="font-size: 120%" href="'+suiv+'">→</a></div>');
  $('.modal-header .menu-docs').remove();
  m.appendTo($('.modal-header'));
}

var myEfficientFn = debounce(function() {
  var m = $('.modal');
  if(m.is(':visible')){
    var h = $('.modal-body').height();
    var p = Visionneuse.currentPage();
    Visionneuse.destroy();
    var h = $('.modal-body').height();
    m.find('.page-document').css({'height':h+'px'});
    m.find('.numerisation').css({'height':h+'px'});
    InitVisionneuse(Sequence,(p+1));
  }
}, 500);

window.addEventListener('resize', myEfficientFn);

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};
