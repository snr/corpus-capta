
/**
 * 
 * 
 * @param line The desired line's identifier
 * @param params Parameter that might be passed to the line
 * 
 * @return string
 */
function __(line, params) {
    params = params || {};

    //console.log(typeof(LOCALE)+' > '+line);
    if (typeof (LOCALE) !== 'undefined' && LOCALE.hasOwnProperty(line)) {
        line = LOCALE[line];
    }

    for (var key in params) {
        line = line.replace(key, params[key]);
    }

    return line;
}


function UrlVarsToHiddenField(href, exclude) {
    
    if (exclude === undefined) {
        exclude = '';
    }
    
    var hidden_field = '';
    var parts = href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        if (exclude != key) {
            value = value.replace(/\+/g, ' ');
            value = decodeURIComponent(value);
            value = escapeHtml(value);
            hidden_field += '<input type="hidden" name="' + key + '" value="' + value + '">';
        }
    });
    return hidden_field;
}

function escapeHtml(s) {
    return s.toString().split('&').join('&amp;').split('<').join('&lt;').split('"').join('&quot;');
}