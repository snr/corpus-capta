var ContentLeft = function (element, options) {
    options = options || {};
    this.element = element;

    this.viewer_width = options.viewer_width;
    this.menu_left_visible = false;
    this.menu_left = options.menu_left;
    this.open_type = '';

    // init ui and apps
    this.viewer = null;
    this.picture = null;
    this.ui_bottom = null;
    this.flip = null;
    this.ui_bottom = null;

    // params
    this.options = options;
    this.options.viewer_type = options.viewer_type || 'picture';
    this.options.item_id = options.item_id || 0;
    this.page = options.page || 1;
    this.uniqkey = Math.random() * 0x100000000000000;
    this.image = options.image;
    this.search_q = options.search_q || '';
    this.direction = options.direction;
    this.options.dir_ico = options.dir_ico || this.links_prefix + '/assets/images/viewer/';
    this.options.bookmarks_collapsed = options.bookmarks_collapsed || false;
    this.user_id = options.user_id || false;
    this.volume = options.volume;
    this.links_prefix = options.links_prefix || '';
    this.alto_count = 0;
    this.offset = 0;
    this.can_search = 1;
    this.search_alto_open = 0;
    this.search_limit = options.search_limit || 10;
    this.prev_next_type = '';
    this.html_page = options.html_page || ''
    this.language = options.language || '';
    this.custom_bottom_name = '';
    this.custom_bottom_href = '';
    this.download_format = '';
    this.download_type = '';
    this.share_crop = 0;
    this.http = options.http || '';
    this.parent_itemtype_id = options.parent_itemtype_id || '';

    this.init_ui();
    this.setup_events();
    if (this.options.open_by_default == 'search') {
        var that = this;
        setTimeout(function () {
            that.search();
        }, 500);
    }
};

// LINK TO PICTURE
ContentLeft.prototype.set_picture = function (picture) {
    this.picture = picture;
}
// LINK TO FLIP
ContentLeft.prototype.set_flip = function (flip) {
    this.flip = flip;
}
// LINK TO VIEWER
ContentLeft.prototype.set_viewer = function (viewer) {
    this.viewer = viewer;
}
// LINK TO BOTTOM
ContentLeft.prototype.set_ui_bottom = function (ui_bottom) {
    this.ui_bottom = ui_bottom;
}
// CHANGE VIEWER TYPE
ContentLeft.prototype.set_viewer_type = function (viewer) {
    this.options.viewer_type = viewer;
    if (this.open_type == 'info' && this.html_page != '') {
        this.info_html_page();
    }
    if (this.open_type == 'info') {
        this.info_viewer_left();
    }
    if (this.open_type == 'share') {
        this.share_content();
    }
    if (this.open_type == 'comparator') {
        this.comparator_content();
    }
    if (this.open_type == 'download') {
        this.download_content();
    } else {
        this.download_type = 'picture';
        this.printfile_type_val();
        this.remove_crop();
    }
}
// CHANGE PAGE PICTURE
ContentLeft.prototype.set_zoom = function (prc) {
    if (this.open_type == 'download') {
        if (this.download_type == 'crop') {
            this.download_type = 'picture';
            this.printfile_type_val();
            this.remove_crop();
        }
    } else if (this.share_crop == 1) {
        $('#cl_share_crop').prop("checked", false);
        this.remove_crop();
    }
}

// CHANGE PAGE PICTURE
ContentLeft.prototype.set_page = function (page) {
    this.page = page;
    if (this.open_type == 'ocr') {
        this.ocr_content(__('View OCR by selecting the desired area.'));
    }
    if (this.open_type == 'info' && this.html_page != '') {
        this.info_html_page();
    }
    if (this.open_type == 'info') {
        this.info_viewer_left();
    }
    if (this.open_type == 'download') {
        this.remove_crop();
        this.download_content();
    }
    if (this.open_type == 'share') {
        $('#cl_share_crop').prop("checked", false);
        this.remove_crop();
        this.share_content();
    }
    if (this.open_type == 'annotate') {
        this.collaborate_annotate.set_page(page);
    }
    if (this.open_type == 'comparator') {
        this.comparator_content();
    }
    if (this.open_type.substring(0, 14) == 'custom_bottom_') {
        this.custom_bottom(this.custom_bottom_name, this.custom_bottom_href, 1);
    }
}

// UI TRACER
ContentLeft.prototype.init_ui = function () {

    this.left_content = $('<div class="left_content"></div>');
    this.left_content.appendTo(this.element);

    this.content_left_bar = $('<div class="content_left_bar"></div>');
    this.content_left_bar.appendTo(this.left_content);

    this.content_left_title = $('<div class="content_left_title">' + __('Bookmarks') + '</div>');
    this.content_left_title.appendTo(this.content_left_bar);

    this.content_left_close = $('<div class="content_left_close button"><img title="' + __('Close') + '" alt="' + __('Close') + '" src="' + this.options.dir_ico + 'close.png"></div>');
    this.content_left_close.appendTo(this.content_left_bar);

    this.content_clear = $('<div style="clear:both"></div>');
    this.content_clear.appendTo(this.content_left_bar);

    this.left_content_div = $('<div></div>');
    this.left_content_div.appendTo(this.left_content);
}

// RESIZE
ContentLeft.prototype.set_resize = function (w, h) {
    this.viewer_height = h;
    this.viewer_width = w;

}
// EVENT TRACKER
ContentLeft.prototype.setup_events = function () {
    var that = this;
    this.element.resizable(
            {
                handles: "e",
                minWidth: 200,
                start: function (event, ui)
                {
                    that.picture._hide_prev_next();
                    if (that.options.flipping) {
                        that.flip._hide_prev_next();
                    }
                },
                stop: function (event, ui)
                {
                    that.menu_left = that.element.width();
                    that.picture._show_prev_next();
                    if (that.options.flipping) {
                        that.flip._show_prev_next();
                    }
                    that.viewer.set_menu_left_width(that.menu_left);
                }
            });

    // hide content_left
    this.content_left_close.on('click', function (e) {
        e.preventDefault();
        that.close();
    });

    // change page
    this.element.on('click', '.click_bookmark', function (e) {
        e.preventDefault();
        var page = $(this).attr('data');

        that.viewer.set_page(page);
        if (that.options.viewer_type == 'thumb' || that.options.viewer_type == 'thumbbig')
        {
            that.viewer.position();
        }
    });
};

// WIDTH
ContentLeft.prototype._width = function () {

    this.menu_left_w = 0;
    if (this.menu_left > this.viewer_width)
    {
        this.menu_left = this.viewer_width * 0.25;
    }
    if (this.menu_left < 200)
    {
        this.menu_left = 200;
    }
    if (this.menu_left_visible == true)
    {
        this.menu_left_w = this.menu_left;
        this.element.width(this.menu_left_w);
    }

    this.viewer.set_menu_left_width(this.menu_left_w);
};

// CLOSE
ContentLeft.prototype.close = function () {
    this.open_type = '';
    this.menu_left_visible = false;
    this.element.hide();

    this._width();
    this.set_resize(this.viewer_width, this.viewer_height);

    this.picture.remove_highlight();
    this.flip.remove_highlight();

    if (this.collaborate_annotate) {
        this.collaborate_annotate.close();
    }
};

// OPEN
ContentLeft.prototype.open = function () {
    this.menu_left_visible = true;
    this.element.show();

    this._width();
    this.set_resize(this.viewer_width, this.viewer_height);
};

// OPEN / CLOSE
ContentLeft.prototype.open_close = function (open_type) {
    if (this.menu_left_visible == false || this.open_type != open_type)
    {
        this.open();
        this.viewer.set_open(open_type);
    } else
    {
        this.close();
        this.viewer.set_open('');
    }
    if (this.collaborate_annotate) {
        this.collaborate_annotate.close();
    }
    this.flip.remove_highlight();
    this.picture.remove_highlight();
    this.open_type = open_type;
};

// BOOKMARK
ContentLeft.prototype.bookmark = function () {
    this.open_close('bookmark');

    this.content_left_title.html(__('Bookmarks'));
    var that = this;

    $.getJSON(that.links_prefix + "/api/bookmarks",
            {
                item_id: that.options.item_id,
                volume: that.volume
            },
            function (data)
            {
                var html_bookmarks = '';
                if (data && data.items) {
                    html_bookmarks += '<div dir="' + that.direction + '" class="bookmark_content bookmark_content_' + that.uniqkey + '">';
                    html_bookmarks += that.draw_bookmark(data.items);
                    html_bookmarks += '</div>';
                }
                that.left_content_div.html(html_bookmarks);

                $(".bookmark_content_" + that.uniqkey).treeview({
                    persist: "location",
                    collapsed: that.options.bookmarks_collapsed,
                    unique: true
                });

            });

};
ContentLeft.prototype.draw_bookmark = function (bookmarks) {
    var html = '';
    if (bookmarks != undefined) {
        if (bookmarks.length > 0) {
            html += '<ul>';
            for (var i = 0; i < bookmarks.length; i++) {
                var bookmark = bookmarks[i];
                html += '<li class="class_bookmark class_bookmark_' + bookmark.page + '"><span class="click_bookmark" data="' + bookmark.page + '">' + bookmark.text + '</span>';
                html += this.draw_bookmark(bookmark.sons);
                html += '</li>';
            }
            html += '</ul>';
        }
    }
    return html;
};


// OCR
ContentLeft.prototype.ocr = function () {
    this.open_close('ocr');

    this.content_left_title.html(__('Text'));
    this.ocr_content(__('You can see the ocr by selecting the desired area.'));
    this.viewer.set_page(this.page);
};
ContentLeft.prototype.ocr_content = function (txt) {

    var html = '';
    html += '<div dir="' + this.direction + '" class="ocr_content">';
    html += txt;
    html += '</div>';

    this.left_content_div.html(html);
}


// ANNOTATE


ContentLeft.prototype.annotate = function () {
    this.open_close('annotate');
    this.content_left_title.html(__('Annotate'));
    // this.left_content_div.html('...');
    //this.left_content_div.Collaborate_Annotate(this.menu_left_visible);

    if (this.collaborate_annotate) {
        if (this.menu_left_visible == false) {
            this.collaborate_annotate.close();
        } else {
            this.collaborate_annotate.set_page_direct(this.page);
            this.collaborate_annotate.init();
        }
    } else {
        this.collaborate_annotate = new Collaborate_Annotate(this.left_content_div, {
            'page': this.page,
            'item_id': this.options.item_id,
            'show_annotate': this.options.show_annotate,
            'annotate_id': this.options.annotate_id,
            'volume': this.options.volume,
            'user_id': this.user_id,
            'server': this.options.server,
            'image': this.image
        });
    }
};

// SEARCH
ContentLeft.prototype.search = function () {

    this.open_close('search');
    this.content_left_title.html(__('Search'));

    var html = '';
    html += '<div id="search_form_' + this.uniqkey + '" class="search_form">';
    html += '<input id="search_txt_' + this.uniqkey + '" class="search_txt" type="text" value="' + this.search_q.replace(/"/g, "&quot;") + '" size="30" name="search_txt">';
    html += '<input title="' + __('Search') + '" alt="' + __('Search') + '" id="search_bt_' + this.uniqkey + '" class="search_bt" type="submit" value="ok" name="search_bt">';
    html += '<div id="search_wait_' + this.uniqkey + '" class="search_wait" style="display:none;">' + __('Searching...') + '</div>';
    html += '</div>';
    html += '<div id="search_result_' + this.uniqkey + '" class="search_result" dir="' + this.direction + '"></div>';

    this.left_content_div.html(html);
    this.search_events();

    $('#search_txt_' + this.uniqkey).focus();

    if (this.search_q != '') {
        this.search_init();
        this.viewer.set_page(this.page);
    }
};

ContentLeft.prototype.search_events = function () {

    this.element.off("click", '#search_bt_' + this.uniqkey);
    this.element.off("keypress", '#search_txt_' + this.uniqkey);
    this.element.off("click", '.search_result_title');

    var that = this;
    this.element.on('click', '#search_bt_' + this.uniqkey, function (e) {
        e.preventDefault();
        that.search_q = $("#search_txt_" + that.uniqkey).val();
        that.search_init();

    });
    this.element.on('keypress', '#search_txt_' + this.uniqkey, function (e) {
        if (e.which == 13)
        {
            that.search_q = $("#search_txt_" + that.uniqkey).val();
            that.search_init();
        }
    });

    this.element.on('click', '.search_result_title', function (e) {
        e.preventDefault();
        var type = $(this).attr("data-type");
        $('.search_result_text_' + type).toggle();
        $('.arrow_' + type).toggle();
    });



    //

    //annotation
    this.element.on('mouseenter', '.zone_highlight_search_show', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var page = $(this).attr('data-page');
        $('.zone_highlight_search_box').remove();
        clearInterval(that.zone_highlight_search_interval);
        if (that.page == page) {
            var zones = $('.zone_highlight_search_show').find('.zone_highlight_search_' + id);
            $.each(zones, function (key, val)
            {
                var zone = $(this).html();
                if (zone != undefined) {
                    var z = '<div class="zone_highlight_search_box">' + zone + '</div>';
                    var pic = $('.picture > .openseadragon-container .picture_highlight_div');
                    pic.append(z);
                }
            });

        } else {
            that.zone_highlight_search_interval = setInterval(function () {
                if (that.page == page) {
                    clearInterval(that.zone_highlight_search_interval);
                    $('.zone_highlight_search_box').remove();
                    setTimeout(function () {
                        $('.zone_highlight_search_box').remove();
                        var zones = $('.zone_highlight_search_show').find('.zone_highlight_search_' + id);
                        $.each(zones, function (key, val)
                        {
                            var zone = $(this).html();
                            if (zone != undefined) {
                                var z = '<div class="zone_highlight_search_box">' + zone + '</div>';
                                var pic = $('.picture > .openseadragon-container .picture_highlight_div');
                                pic.append(z);
                            }
                        });

                    }, 2000);
                }
            }, 500);
        }

    });
    this.element.on('mouseleave', '.zone_highlight_search_show', function (e) {
        e.preventDefault();
        clearInterval(that.zone_highlight_search_interval);
        $('.zone_highlight_search_box').remove();
    });
    // end annotation
};

ContentLeft.prototype.search_init = function () {
    $('#search_result_' + this.uniqkey).html('');
    this.search_alto_open = 0;
    this.offset = 0;
    this.offset_annotation = 0;
    this.flip.remove_highlight();
    this.picture.remove_highlight();
    this.search_alto();
    this.picture.highlight_alto();
    this.flip.highlight_alto();
    this.viewer.options.search_q = this.search_q;
    this.viewer.set_open('search');
};

ContentLeft.prototype.search_alto = function ()
{
    this.element.off("click", '.search_result_content');
    this.element.off("click", '.prev_next_bt_alto');
    this.element.off("click", '.prev_next_bt_annotation');
    clearInterval(this.zone_highlight_search_interval);


    $('#search_result_' + this.uniqkey).hide();
    $('#search_result_' + this.uniqkey).html('');


    if (this.can_search == 1 && this.search_q.trim() != '')
    {

        this.can_search = 0;
        $('#search_wait_' + this.uniqkey).show();
        $('#search_result_' + this.uniqkey).hide();
        $('#search_result_' + this.uniqkey).html('');

        var that = this;

        $.getJSON(that.links_prefix + "/api/altopages",
                {
                    limit: that.search_limit,
                    offset: that.offset,
                    item_id: that.options.item_id,
                    q: that.search_q,
                    volume: that.volume
                },
                function (data)
                {
                    that.alto_count = 0;
                    var hrml_ocr = '';
                    if (data && data.items) {
                        hrml_ocr = that.draw_items(data.items);
                    }
                    if (data) {
                        if (data._meta) {
                            if (data._meta.total) {
                                that.alto_count = data._meta.total;

                                if (that.alto_count > that.search_limit) {

                                    var alto_prev_next = data._links;

                                    if (alto_prev_next) {
                                        hrml_ocr += '<div class="search_prev_next">';

                                        hrml_ocr += '<div class="search_prev_next_td">&nbsp;';
                                        if (alto_prev_next.first && alto_prev_next.prev) {
                                            hrml_ocr += '<span class="prev_next_bt_alto prev_next_bt prev" data-href="' + alto_prev_next.first.href + '">' + __('First') + '</span>';
                                        }
                                        if (alto_prev_next.prev) {
                                            hrml_ocr += '<span class="prev_next_bt_alto prev_next_bt prev" data-href="' + alto_prev_next.prev.href + '">' + __('Prev') + '</span>';
                                        }
                                        hrml_ocr += '</div>';

                                        hrml_ocr += '<div class="search_prev_next_td">';
                                        hrml_ocr += '<span class="search_prev_next_page">' + __('Page') + ' : ' + ((that.offset / that.search_limit) + 1) + '</span>';
                                        hrml_ocr += '</div>';

                                        hrml_ocr += '<div class="search_prev_next_td">';
                                        if (alto_prev_next.last && alto_prev_next.next) {
                                            hrml_ocr += '<span class="prev_next_bt_alto prev_next_bt next" data-href="' + alto_prev_next.last.href + '">' + __('Last') + '</span>';
                                        }
                                        if (alto_prev_next.next) {
                                            hrml_ocr += '<span class="prev_next_bt_alto prev_next_bt next" data-href="' + alto_prev_next.next.href + '">' + __('Next') + '</span>';
                                        }
                                        hrml_ocr += '</div>';

                                        hrml_ocr += '<div style="clear:both"></div></div>';
                                    }
                                }
                            }
                        }
                    }

///////////
                    $.getJSON(that.links_prefix + "/api/bookmarks/search",
                            {
                                limit: that.search_limit,
                                offset: that.offset,
                                item_id: that.options.item_id,
                                q: that.search_q,
                                volume: that.volume
                            },
                            function (data)
                            {
                                that.bookmarks_count = 0;
                                var html_bookmarks = '';
                                if (data && data.items) {
                                    html_bookmarks = that.draw_items(data.items);
                                }
                                if (data) {
                                    if (data._meta) {
                                        if (data._meta.total) {
                                            that.bookmarks_count = data._meta.total;
                                        }
                                    }
                                }
/////////////

///////////
                                var search_volume = '';
                                if (that.volume != '') {
                                    search_volume = ' AND *' + that.volume + '*';
                                }

                                var html_annotation = '';
                                $.getJSON(that.links_prefix + "/api/items",
                                        {
                                            limit: that.search_limit,
                                            itemtype_id: that.options.show_annotate,
                                            offset: that.offset_annotation,
                                            father_id: that.options.item_id,
                                            search: that.search_q + search_volume,
                                            elements: 1
                                        },
                                        function (data)
                                        {
                                            that.annotation_count = 0;
                                            if (data && data.items && data._meta) {
                                                html_annotation = that.draw_annotations(data.items, data._meta);
                                            }
                                            if (data) {
                                                if (data._meta) {
                                                    if (data._meta.total) {
                                                        that.annotation_count = data._meta.total;

                                                        if (that.annotation_count > that.search_limit) {

                                                            var annotation_prev_next = data._links;

                                                            if (annotation_prev_next) {
                                                                html_annotation += '<div class="search_prev_next">';

                                                                html_annotation += '<div class="search_prev_next_td">&nbsp;';
                                                                if (annotation_prev_next.first && annotation_prev_next.prev) {
                                                                    html_annotation += '<span class="prev_next_bt_annotation prev_next_bt prev" data-href="' + annotation_prev_next.first.href + '">' + __('First') + '</span>';
                                                                }
                                                                if (annotation_prev_next.prev) {
                                                                    html_annotation += '<span class="prev_next_bt_annotation prev_next_bt prev" data-href="' + annotation_prev_next.prev.href + '">' + __('Prev') + '</span>';
                                                                }
                                                                html_annotation += '</div>';

                                                                html_annotation += '<div class="search_prev_next_td">';
                                                                html_annotation += '<span class="search_prev_next_page">' + __('Page') + ' : ' + ((that.offset_annotation / that.search_limit) + 1) + '</span>';
                                                                html_annotation += '</div>';

                                                                html_annotation += '<div class="search_prev_next_td">';
                                                                if (annotation_prev_next.last && annotation_prev_next.next) {
                                                                    html_annotation += '<span class="prev_next_bt_annotation prev_next_bt next" data-href="' + annotation_prev_next.last.href + '">' + __('Last') + '</span>';
                                                                }
                                                                if (annotation_prev_next.next) {
                                                                    html_annotation += '<span class="prev_next_bt_annotation prev_next_bt next" data-href="' + annotation_prev_next.next.href + '">' + __('Next') + '</span>';
                                                                }
                                                                html_annotation += '</div>';

                                                                html_annotation += '<div style="clear:both"></div></div>';
                                                            }
                                                        }


                                                    }
                                                }
                                            }
/////////////

                                            var open = 'none';
                                            var open_array = 'block';
                                            //if (that.search_alto_open == 1) {
                                            open = 'block';
                                            open_array = 'none';
                                            // }

                                            var html = '';

                                            if (that.alto_count > 0) {
                                                html += '<div id="search_result_alto" class="search_result_title" data-type="alto">';
                                                html += '<span style="float:left" class="search_result_title_span">' + __('Full text') + ' : ' + that.alto_count + '</span>';
                                                html += '<span style="float:right; display:' + open_array + '" class="arrow_alto"><img title="' + __('Show') + '" alt="' + __('Show') + '" src="' + that.options.dir_ico + 'mini-arrow-left.png"></span>';
                                                html += '<span style="float:right; display:' + open + '" class="arrow_alto"><img title="' + __('Hide') + '" alt="' + __('Hide') + '" src="' + that.options.dir_ico + 'mini-arrow-down.png"></span>';
                                                html += '<div style="clear:both"></div>';
                                                html += '</div>';
                                                html += '<div class="search_result_text_alto" style="display:' + open + '">';
                                                html += hrml_ocr;
                                                html += '</div>';
                                            }

                                            if (that.bookmarks_count > 0) {
                                                html += '<div id="search_result_bookmarks" class="search_result_title" data-type="bookmarks">';
                                                html += '<span style="float:left" class="search_result_title_span">' + __('Bookmarks') + ' : ' + that.bookmarks_count + '</span>';
                                                html += '<span style="float:right; display:' + open_array + '" class="arrow_bookmarks"><img title="' + __('Show') + '" alt="' + __('Show') + '" src="' + that.options.dir_ico + 'mini-arrow-left.png"></span>';
                                                html += '<span style="float:right; display:' + open + '" class="arrow_bookmarks"><img title="' + __('Hide') + '" alt="' + __('Hide') + '" src="' + that.options.dir_ico + 'mini-arrow-down.png"></span>';
                                                html += '<div style="clear:both"></div>';
                                                html += '</div>';
                                                html += '<div class="search_result_text_bookmarks" style="display:' + open + '">';
                                                html += html_bookmarks;
                                                html += '</div>';
                                            }

                                            if (that.annotation_count > 0) {
                                                html += '<div id="search_result_annotation" class="search_result_title" data-type="annotation">';
                                                html += '<span style="float:left" class="search_result_title_span">' + __('Annotations') + ' : ' + that.annotation_count + '</span>';
                                                html += '<span style="float:right; display:' + open_array + '" class="arrow_annotation"><img title="' + __('Show') + '" alt="' + __('Show') + '" src="' + that.options.dir_ico + 'mini-arrow-left.png"></span>';
                                                html += '<span style="float:right; display:' + open + '" class="arrow_annotation"><img title="' + __('Hide') + '" alt="' + __('Hide') + '" src="' + that.options.dir_ico + 'mini-arrow-down.png"></span>';
                                                html += '<div style="clear:both"></div>';
                                                html += '</div>';
                                                html += '<div class="search_result_text_annotation" style="display:' + open + '">';
                                                html += html_annotation;
                                                html += '</div>';
                                            }

                                            $('#search_result_' + that.uniqkey).append(html);
                                            $('#search_wait_' + that.uniqkey).hide();
                                            $('#search_result_' + that.uniqkey).show();

                                            that.result_total();
                                            that.can_search = 1;

                                            if (that.prev_next_type == 'annotation') {
                                                setTimeout(function () {
                                                    $('.left_content').animate({
                                                        scrollTop: $("#search_result_annotation").offset().top
                                                    }, 0);
                                                }, 500);
                                            }
                                            ///
                                            that.prev_next_type = '';


                                            that.element.on('click', '.search_result_content', function (e) {
                                                e.preventDefault();
                                                var page = $(this).attr('data-page');
                                                that.viewer.set_page(page);
                                            });

                                            that.element.on('click', '.prev_next_bt_alto', function (e) {
                                                e.preventDefault();

                                                var href = $(this).attr('data-href');
                                                var offset = href.split("&offset=");
                                                var offset = offset[1].split("&q=");
                                                var offset = offset[0].split("&item_id=");
                                                that.offset = offset[0];
                                                that.search_alto_open = 1;
                                                $('#search_result_' + that.uniqkey).html('');
                                                that.search_alto();
                                            });

                                            that.element.on('click', '.prev_next_bt_annotation', function (e) {
                                                e.preventDefault();

                                                that.prev_next_type = 'annotation';
                                                var href = $(this).attr('data-href');
                                                var offset = href.split("&offset=");
                                                var offset = offset[1].split("&q=");
                                                var offset = offset[0].split("&item_id=");
                                                var offset = offset[0].split("&itemtype_id=");
                                                var offset = offset[0].split("&father_id=");

                                                that.offset_annotation = offset[0];
                                                that.search_alto_open = 1;
                                                $('#search_result_' + that.uniqkey).html('');
                                                that.search_alto();
                                            });

                                        });
                            });
                });
    }
};
ContentLeft.prototype.draw_annotations_zone = function (ID) {

    //var pic = $('.picture > .openseadragon-container .picture_highlight_div');
    //pic.append(html_zone);
};

ContentLeft.prototype.draw_annotations = function (items, _meta) {
    var html = '';

    if (_meta.elements) {
        for (var i = 0; i < items.length; i++) {
            var _elements = _meta.elements;
            var _src = items[i]._source;
            var _annotation = _src.annotation_page;
            var ID = items[i]._id;


            if (_annotation) {

                var html_zone = '';
                if (_src.annotation_zone) {
                    var num = 0;
                    $.each(_src.annotation_zone, function (key, val)
                    {
                        num++;
                        if (val) {
                            var zone = val.split(',');
                            if (zone.length == 4) {
                                html_zone += '<div class="zone_highlight_search zone_highlight_search_' + ID + '">';
                                html_zone += '<div id="zone_' + ID + '_' + num + '" class="zone_highlight " data-num="' + ID + '_' + num + '" data-id="' + ID + '" style="';
                                html_zone += 'top:' + zone[0] + '%; ';
                                html_zone += 'left:' + zone[1] + '%; ';
                                html_zone += 'width:' + zone[2] + '%; ';
                                html_zone += 'height:' + zone[3] + '%; ';
                                html_zone += '"></div>';
                                html_zone += '</div>';
                            }
                        }
                    });
                }


                html += '<div>';
                html += '<div class="search_result_content zone_highlight_search_show" data-page="' + _annotation + '" data-id="' + ID + '">';
                html += html_zone;
                html += '<div class="search_result_content_span">';

                $.each(_elements, function (elem, name)
                {

                    var exist = [];
                    exist[name] = new Array();

                    var annotation_val = _src[name];
                    if (annotation_val != '' && annotation_val != undefined) {
                        $.each(annotation_val, function (annotation_key2, annotation_value2)
                        {

                            if (annotation_value2.title) {
                                annotation_value2 = annotation_value2.title;
                                annotation_value2 = annotation_value2.replace('$$$', ' > ');
                            }
                            if (exist[name][annotation_value2] == undefined && annotation_value2) {
                                html += '<div class="search_ocr">' + __(name) + ': ' + annotation_value2 + '</div>';
                            }


                            var explode = annotation_value2.split(' > ');
                            for (var j = 0; j < explode.length; j++) {
                                exist[name][explode[j]] = 1;
                            }
                        });
                    }

                });

                html += '<br>' + __('View') + ' ' + _annotation;
                html += '</div></div></div>';
            }
        }
    }

    return html;
};


ContentLeft.prototype.draw_items = function (items) {
    var html = '';
    for (var i = 0; i < items.length; i++) {
        var item = items[i];
        html += this.template_item(item);
    }
    return html;
};

ContentLeft.prototype.template_item = function (item) {

    if (item.highlights) {
        var page = 0;
        var item_highlights = '';
        if (item.highlights.content) {
            page = jQuery.inArray(item.medianame, this.image) + 1;
            item_highlights = '....' + item.highlights.content.join('... \n ...') + '....';
        } else {
            page = item.page;
            item_highlights = item.highlights.text.join('... \n ...');
        }
    } else {
        item_highlights = '....';
        page = jQuery.inArray(item.medianame, this.image) + 1;
    }

    var html = '';
    html += '<div>';
    html += '<div class="search_result_content" data-page="' + page + '">';
    html += '<div class="search_result_content_span">';
    html += '<div class="search_ocr">' + item_highlights + '</div>';
    html += __('View') + ' ' + page;
    html += '</div></div></div>';
    return html;


};


ContentLeft.prototype.result_total = function () {
    var tt = this.alto_count + this.bookmarks_count + this.annotation_count;
    var html = '<div class="search_result_total">' + tt + ' ' + __('results for your search') + '</div>';
    $('#search_result_' + this.uniqkey).prepend(html);
};


// INFO

ContentLeft.prototype.infos_events = function () {
    this.element.off("click", '.info_title');
    var that = this;

    this.element.on('click', '.info_title', function (e) {
        e.preventDefault();
        var type = $(this).attr("data-type");
        $('.info_text_' + type).toggle();
        $('.arrow_' + type).toggle();
    });
};

ContentLeft.prototype.info = function () {

    var that = this;
    this.infos_events();
    this.open_close('info');
    this.content_left_title.html(__('Information'));

    var html = '<div class="search_result">';
    var info_content = $("#viewer_info_content").html();
    if (info_content != '') {
        html += '<div class="info_title" data-type="html_info">';
        html += '<span style="float:left" class="search_result_title_span">' + __('Document information') + '</span>';
        html += '<span style="float:right; display:none" class="arrow_html_info"><img src="' + this.options.dir_ico + 'mini-arrow-left.png" title="' + __('Show') + '" alt="' + __('Show') + '" ></span>';
        html += '<span style="float:right; display:block" class="arrow_html_info"><img src="' + this.options.dir_ico + 'mini-arrow-down.png" title="' + __('Hide') + '" alt="' + __('Hide') + '" ></span>';
        html += '<div style="clear:both"></div>';
        html += '</div>';
        html += '<div id="viewer_info" class="info_text_html_info info_content">';
        html += info_content;
        html += '</div>';
    }

    if (this.html_page != '') {
        html += '<div class="info_title" data-type="html_page">';
        html += '<span style="float:left" id="html_page_title" class="search_result_title_span"></span>';
        html += '<span style="float:right; display:none" class="arrow_html_page"><img src="' + this.options.dir_ico + 'mini-arrow-left.png"></span>';
        html += '<span style="float:right; display:block" class="arrow_html_page"><img src="' + this.options.dir_ico + 'mini-arrow-down.png"></span>';
        html += '<div style="clear:both"></div>';
        html += '</div>';
        html += '<div id="viewer_page_info"  class="info_text_html_page info_content" >';
        html += '</div>';

        html += '<div class="info_title" data-type="html_page2" id="html_page2" style="display:none">';
        html += '<span style="float:left" id="html_page_title2" class="search_result_title_span"></span>';
        html += '<span style="float:right; display:block" class="arrow_html_page2"><img src="' + this.options.dir_ico + 'mini-arrow-left.png"></span>';
        html += '<span style="float:right; display:none" class="arrow_html_page2"><img src="' + this.options.dir_ico + 'mini-arrow-down.png"></span>';
        html += '<div style="clear:both"></div>';
        html += '</div>';
        html += '<div id="viewer_page_info2"  class="info_text_html_page2 info_content" style="display:none">';
        html += '</div>';
    }

    // load custom infos
    $('.viewer_left').each(function () {

        var href = $(this).attr('data-href');
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');
        if (href) {

            html += '<div id="div_' + id + '" style="display:none" class="info_title" data-type="' + id + '">';
            html += '<span style="float:left" id="div_' + id + '_title" class="search_result_title_span">' + name + '</span>';
            html += '<span style="float:right; display:none" class="arrow_' + id + '"><img src="' + that.options.dir_ico + 'mini-arrow-left.png" title="' + __('Show') + '" alt="' + __('Show') + '" ></span>';
            html += '<span style="float:right; display:block" class="arrow_' + id + '"><img src="' + that.options.dir_ico + 'mini-arrow-down.png" title="' + __('Hide') + '" alt="' + __('Hide') + '" ></span>';
            html += '<div style="clear:both"></div>';
            html += '</div>';
            html += '<div id="' + id + '" class="info_text_' + id + ' info_content">';
            html += '</div>';
        }
    });
    html += '</div>';


    this.left_content_div.html(html);
    this.info_html_page();
    this.info_viewer_left();
};

ContentLeft.prototype.info_viewer_left = function () {
    // load custom infos
    var that = this;
    $('.viewer_left').each(function ()
    {
        var href = $(this).attr('data-href');
        var id = $(this).attr('data-id');
        if (href)
        {

            var href = encodeURIComponent(href) + '&page=' + that.page + '&item_id=' + that.options.item_id + '&image=' + that.options.image[that.page - 1] + '&type=' + that.options.viewer_type + '&language=' + that.options.language;
            var hp = encodeURI(that.links_prefix + "/api/custom?include=" + href);
            $('#' + id).hide();
            $('#' + id).load(hp);

            if ($('#' + id).html() != '') {
                $('#div_' + id).show();
                $('#' + id).show();
            }
            setTimeout(function () {
                if ($('#' + id).html() != '') {
                    $('#div_' + id).show();
                    $('#' + id).show();
                }
            }, 100);
            setTimeout(function () {
                if ($('#' + id).html() != '') {
                    $('#div_' + id).show();
                    $('#' + id).show();
                }
            }, 500);
        }
    });
};



ContentLeft.prototype.info_html_page = function () {
    if (this.html_page != '')
    {
        $('#html_page2').hide();
        this.info_html_page_content((this.page - 1), '');
        if (this.options.viewer_type == 'flip' && this.page > 1 && this.page < this.options.image.length) {
            $('#html_page2').show();
            this.info_html_page_content((this.page), '2');
        }
    }
};
ContentLeft.prototype.info_html_page_content = function (page, num) {
    if (this.html_page != '')
    {
        page = page * 1;
        var page_html = this.image[page] + '.html';
        var exist = jQuery.inArray(page_html, this.html_page) + 1;
        if (exist > 0) {
            $("#html_page_title" + num).html(__('Information page:') + ' ' + (page + 1));
            var hp = encodeURI(this.options.server + '/html/' + page_html);
            $("#viewer_page_info" + num).load(hp);
        } else {
            $("#html_page_title" + num).html(__('No Information for this page:') + ' ' + (page + 1));
            $("#viewer_page_info" + num).html('');
        }

    }
};

// COMPARATOR
ContentLeft.prototype.comparator = function () {
    this.comparator_events();
    this.open_close('comparator');
    this.content_left_title.html(__('Images comparator'));
    this.comparator_content();
};

ContentLeft.prototype.comparator_content = function () {
    var that = this
    var page = this.page * 1;
    var media = this.image[page - 1];

    $.getJSON('/api/viewer/comparator_list',
            function (data)
            {
                var html = '<div class="search_result"><div class="info_content">';
                var nb = 0;
                var exist = 0;
                if (data && data.item) {
                    $.each(data.item, function (key, val)
                    {
                        if (val.type == 'direct') {
                            if (val.item_id == that.options.item_id && val.page == page) {
                                exist = 1;
                            }
                        }

                        nb++;
                    });
                }
                html += '' + nb + ' '+__('items in comparator');

                if (exist == 0) {
                    html += '<input class="comparator_add_bt" data-page="' + page + '" type="button" value="+ ' + __('Add this page to comparator') + '">';
                } else {
                    html += '<div class="comparator_exist">' + __('already in comparator') + '</div>';
                }
                html += '<input class="comparator_bt" data-page="' + page + '"  type="button" value="' + __('Open Comparator') + '">';
                //parent_itemtype_id
                html += '</div></div>';
                that.left_content_div.html(html);
            });

    this.info_html_page();
    this.info_viewer_left();
};
ContentLeft.prototype.comparator_events = function () {
    this.element.off("click", '.comparator_bt');
    var that = this;
    this.element.on('click', '.comparator_bt', function (e) {
        e.preventDefault();
        var page = $(this).attr('data-page');

        var back = '/viewer/' + that.options.item_id + "#page=" + page + '&o=comparator';
        if (that.options.get_css == 'include' && that.options.return_url != '') {
            back = that.options.return_url + "#page=" + page + '&o=comparator';
        }

        var link = "/viewer/comparator/" + that.parent_itemtype_id + '?back=' + encodeURIComponent(back);
        //window.open(link, '_parent');
        parent.document.location.href = link;
    });

/////
    this.element.off("click", '.comparator_add_bt');
    var that = this;
    this.element.on('click', '.comparator_add_bt', function (e) {
        e.preventDefault();

        var page = $(this).attr('data-page');
        var media = that.image[page - 1];
        $.getJSON('/api/viewer/comparator_add/' + that.options.item_id + '?page=' + page + '&media=' + media);

        setTimeout(function () {
            that.comparator_content();
        }, 200);

    });
};



// VOLUMES
ContentLeft.prototype.volumes = function () {
    this.volumes_events();
    this.open_close('volume');
    this.content_left_title.html(__('volumes'));
    var html = '<div class="search_result">';
    var info_content = $("#viewer_volume_content").html();
    if (info_content != '') {
        html += '<div id="viewer_volumes" class="info_text_html_info info_content">';
        html += info_content;
        html += '</div>';
    }
    html += '</div>';
    this.left_content_div.html(html);
    this.info_html_page();
    this.info_viewer_left();
};
ContentLeft.prototype.volumes_events = function () {
    this.element.off("click", '.bt_volume');
    var that = this;
    this.element.on('click', '.bt_volume', function (e) {
        e.preventDefault();
        var href = $(this).attr('data-href');
        var target = $(this).attr('data-target');
        var link = href + '&viewer=' + that.options.viewer_type + '&o=volume';

        if (target == '_blank') {
            window.open(link, '_blank');
        } else if (target == '_parent') {
            parent.document.location.href = link;
        } else {
            document.location.href = link;
        }
    });
};

// CUSTOM BOTTOM
ContentLeft.prototype.custom_bottom = function (name, href, set_page) {
    var that = this;
    this.custom_bottom_name = name;
    this.custom_bottom_href = href;

    if (set_page != 1) {
        this.open_close('custom_bottom_' + name);
    }
    this.content_left_title.html(__(name));
    var html = '<div class="search_result">';
    html += '<div id="custom_bottom" class="info_text_html_info info_content"></div>';
    html += '</div>';

    this.left_content_div.html(html);

    if (href)
    {
        var href = encodeURIComponent(href + '/index.php') + '&page=' + that.page + '&item_id=' + that.options.item_id + '&image=' + that.options.image[that.page - 1] + '&type=' + that.options.viewer_type + '&language=' + that.options.language;
        var hp = encodeURI(that.links_prefix + "/api/custom?include=" + href);
        $('#custom_bottom').load(hp);
    }
};




/*** DOWNLOAD ****/

ContentLeft.prototype.download_events = function () {
    this.element.off("click", '.cl_print_fullpicture');
    this.element.off("click", '.cl_print_screen');
    this.element.off("click", '.cl_download_picture');
    this.element.off("click", '.cl_download_pdf');
    this.element.off("click", '.cl_download_attached_file');
    this.element.off("click", '.cl_download_pdf_page');

    var that = this;
    this.element.on('click', '.cl_print_fullpicture', function (e) {
        e.preventDefault();
        var page = $(this).attr("data-page");
        that.viewer.tracker_print(that.page);
        that._print_all(page - 1);
    });


    this.element.on('click', '.cl_download_attached_file', function (e) {
        e.preventDefault();
        var file = $(this).attr("data-file");
        that.viewer.tracker_download_file(file);
        window.open(that.links_prefix + '/viewer/' + that.options.item_id + '/download?file=' + file + '&type=attached_file');
    });


    // checkbox format : JPG / PDF
    this.element.on('click', '.cl_printfile_format_val', function (e) {
        //e.preventDefault();
        var value = $(this).attr("value");
        that.printfile_format_val(value);
    });

    // checkbox type : pdf_full
    this.element.on('click', '.cl_download_pdf_full', function (e) {
        that.download_type = 'pdf_full';
        that.printfile_type_val();
    });

    // checkbox type : pdf_page
    this.element.on('click', '.cl_download_pdf_page', function (e) {
        that.download_type = 'pdf_page';
        that.printfile_type_val();
    });

    // checkbox type : picture
    this.element.on('click', '.cl_download_picture', function (e) {
        that.download_type = 'picture';
        that.printfile_type_val();

    });
    // checkbox type : crop
    this.element.on('click', '.cl_print_screen', function (e) {
        that.download_type = 'crop';
        that.printfile_type_val();
        that.add_crop();
    });

    // download bt
    this.element.on('click', '.cl_download_bt', function (e) {
        e.preventDefault();

        if (that.download_format == 'PDF') {
            var type = $('input[name=cl_download_pdf]:checked').val();
            var file = $('input[name=cl_download_pdf]:checked').attr("data-file");
            if (type == 'full') {
                that.viewer.tracker_download(file);
                window.open(that.links_prefix + '/viewer/' + that.options.item_id + '/download?file=' + file + '&type=pdf');
            } else if (type == 'page') {

                that.viewer.tracker_download_page((that.page - 1));
                var page = that.page;
                var download_img = that.options.image[(that.page - 1)];

                if (that.options.viewer_type == 'flip' && that.page > 1 && that.page < that.options.image.length) {
                    download_img += ';' + that.options.image[(that.page)];
                    page += ',' + (that.page - 2);
                }
                window.open(that.links_prefix + '/viewer/' + that.options.item_id + '/download?file=' + file + '&type=pdf&page_name=' + download_img + '&page=' + page);
            }
        } else if (that.download_format == 'JPG') {
            if (that.download_type == 'crop') {
                that.viewer.tracker_print_screen(that.page);
                var crop = that.ias.getSelection();
                var print_img = that.picture._print_screen(crop);

                window.open(that.links_prefix + '/viewer/' + that.options.item_id + '/download?file=' + print_img + '&type=img');
            } else if (that.download_type == 'picture' || that.download_type == '') {
                var page = $('input[name=cl_download_jpg]:checked').val();
                if (that.options.viewer_type == 'flip') {
                    page = page - 1;
                }
                that.viewer.tracker_download_page(page);
                var download_img = that.options.image[(page)];
                window.open(that.links_prefix + '/viewer/' + that.options.item_id + '/download?file=' + download_img + '&type=img');
            }
        }
    });
};


ContentLeft.prototype.printfile_format_val = function (value) {
    this.remove_crop();
    this.download_format = value;
    $('#cl_printfile_format_val_' + value).prop("checked", true);
    $('.cl_printfile_format').hide();
    $('.cl_printfile_format_' + value).show();
    this.printfile_type_val();
};

ContentLeft.prototype.printfile_type_val = function () {
    this.remove_crop();
    if (this.download_type == 'crop' && this.download_format == 'JPG') {
        $('.cl_print_screen').prop("checked", true);
    } else {
        //remove crop
        $('#download_crop_content').remove();
        $('#download_crop_content_father').remove();
        //
        if (this.download_type == 'pdf_full' || (this.download_type != 'pdf_page' && this.download_format == 'PDF')) {
            $('.cl_download_pdf_full').prop("checked", true);
        } else if (this.download_type == 'pdf_page' && this.download_format == 'PDF') {
            $('.cl_download_pdf_page').prop("checked", true);
        } else if (this.download_type == 'picture' || (this.download_type != 'crop' && this.download_format == 'JPG')) {
            $('.cl_download_picture').prop("checked", true);
        }
    }
};

ContentLeft.prototype.remove_crop = function () {
    $('#download_crop_content').remove();
    $('#download_crop_content_father').remove();
    if (this.ias) {
        $('.cl_print_screen').prop("checked", false);
        this.ias.remove();
        clearInterval(this.ias_obs);
        $('.cl_share_crop_block').hide();
    }
};
ContentLeft.prototype.add_crop = function () {
    if (this.download_type == 'crop' || this.share_crop == 1) {
        var that = this;
        var num = Math.random() * 0x100000000000000;
        this.remove_crop();
        $('.cl_print_screen').prop("checked", true);

        var html = ''
        html += '<div id="download_crop" data-move="1" style="';
        html += 'top:0%; ';
        html += 'left:0%; ';
        html += 'width:100%; ';
        html += 'height:100%; ';
        html += '"></div>';
        $('.content_picture .picture ').append('<div id="download_crop_content_father"><div id="download_crop_content">' + html + '</div></div>');
        that.resize_crop();

        //crop
        var pic = $('#download_crop');
        this.ias = pic.imgAreaSelect({instance: true});
        this.ias.setSelection(120, 90, 280, 210);
        this.ias.setOptions({show: true, handles: true});
        this.ias.update();


        //crop value in txt
        $('.cl_share_crop_block').hide();
        this.ias_obs = setInterval(function () {
            if ($('#cl_share_crop_txt').length) {
                var crop = that.ias.getSelection();
                if (crop.x1 > 0 && crop.y1 > 0 && crop.width > 0 && crop.height > 0) {
                    $('.cl_share_crop_block').show();
                    var print_img = that.picture._crop_screen(crop);
                    $('#cl_share_crop_txt').val('<img src="' + that.http + print_img + '">');
                    $('#cl_share_crop_img').attr('src', that.http + print_img);
                    $('#cl_share_crop_img_href').attr('href', that.http + print_img);
                } else {
                    $('.cl_share_crop_block').hide();
                }
            }

        }, 1000);
    }
};

ContentLeft.prototype.resize_crop = function () {
    var that = this;
    var pic = $('.picture > .openseadragon-container .picture_highlight_div');
    var position = pic.position();

    var content_picture_position = $('.content_picture').position();

    if (position) {

        var top = position.top;
        if (top < 0) {
            top = 0;
        }

        var height = pic.height();
        if (height > $('.content_picture').height()) {
            height = $('.content_picture').height();
        }

        var left = position.left;
        if (left < 0) {
            left = 0;
        }

        var width = pic.width();
        if (width > $('.content_picture').width()) {
            width = $('.content_picture').width();
        }

        $('#download_crop_content').css({top: top, left: left, width: width, height: height});
    }

};

ContentLeft.prototype.download = function () {

    this.download_events();
    this.open_close('download');
    this.content_left_title.html(__('Print or download files'));

    var html = '<div id="download_content" class="info_text_html_info info_content"></div>';
    this.left_content_div.html(html);
    this.download_content();

};
ContentLeft.prototype.download_content = function () {

    var html = '';
    var page = this.page * 1;
    var that = this;

    var block_jpg = this.options.show_download_jpg + this.options.show_download_crop;
    var pdf_list = this.options.show_download_pdf_pages;
    if (pdf_list == 0) {
        pdf_list = this.options.show_download_full_pdf;
    }

    if (block_jpg > 0 || pdf_list != 0) {
        html += '<div class="content_left_block">'

        if (block_jpg > 0 && pdf_list != 0) {
            html += '<h4>' + __('File format') + '</h4>';

            html += '<div class="content_left_block_options">'
            html += '<div><label><input id="cl_printfile_format_val_PDF" class="cl_printfile_format_val" type="radio" name="cl_printfile_format" value="PDF"> ' + __('PDF') + '</label></div>';
            html += '<div><label><input id="cl_printfile_format_val_JPG" class="cl_printfile_format_val"  type="radio" name="cl_printfile_format" value="JPG"> ' + __('JPG') + '</label></div>';
            html += '</div>';
        }

        // download image page JPG

        if (block_jpg > 0) {

            html += '<div class="cl_printfile_format cl_printfile_format_JPG">';
            html += '<h4>' + __('Download') + '</h4>';

            html += '<div class="content_left_block_options">';


            if (this.options.show_download_jpg == 1) {
                html += '<div><label><input type="radio" name="cl_download_jpg" class="cl_download_picture" value="' + (page - 1) + '">'
                if (this.options.viewer_type == 'flip') {
                    html += __('Download page image:') + ' ' + page;
                } else {
                    html += __('Current page');
                }
                html += '</label></div>';

                if (this.options.viewer_type == 'flip' && page > 1 && page < this.options.image.length) {
                    html += '<div><label><input type="radio" name="cl_download_jpg" class="cl_download_picture2" value="' + (page) + '">'
                    html += __('Download page image:') + ' ' + (page + 1);
                    html += '</label></div>';
                }
            }
            if (this.options.show_download_crop == 1 && this.options.viewer_type == 'picture') {
                html += '<div><label><input type="radio" name="cl_download_jpg" class="cl_print_screen" value="' + (page - 1) + '">'
                html += __('Portion of the picture') + '</label>';
                html += '</div>';
            }
            html += '</div>';
            html += '</div>';
        }

        // download image page PDF
        if (pdf_list != 0) {
            var file_download_full_pdf = 0;
            if (pdf_list.length == 1) {
                file_download_full_pdf = 1;
            }

            html += '<div class="cl_printfile_format cl_printfile_format_PDF">';
            html += '<h4>' + __('Download') + '</h4>';
            html += '<div class="content_left_block_options">';
            $.each(pdf_list, function (src, file)
            {
                if (that.options.show_download_full_pdf != 0) {
                    file_download_title = file;
                    if (file_download_full_pdf == 1) {
                        file_download_title = __('full PDF');
                    }

                    html += '<div><label><input type="radio" name="cl_download_pdf" data-file="' + (file) + '" class="cl_download_pdf_full" value="full">'
                    html += file_download_title + '</label>';
                    html += '</div>';
                }
                if (that.options.show_download_pdf_pages != 0) {
                    var page_label = page;
                    if (that.options.viewer_type == 'flip' && page > 1 && page < that.options.image.length) {
                        page_label = page + ' & ' + (page + 1);
                    }

                    html += '<div><label><input type="radio" name="cl_download_pdf" data-file="' + (file) + '" class="cl_download_pdf_page" value="page">'
                    html += __('PDF page:') + ' ' + page_label + '</label>';
                    html += '</div>';
                }

            });
            html += '</div>';
            html += '</div>';

        }
        html += '</div>';

        html += '<input class="cl_download_bt" type="button" value="' + __('Download') + '">';
    }



    // download attached files
    var that = this;
    if (this.options.show_download_attached_files != 0) {
        var pass_attached_files = 0;
        $.each(this.options.show_download_attached_files, function (src, file)
        {
            if (pass_attached_files == 0) {
                if (block_jpg > 0 && that.options.show_download_pdf != 0) {
                    html += '<hr>';
                }
                html += '<div class="content_left_block">';
                html += '<h4>' + __('Others Downloads') + '</h4>';
            }

            html += '<div class="cl_download_attached_file" data-file="' + (file) + '">'
            html += '<span class="content_left_title_attach">' + __('Download:') + ' ' + file + '</span>';
            html += '</div>';
            pass_attached_files++;
        });
        if (pass_attached_files > 0) {
            html += '</div>';
        }
    }

    $("#download_content").html(html);


    if (this.download_format != '') {
        this.printfile_format_val(this.download_format);
    } else {
        if (pdf_list != 0) {
            this.printfile_format_val('PDF');
        } else {
            this.printfile_format_val('JPG');
        }
    }

    this.printfile_type_val();
};
ContentLeft.prototype._print_all = function (page) {
    var print_img = this.options.image[page];
    window.open(this.links_prefix + '/viewer/' + this.options.item_id + '/img_print?img=' + print_img);
};



/*** SHARE ****/

ContentLeft.prototype.share_events = function () {
    this.element.off("click", '.cl_share_permalink_bt');
    this.element.off("click", '#cl_share_crop');
    this.element.off("click", '#cl_share_crop_bt');
    this.element.off("click", '#cl_share_embeded');
    this.element.off("click", '#cl_share_embeded_color');
    this.element.off("click", '#cl_share_embeded_bt');
    this.element.off("click", '#cl_share_permalink_bt');

    var that = this;
    this.element.on('click', '#cl_share_crop', function (e) {
        //hide embeded
        $('#cl_share_embeded').prop("checked", false);
        $('.cl_share_embeded_block').hide();

        //show crop
        if ($('#cl_share_crop').prop('checked')) {
            that.share_crop = 1;
            that.add_crop();
        } else {
            that.share_crop = 0;
            that.remove_crop();
        }
    });
    this.element.on('click', '#cl_share_crop_bt', function (e) {
        e.preventDefault();
        var copyText = document.getElementById("cl_share_crop_txt");
        copyText.select();
        document.execCommand("copy");
        alert(__("Copied text"));
    });


    this.element.on('click', '#cl_share_embeded', function (e) {
        //hide crop
        $('#cl_share_crop').prop("checked", false);
        that.share_crop = 0;
        that.remove_crop();

        //show embeded
        if ($('#cl_share_embeded').prop('checked')) {
            that.share_embed();
        } else {
            $('#cl_share_embeded').prop("checked", false);
            $('.cl_share_embeded_block').hide();
        }
    });

    this.element.on('click', '#cl_share_embeded_color', function (e) {
        that.share_embed();
    });

    this.element.on('click', '#cl_share_embeded_bt', function (e) {
        e.preventDefault();
        var copyText = document.getElementById("cl_share_embeded_txt");
        copyText.select();
        document.execCommand("copy");
        alert(__("Copied text"));
    });


    this.element.on('click', '#cl_share_permalink_bt', function (e) {
        e.preventDefault();
        var copyText = document.getElementById("cl_share_permalink_txt");
        copyText.select();
        document.execCommand("copy");
        alert(__("Copied the text:") + " " + copyText.value);
    });

};

ContentLeft.prototype.share_embed = function () {
    var color = '';
    if ($('#cl_share_embeded_color').prop('checked')) {
        color = "_dark";
    }

    var url = this.http + '/assets/pages/embed.php?id=' + this.options.item_id + '&color=' + color + '&page=' + this.page;
    $('.cl_share_embeded_block').show();
    $('#cl_share_embeded_iframe').attr('src', url);
    $('#cl_share_embeded_txt').val('<iframe src="' + url + '" frameborder="0" sandbox="allow-scripts allow-popups allow-same-origin" allowfullscreen="allowfullscreen"></iframe>');
};


ContentLeft.prototype.share = function () {
    this.share_events();
    this.open_close('share');
    this.content_left_title.html(__('Share'));
    var html = '<div id="share_content" class="info_text_html_info info_content"></div>';
    this.left_content_div.html(html);
    this.share_content();
};

ContentLeft.prototype.share_content = function () {
    this.remove_crop();
    var html = '';
    var page = this.page * 1;
    var that = this;

    if (this.options.show_share_permalink != 0) {
        html += '<div class="content_left_block">'
        html += '<h4>' + __('Permalink') + '</h4>';
        html += '<div><input id="cl_share_permalink_txt" class="cl_share_permalink_txt" type="text" value="' + window.location.hostname + '/idviewer/' + that.options.item_id + '/' + page + '">';
        html += '<input id="cl_share_permalink_bt" class="cl_share_permalink_bt" type="button" value="' + __('Copy') + '"></div>';
        html += '</div>';
    }


    if (this.options.show_share_crop != 0 || this.options.show_share_embeded != 0) {
        html += '<hr><div class="content_left_block">'
        html += '<h4>' + __('Embed') + '</h4>';
        if (this.options.show_share_crop != 0 && this.options.viewer_type == 'picture') {
            html += '<div><label><input id="cl_share_crop" class="cl_share_radio" type="checkbox" name="cl_share_embed_type" value="crop"> ' + __('Portion of the picture') + '</label></div>';
        }
        if (this.options.show_share_embeded != 0) {
            html += '<div><label><input id="cl_share_embeded" class="cl_share_radio"  type="checkbox" name="cl_share_embed_type" value="iframe"> ' + __('Mini viewer') + '</label></div>';
        }
        html += '</div>';
    }
    //share crop
    if (this.options.show_share_crop != 0) {
        html += '<div class="cl_share_crop_block" style="display:none">'
        html += '<br><div class="content_left_block">'
        html += '<h4>' + __('Portion of the picture') + '</h4>';
        html += '<div>';
        html += '<div id="cl_share_crop_img_content"><a id="cl_share_crop_img_href" href="" target="_blank"><img id="cl_share_crop_img" class="cl_share_permalink_img"></a></div>';
        html += '<div><input id="cl_share_crop_txt" class="cl_share_permalink_txt" type="text" value="">';
        html += '<input id="cl_share_crop_bt" class="cl_share_permalink_bt" type="button" value="' + __('Copy') + '"></div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
    }

    //share embeded
    if (this.options.show_share_embeded != 0) {
        html += '<div class="cl_share_embeded_block" style="display:none">'
        html += '<br><div class="content_left_block">'
        html += '<h4>' + __('Mini viewer') + '</h4>';
        html += '<div>';
        html += '<div><label><input id="cl_share_embeded_color" class="cl_share_embeded_color" type="checkbox" value="dark"> ' + __('Dark version') + '</label></div>';
        html += '<div id="cl_share_embeded_content"><iframe src="" id="cl_share_embeded_iframe"></iframe></div>';
        html += '<div><input id="cl_share_embeded_txt" class="cl_share_permalink_txt" type="text" value="">';
        html += '<input id="cl_share_embeded_bt" class="cl_share_permalink_bt" type="button" value="' + __('Copy') + '"></div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
    }

    $("#share_content").html(html);
};


