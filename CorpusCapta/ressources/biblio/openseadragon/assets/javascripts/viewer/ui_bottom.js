var UiBottom = function (element, options) {
    options = options || {};
    this.element = element;

    // init ui and apps
    this.thumb = null;
    this.thumbbig = null;
    this.picture = null;
    this.flip = null;
    this.viewer = null;
    this.content_left = null;

    // params
    this.options = options;
    this.options.dir_ico = options.dir_ico || '/assets/images/viewer/';
    // action and events
    this.init_ui();
    this.setup_events();
};

// VIEWER TYPE
UiBottom.prototype.set_viewer_type = function (viewer) {
    this.options.viewer_type = viewer;
}

// EVENTS TRACKER
UiBottom.prototype.setup_events = function () {
    var that = this;

    // show thumb apps
    //this.element.on('click', '.BtThumb', function(e) {
    this.BtThumb.on('click', function (e) {
        e.preventDefault();
        that.viewer_control.hide();
        that.hide_elements();
        that.viewer.set_viewer_type('thumb');

        that.viewer.tracker_mode(__('Thumbnails'));
    });

    // show thumbbig apps
    //this.element.on('click', '.BtThumbBig', function(e) {
    this.BtThumbBig.on('click', function (e) {
        e.preventDefault();

        that.viewer_control.hide();
        that.hide_elements();
        that.viewer.set_viewer_type('thumbbig');
        that.viewer.tracker_mode(__('Facing pages'));
    });

    // show flip apps
    this.BtFlip.on('click', function (e) {
        e.preventDefault();

        that.viewer_control.hide();
        that.viewer_control_page.show();
        that.hide_elements();

        that.viewer.set_viewer_type('flip');
        that.viewer.tracker_mode(__('Flipping'));
    });

    // show picture apps
    this.BtViewer.on('click', function (e) {
        e.preventDefault();

        that.viewer_control.show();
        that.hide_elements();

        that.viewer.set_viewer_type('picture');
        that.viewer.tracker_mode(__('Picture'));
    });

    // show/hide content_left bookmark
    this.BtBookmark.on('click', function (e) {
        e.preventDefault();
        that.content_left.bookmark();
        //that.viewer.tracker_action(__('Bookmarks'));
    });

    // show/hide content_left bookmark
    this.BtDownload.on('click', function (e) {
        e.preventDefault();
        that.content_left.download();
    });

    // show/hide content_left shate
    this.BtShare.on('click', function (e) {
        e.preventDefault();
        that.content_left.share();
    });

    // show/hide content_left Comparator
    this.BtComparator.on('click', function (e) {
        e.preventDefault();
        that.content_left.comparator();
    });

    // show/hide content_left volume
    this.BtVolume.on('click', function (e) {
        e.preventDefault();
        that.content_left.volumes();
    });

    // show/hide content_left ocr
    this.BtOcr.on('click', function (e) {
        e.preventDefault();
        that.content_left.ocr();
    });

    // show/hide content_left annotate
    this.BtAnnotate.on('click', function (e) {
        e.preventDefault();
        that.content_left.annotate();
    });

    // show/hide content_left search
    this.BtSearch.on('click', function (e) {
        e.preventDefault();
        that.content_left.search();
        //that.viewer.tracker_action(__('Search'));
    });

    // show/hide content_left info
    this.BtInfo.on('click', function (e) {
        e.preventDefault();
        that.content_left.info();
        //that.viewer.tracker_action(__('Info'));
    });

    // Zoom Optimize
    this.BtZoomOptimize.on('click', function (e) {
        e.preventDefault();
        that.picture.zoom_optimize();
    });

    // if (this.options.is_mobile == 1) {
    // Zoom in
    this.BtZoomMobileIn.on('click', function (e) {
        e.preventDefault();
        that.picture.zoom_in();
    });

    // Zoom out
    this.BtZoomMobileOut.on('click', function (e) {
        e.preventDefault();
        that.picture.zoom_out();
    });
    // }


    // hide/show navbox
    this.Btnavbox.on('click', function (e) {
        e.preventDefault();
        that.picture._show_hide_navbox();
    });

    // hide/show scale
    this.BtScale.on('click', function (e) {
        e.preventDefault();
        that.picture._show_hide_scale();
    });



    // print all
    /*
     this.BtPrintAll.on('click', function(e) {
     e.preventDefault();
     that.picture._print_all();
     });
     
     // print screen
     this.BtPrintScreen.on('click', function(e) {
     e.preventDefault();
     that.picture._print_screen();
     });
     */
    // invert
    this.BtInvert.on('click', function (e) {
        e.preventDefault();
        that.picture.set_reverse();
    });

    // rotate
    this.BtRotate.on('click', function (e) {
        e.preventDefault();

        if (that.rotate_action.is(":visible"))
        {
            that.rotate_action.hide();
        } else
        {
            that.rotate_action.show();
        }
    });
    this.rotate_action.mouseleave(function ()
    {
        that.rotate_action.hide();
    });
    this.rotate_bt.on('click', function (e) {
        e.preventDefault();
        var deg = $(this).attr('data');

        that.picture.set_rotate(deg);

        /*
         var rotate_val = $("#rotate").val();
         var deg = $(this).attr('data');
         if(deg!=rotate_val){
         $("#rotate").val(deg);
         $('#rotate').change(); 
         }*/
    });

    // show/hide contrast
    this.BtContrast.on('click', function (e) {
        e.preventDefault();
        if (that.contrast_action.is(":visible"))
        {
            that.contrast_action.hide();
        } else
        {
            that.contrast_action.show();
        }
    });
    // hide contrast
    $(that.contrast_action).mouseleave(function ()
    {
        that.contrast_action.hide();
    });
    // adjust contrast
    this.slider_contrast.slider(
            {
                orientation: "vertical",
                range: "min",
                min: 0,
                max: 155,
                step: 1,
                value: 0,
                slide: function (event, ui) {
                    that.contrast_value.val(ui.value);
                },
                stop: function (event, ui)
                {
                    that.picture.set_contrast(ui.value);
                }
            });

    //page number
    this.PageValue.change(function ()
    {
        that.viewer.set_page(that.PageValue.val());
    });
    //page prev / next
    this.page_prev_mini.on('click', function (e) {
        e.preventDefault();

        if (that.options.viewer_type == 'flip' && Number(that.PageValue.val()) > 2) {
            that.viewer.set_page(Number(that.PageValue.val()) - 2);
        } else {
            that.viewer.set_page(Number(that.PageValue.val()) - 1);
        }

    });
    this.page_next_mini.on('click', function (e) {
        e.preventDefault();
        if (that.options.viewer_type == 'flip' && Number(that.PageValue.val()) > 1) {
            that.viewer.set_page(Number(that.PageValue.val()) + 2);
        } else {
            that.viewer.set_page(Number(that.PageValue.val()) + 1);
        }
    });
};

// LINK TO VIEWER
UiBottom.prototype.set_viewer = function (viewer) {
    this.viewer = viewer;
}
// LINK TO THUMB
UiBottom.prototype.set_thumb = function (thumb) {
    this.thumb = thumb;
}
// LINK TO THUMB
UiBottom.prototype.set_thumbbig = function (thumbbig) {
    this.thumbbig = thumbbig;
}
// LINK TO PICTURE
UiBottom.prototype.set_picture = function (picture) {
    this.picture = picture;
}
// LINK TO FLIP
UiBottom.prototype.set_flip = function (flip) {
    this.flip = flip;
}
// LINK TO CONTENT LEFT
UiBottom.prototype.set_content_left = function (content_left) {
    this.content_left = content_left;
}


// CHANGE PAGE
UiBottom.prototype.set_page = function (page) {
    this.options.page = page;
    this.PageValue.val(this.options.page);

    this.show_hide_prev_next();
}

// SHOW/HIDE PREV/NEXT MINI
UiBottom.prototype.show_hide_prev_next = function (page) {
    this.page_prev_mini.hide();
    this.page_next_mini.hide();
    if (this.options.page > 1)
    {
        this.page_prev_mini.show();
    }

    if (this.options.page < this.options.file_count)
    {
        this.page_next_mini.show();
    }
}

// UI TRACER
UiBottom.prototype.init_ui = function () {

    var BtInfo_title = 'title="' + __('View information') + '"';
    var BtThumb_title = 'title="' + __('Thumbnails') + '"';
    var BtViewer_title = 'title="' + __('Picture') + '"';
    var BtSearch_title = 'title="' + __('Search') + '"';
    var BtBookmark_title = 'title="' + __('Show bookmarks') + '"';
    var BtDownload_title = 'title="' + __('Print or Download files') + '"';
    var BtShare_title = 'title="' + __('Share') + '"';
    var BtVolume_title = 'title="' + __('All volumes') + '"';
    var BtComparator_title = 'title="' + __('Comparator') + '"';
    if (this.options.is_mobile == 1) {
        BtInfo_title = '';
        BtThumb_title = '';
        BtViewer_title = '';
        BtSearch_title = '';
        BtBookmark_title = '';
        BtDownload_title = '';
        BtShare_title = '';
        BtComparator_title = '';
        BtVolume_title = '';
    }
    //bottom_menu content
    this.bottom_menu_content = $('<div class="bottom_menu_content">');
    this.element.append(this.bottom_menu_content);

    // create elements
    this.type_view = $('<div class="bottom_bt_left type_view">');
    //this.element.append(this.type_view);
    this.type_view.appendTo(this.bottom_menu_content);

    //this.BtInfo = $('<img src="'+this.options.dir_ico+'info.png" class="button_type_view btalt BtInfo" ' + BtInfo_title + '>');
    //this.BtInfo.appendTo(this.type_view);

    this.BtThumb = $('<img src="' + this.options.dir_ico + 'thumb.png" class="button_type_view btalt BtThumb" ' + BtThumb_title + '>');
    this.BtThumb.appendTo(this.type_view);

    this.BtThumbBig = $('<img src="' + this.options.dir_ico + 'thumb-big.png" class="button_type_view btalt BtThumbBig" title="' + __('Facing pages') + '">');
    this.BtThumbBig.appendTo(this.type_view);

    this.BtFlip = $('<img src="' + this.options.dir_ico + 'flip.png" class="button_type_view btalt BtFlip" title="' + __('Flipping') + '">');
    this.BtFlip.appendTo(this.type_view);

    this.BtViewer = $('<img src="' + this.options.dir_ico + 'view.png" class="button_type_view btalt BtViewer" ' + BtViewer_title + '>');
    this.BtViewer.appendTo(this.type_view);

    //////////////
    this.BtInfo = $('<div class="bottom_bt_left flip_control BtInfo"><img src="' + this.options.dir_ico + 'info.png" class="button_type_view btalt " ' + BtInfo_title + '></div>');
    this.BtInfo.appendTo(this.bottom_menu_content);

    this.BtVolume = $('<div class="bottom_bt_left flip_control BtVolume"><img src="' + this.options.dir_ico + 'volumes.png" class="button btalt" ' + BtVolume_title + '></div>');
    //this.element.append(this.BtVolume);
    this.BtVolume.appendTo(this.bottom_menu_content);

    var that = this;
    $('.bt_bottom').each(function ()
    {
        var BtCustom_bottom_title = 'title="' + __($(this).attr('data-name')) + '"';
        var BtCustom_bottom = $('<div class="bottom_bt_left flip_control Custom_bottom" data-name="'+$(this).attr('data-name')+'" data-href="'+$(this).attr('data-href')+'"><img src="' + $(this).attr('data-href')+'/ico.png" class="button btalt" ' + BtCustom_bottom_title + '></div>');
        BtCustom_bottom.appendTo(that.bottom_menu_content);

        // show/hide content_left volume
        BtCustom_bottom.on('click', function (e) {
            e.preventDefault();
            that.content_left.custom_bottom($(this).attr('data-name'),$(this).attr('data-href'),0);
        });

    });


    this.BtDownload = $('<div class="bottom_bt_left flip_control BtDownload"><img src="' + this.options.dir_ico + 'download.png" class="button btalt" ' + BtDownload_title + '></div>');
    //this.element.append(this.BtDownload);
    this.BtDownload.appendTo(this.bottom_menu_content);

    this.BtShare = $('<div class="bottom_bt_left flip_control BtShare"><img src="' + this.options.dir_ico + 'share.png" class="button btalt" ' + BtShare_title + '></div>');
    this.BtShare.appendTo(this.bottom_menu_content);

    this.BtComparator = $('<div class="bottom_bt_left flip_control BtComparator"><img src="' + this.options.dir_ico + 'comparator.png" class="button btalt" ' + BtComparator_title + '></div>');
    this.BtComparator.appendTo(this.bottom_menu_content);

    this.BtBookmark = $('<div class="bottom_bt_left flip_control BtBookmark"><img src="' + this.options.dir_ico + 'bookmark.png" class="button btalt" ' + BtBookmark_title + '></div>');
    //this.element.append(this.BtBookmark);
    this.BtBookmark.appendTo(this.bottom_menu_content);

    this.BtSearch = $('<div class="bottom_bt_left flip_control BtSearch"><img src="' + this.options.dir_ico + 'search.png" class="button btalt" ' + BtSearch_title + '"></div>');
    //this.element.append(this.BtSearch);
    this.BtSearch.appendTo(this.bottom_menu_content);

    this.BtOcr = $('<div class="bottom_bt_left viewer_control flip_control BtOcr"><img src="' + this.options.dir_ico + 'ocr.png"  class="button btalt" title="' + __('Show Text') + '" /></div>');
    //this.element.append(this.BtOcr);
    this.BtOcr.appendTo(this.bottom_menu_content);

    this.BtAnnotate = $('<div class="bottom_bt_left viewer_control flip_control BtAnnotate"><img src="' + this.options.dir_ico + 'annotate.png" class="button btalt" title="' + __('Show Annotations') + '" /></div>');
    //this.element.append(this.BtAnnotate);
    this.BtAnnotate.appendTo(this.bottom_menu_content);

    this.BtInvert = $('<div class="bottom_bt viewer_control BtInvert"><img src="' + this.options.dir_ico + 'invert.png" class="button btalt" title="' + __('Reverse') + '" /></div>');
    //this.element.append(this.BtInvert);
    this.BtInvert.appendTo(this.bottom_menu_content);

    this.BtContrast = $('<div class="bottom_bt viewer_control BtContrast"><img src="' + this.options.dir_ico + 'contrast.png" class="button btalt" title="' + __('Adjust the contrast') + '"><div class="contrast_action" style="display:none"><div class="slider-contrast" style="height: 200px;"></div><input type="text" class="contrast-value" value="0"></div></div>');
    //this.element.append(this.BtContrast);
    this.BtContrast.appendTo(this.bottom_menu_content);

    this.BtScale = $('<div class="bottom_bt viewer_control BtScale"><img src="' + this.options.dir_ico + 'map-scale.png" class="button btalt" title="' + __('Show/Hide Scale') + '"></div>');
    //this.element.append(this.BtScale);
    this.BtScale.appendTo(this.bottom_menu_content);

    this.BtRotate = $('<div class="bottom_bt viewer_control BtRotate"><img src="' + this.options.dir_ico + 'rotate.png" class="button btalt" title="' + __('Rotate') + '"><div class="rotate_action" style="display:none"><div class="rotate_bt" data="0">0°</div><div class="rotate_bt" data="90">90°</div><div class="rotate_bt" data="180">180°</div><div class="rotate_bt" data="-90">-90°</div></div></div>');
    //this.element.append(this.BtRotate);
    this.BtRotate.appendTo(this.bottom_menu_content);

    this.Btnavbox = $('<div class="bottom_bt viewer_control Btnavbox"><img src="' + this.options.dir_ico + 'navbox.png" class="button btalt" title="' + __('Show/Hide information boxes') + '"></div>');
    //this.element.append(this.Btnavbox);
    this.Btnavbox.appendTo(this.bottom_menu_content);

    this.BtZoomOptimize = $('<div class="bottom_bt viewer_control BtZoomOptimize"><img src="' + this.options.dir_ico + 'zoom-optimize.png" class="button btalt"  title="' + __('optimized Zoom') + '"></div>');
    //this.element.append(this.BtZoomOptimize);
    this.BtZoomOptimize.appendTo(this.bottom_menu_content);

    /////////////
    /*
     this.type_share = $('<div class="bottom_bt_left viewer_control type_share">');
     this.element.append(this.type_share);
     
     this.BtPrintAll = $('<img src="/assets/images/viewer/print.png" class="button btalt BtPrintAll" title="' + __('Print the fullpicture') + '">');
     this.BtPrintAll.appendTo(this.type_share);
     
     this.BtPrintScreen = $('<img src="/assets/images/viewer/printscreen.png" class="button btalt BtPrintScreen" title="' + __('Print visible portion of the picture') + '">');
     this.BtPrintScreen.appendTo(this.type_share);
     */
    /////////////

    var txt_page = __('Page');
    if (this.options.is_mobile == 1) {
        var txt_page = "";
    }

    //if (this.options.is_mobile == 1) {
    this.type_zoom = $('<div class="bottom_bt_left viewer_control type_zoom">');
    //this.element.append(this.type_zoom);
    this.type_zoom.appendTo(this.bottom_menu_content);

    this.BtZoomMobileOut = $('<img src="' + this.options.dir_ico + 'zoom-out-bottom.png" class="button btalt"  title="' + __('Zoom out') + '" border="0" id="zoomOut">');
    this.BtZoomMobileOut.appendTo(this.type_zoom);

    this.BtZoomMobileIn = $('<img src="' + this.options.dir_ico + 'zoom-in-bottom.png" class="button btalt"  title="' + __('Zoom in') + '" border="0" id="zoomIn">');
    this.BtZoomMobileIn.appendTo(this.type_zoom);
    //}




    if (this.options.direction == 'RTL') {
        this.Page = $('<div class="bottom_bt_right viewer_control viewer_control_page"><img src="' + this.options.dir_ico + 'mini-arrow-left.png" class="button btalt page_next_mini"><span class="page_control"><input type="text" class="page" value="' + this.options.page + '">/' + this.options.file_count + '</span><img src="' + this.options.dir_ico + 'mini-arrow-right.png" class="button btalt page_prev_mini"></div>');
    } else {
        this.Page = $('<div class="bottom_bt_right viewer_control viewer_control_page"><img title="' + __('Prev') + '" alt="' + __('Prev') + '" src="' + this.options.dir_ico + 'mini-arrow-left.png" class="button btalt page_prev_mini"><span class="page_control"><input type="text" class="page" value="' + this.options.page + '">/' + this.options.file_count + '</span><img src="' + this.options.dir_ico + 'mini-arrow-right.png" title="' + __('Next') + '" alt="' + __('Next') + '" class="button btalt page_next_mini"></div>');
    }

    //this.element.append(this.Page);
    this.Page.appendTo(this.bottom_menu_content);

    // create link element
    this.PageValue = this.Page.find(".page");
    this.page_prev_mini = this.Page.find(".page_prev_mini");
    this.page_next_mini = this.Page.find(".page_next_mini");

    this.contrast_action = this.BtContrast.find(".contrast_action");
    this.slider_contrast = this.BtContrast.find(".slider-contrast");
    this.contrast_value = this.BtContrast.find(".contrast-value");

    this.rotate_action = this.BtRotate.find(".rotate_action");
    this.rotate_bt = this.BtRotate.find(".rotate_bt");

    this.viewer_control = this.element.find(".viewer_control");
    this.viewer_control_page = this.element.find(".viewer_control_page");

    this.hide_elements();

    // alt

    if ($(window).width() >= 1024) {
        $(".btalt").tooltip(
                {
                    show: {duration: 1000},
                    position: {
                        my: "center top+10", at: "center bottom", collision: "flipfit",
                        using: function (position, feedback) {
                            $(this).css(position);
                            $("<div>")
                                    .addClass("arrow")
                                    .addClass(feedback.vertical)
                                    .addClass(feedback.horizontal)
                                    .appendTo(this);
                        }
                    }
                });
    }
};

// UI HIDE ELEMENT
UiBottom.prototype.hide_elements = function () {
    /// hide elements
    if (this.options.description == '') {
        this.BtInfo.hide();
    }
    if (this.options.file_count < 2) {
        this.BtThumb.hide();
        this.BtThumbBig.hide();
        this.Page.hide();
    }
    if (this.options.flipping == 0 || this.options.is_mobile == 1 || this.options.file_list < 4) {
        this.BtFlip.hide();
    }
    if (this.options.is_mobile == 1) {
        this.BtContrast.hide();
        this.BtComparator.hide();
        this.Btnavbox.hide();
        this.BtScale.hide();
        this.BtZoomOptimize.hide();
        this.BtThumbBig.hide();
        this.BtFlip.hide();
        this.BtRotate.hide();
        this.BtInvert.hide();
        //this.type_share.hide();
        this.BtOcr.hide();

    }
    if (this.options.show_invert == 0) {
        this.BtInvert.hide();
    }
    if (this.options.show_download_crop == 0 && this.options.show_download_pdf_pages == 0 && this.options.show_download_full_pdf == 0 && this.options.show_download_jpg == 0 && this.options.show_download_attached_files == 0) {
        this.BtDownload.hide();
    }
    if (this.options.show_share_permalink == 0 && this.options.show_share_crop == 0 && this.options.show_share_embeded == 0) {
        this.BtShare.hide();
    }
    /* if (this.options.show_print_all == 0) {
     this.BtPrintAll.hide();
     }
     if (this.options.show_print_screen == 0) {
     this.BtPrintScreen.hide();
     }*/
    if (this.options.show_rotate == 0) {
        this.BtRotate.hide();
    }


    if (this.options.show_thumb == 0) {
        this.BtThumb.hide();
    }
    if (this.options.show_bigthumb == 0) {
        this.BtThumbBig.hide();
    }
    if (this.options.show_scale == 0) {
        this.BtScale.hide();
    }
    if (this.options.show_contrast == 0) {
        this.BtContrast.hide();
    }
    if (this.options.show_comparator == 0) {
        this.BtComparator.hide();
    }
    if (this.options.show_optimize == 0) {
        this.BtZoomOptimize.hide();
    }
    if (this.options.show_annotate == 0) {
        this.BtAnnotate.hide();
    }
    if (this.options.show_bookmarks == 0) {
        this.BtBookmark.hide();
    }
    if (this.options.volume == '' || this.options.show_volume == 0) {
        this.BtVolume.hide();
    }
    if (this.options.show_ocr == 0) {
        this.BtOcr.hide();
    }
    if (this.options.show_search == 0) {
        this.BtSearch.hide();
    }

    this.show_hide_prev_next();
};
