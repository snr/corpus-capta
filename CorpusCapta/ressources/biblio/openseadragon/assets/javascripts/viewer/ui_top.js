var UiTop = function(element, options) {
    options = options || {};
    this.element = element;

    // params
    this.options = options;

    // action and events
    this.init_ui();
    this.setup_events();
};

// EVENTS TRACKER
UiTop.prototype.setup_events = function() {
    var that = this;

    this.element.on('click', '.close_viewer', function(e) {
        e.preventDefault();

        if(that.options.return_url.indexOf("?") > -1) {
            window.location.href = that.options.return_url + '&offset=' + that.options.current_offset;
        }else{
            window.location.href = that.options.return_url + '?offset=' + that.options.current_offset;
        }
    });

};

// prev_next_bt_item
UiTop.prototype.prev_next_bt_record = function(bt, offset) {

    //var q = this.options.prev_next_search_request;
    var q = $('#content').attr('search');
    var that = this;
    if (q && offset > 0) {
        $.getJSON('/api/items' + q,
                {
                    offset: offset - 1
                },
        function(data)
        {
            var href = "";
            var title = ""
            if (data && data.items) {
                $.each(data.items, function(key, val)
                {
                    var itemid = val['_id'];
                    var get = '';
                    if(that.options.get_dir_ico){
                        get+='&dir_ico='+that.options.get_dir_ico;
                    }
                    if(that.options.get_css){
                        get+='&css-name='+that.options.get_css;
                    }
                    if(that.options.height_top){
                        get+='&height_top='+that.options.height_top;
                    }
                    href = '/viewer/' + itemid + '/?offset=' + offset+get;
                    var item = val['_source'];
                    title = item['Title'];
                });

                if (href != '') {
                    $('.' + bt + '_record').attr('href', href);
                    $('.' + bt + '_record').attr('alt', title);
                    $('.' + bt + '_record').attr('title', title);
                    $('.' + bt + '_record').show();
                }

            }

        });
    }
}

// RESIZE
UiTop.prototype.set_resize = function(w) {

    if (this.options.collaborate == 1) {
        this.viewer_title.width(w - 200 - this.BtCollaborate.width());
    }
    else {
        this.viewer_title.width(w - 200);
    }
}


// UI TRACER
UiTop.prototype.init_ui = function() {
    if (this.options.fullscreen == 1) {
        this.element.append('<div title="' + __('Close') + '" alt="' + __('Close') + '" class="close_viewer"></div>');
    }
    else {
        this.element.append('<div class="open_viewer"></div>');
    }

    if (this.options.prev_next_search_request != '') {
        this.element.append('<a href="" class="next_record"  title="' + __('Next Document') + '" alt="' + __('Next Document') + '"></a>');
        this.element.append('<a href="" class="prev_record"  title="' + __('Previous Document') + '" alt="' + __('Previous Document') + '"></a>');

        this.prev_next_bt_record('next', Number(this.options.current_offset) + 1);
        this.prev_next_bt_record('prev', Number(this.options.current_offset) - 1);
    }

    this.element.append('<div class="title">' + this.options.title + '</div>');

    this.viewer_title = this.element.find(".title");


    var BtCollaborate = $('<div class="BtCollaborate">Mode Collaboration</div>');
    this.element.append(BtCollaborate);

    if (this.options.is_mobile == 1 || this.options.collaborate == 0) {
        BtCollaborate.hide();
    }
};


