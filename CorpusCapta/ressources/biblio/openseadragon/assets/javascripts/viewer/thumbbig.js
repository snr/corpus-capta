var ThumbBig = function (element, options, json_data) {
    options = options || {};
    this.element = element;
    this.json_data = json_data;

    // init ui and apps
    this.viewer = null;
    this.picture = null;
    this.flip = null;
    this.ui_bottom = null;

    // params
    this.options = options;
    this.options.image_width = options.image_width || 277;
    this.options.image_height = options.image_height || 370;
    this.options.viewer_type = options.viewer_type || 'picture';
    this.options.direction = options.direction || 'direction';
    this.options.image_params = options.image_params || '';

    this.options.position = false;
    if (this.options.image_params != '') {
        if (this.options.image_params.position) {
            this.options.position = this.options.image_params.position;
        }
    }

    this.uniqkey = Math.random() * 0x100000000000000;
    this.thumb_create = 0;

    // action and events
    this.setup_events();
};

// EVENTS TRACKER
ThumbBig.prototype.setup_events = function () {
    var that = this;

    // change page
    this.element.on('click', '.thumbimg-action', function (e) {
        e.preventDefault();
        var page = $(this).attr('data');

        that.viewer.set_viewer_type(that.options.viewer_type);
        that.timewait = 1200;

        if (that.options.viewer_type == 'picture')
        {
            that.ui_bottom.viewer_control.show();
            that.timewait = 1000;
        } else
        {
            that.ui_bottom.viewer_control_page.show();
        }

        that.ui_bottom.hide_elements();

        setTimeout(function ()
        {
            that.viewer.set_page(page);
        }, that.timewait);
    });
};

// LINK TO VIEWER
ThumbBig.prototype.set_viewer = function (viewer) {
    this.viewer = viewer;
}
// LINK TO PICTURE
ThumbBig.prototype.set_picture = function (picture) {
    this.picture = picture;
}
// LINK TO FLIP
ThumbBig.prototype.set_flip = function (flip) {
    this.flip = flip;
}
// LINK TO BOTTOM
ThumbBig.prototype.set_ui_bottom = function (ui_bottom) {
    this.ui_bottom = ui_bottom;
}

// CHANGE PAGE
ThumbBig.prototype.set_page = function (page) {
    if (this.options.page != page) {
        this.options.page = page;
        this.thumbnail_select();
    }
}

// CHANGE VIEWER TYPE
ThumbBig.prototype.set_viewer_type = function (viewer) {
    this.options.viewer_type = viewer;
}

// CREATE THUMBNAIL LIST 
ThumbBig.prototype.create_thumbnail = function () {

    if (this.thumb_create == 0) {
        this.thumb_create = 1;

        this.images_number = 0;
        var response = this.json_data['tiles'];
        var that = this;
        this.tileSources = [];
        this.loaded = [];
        this.images = new Array();

        this.image_width = false;
        this.image_height = false;
        this.image_ratio = false;

        $.each(response, function (src, val)
        {

            if (val['@context'] == undefined) {
                that.images[that.images_number] = src;
                that.loaded[that.images_number] = 0;
                that.tileSources[that.images_number] = {
                    height: val['height'],
                    width: val['width'],
                    tileSize: val['tile_size']['w'],
                    maxLevel: val['resolutions'],
                    getTileUrl: function (level, x, y) {
                        var zoom = (level - val['resolutions']) * -1;
                        return that.options.server + '/tiles/' + src + '/' + zoom + "/" + x + "/" + y + "." + val['extension'];
                    }
                }
            } else {
                that.tileSources[that.images_number] = {
                    "@context": val['@context'],
                    "@id": val['@id'],
                    //"identifier": val['identifier'],
                    "height": val['height'],
                    "width": val['width'],
                    "profile": val['profile'],
                    "protocol": val['protocol'],
                    // "tilesUrl": val['tilesUrl'],
                    "tiles": [{
                            "scaleFactors": val['tiles']['scaleFactors'],
                            "width": val['tiles']['width']
                        }]
                };
            }


            that.images_number++;

            // beter image size
            if (that.image_ratio == false) {
                if (val['height'] >= val['width']) {
                    that.image_ratio = 1;
                } else {
                    that.image_ratio = 0;
                }
            }

            if (that.image_ratio == 1) {
                if (that.image_width == false || that.image_width < val['width']) {
                    that.image_width = val['width'];
                }
                if (that.image_height == false || that.image_height > val['height']) {
                    that.image_height = val['height'];
                }
            } else {
                if (that.image_width == false || that.image_width > val['width']) {
                    that.image_width = val['width'];
                }
                if (that.image_height == false || that.image_height < val['height']) {
                    that.image_height = val['height'];
                }
            }
        });
        ///

        this.view_thumbnail();
    }
};
// RECREATE THUMBNAIL LIST 
ThumbBig.prototype.recreate_thumbnail = function () {
    this.thumb_create = 0;
    this.create_thumbnail();
};


// CREATE THUMBNAIL BOX
ThumbBig.prototype.view_thumbnail = function () {

    var float = 'left';
    if (this.options.direction == 'RTL') {
        var float = 'right';
    }


    var html = '<div><div style="width:100%;"><div style="width:50%; float:' + float + ';">&nbsp;</div>';
    this.mobile_class = '';
    this.position_thumb = [];

    this.ratio_img = this.image_width / this.image_height;

    // plus large que haut
    if (this.ratio_img > 1)
    {
        this.HEI = this.element.height();
        this.WID = this.ratio_img * this.HEI - 160;
    } else
    {
        this.WID = this.element.width() / 2;
        this.HEI = this.WID / this.ratio_img;
    }
    this.WID = this.WID - 140;
    this.HEI = this.HEI - 80;

    //this.WID = this.element.width() / 2 - 80;
    //this.HEI = this.element.height() - 80;

    if (this.WID > this.element.width() / 2) {
        this.WID = this.element.width() / 2 - 140;
    }
    if (this.HEI > this.element.height() / 2) {
        this.HEI = this.element.height() - 80;
    }

    this.array_thumbs = new Array();
    this.array_thumbs_load = new Array();
    this.content_thumb_w = this.element.width();

    this.thumb_w = this.WID + 40;
    this.thumb_h = this.HEI + 80;

    this.thumb_by_line = 2;
    this.thumb_line = 0;

    this.lr = 'left';
    this.empty_div = 0;

    var i_ligne = 0;


    for (var i = 0; i < this.images_number; i++)
    {
        if (this.options.position[i]) {

            html += '<div style="clear:both"></div></div><div style="clear:both"></div><div style="width:100%;">';
            this.thumb_line++;

            this.align = this.options.position[i];

            var width_box = '50';
            var width_box_multipl = 1;
            var width_box_add = 0;
            var style = "";
            if (this.align == "center") {
                width_box = '100';
                width_box_multipl = 2;
                width_box_add = 30;
                this.align = "left; ";
                style = 'style="margin:20px 15px 20px 15px;"';
            }

            html += '<div style="width:' + width_box + '%; float:' + this.align + '; height:' + this.thumb_h + 'px;"><div class="thumbbig" style="float:left;">';
            html += '<div id="thumbbigpage' + this.uniqkey + '_' + i + '" style="display:none">';
            html += '<div class="thumbbigimg" id="thumbimg' + this.uniqkey + '_' + i + '" ' + style + '>';
            html += '<a href="#" class="thumbimg-action"  data="' + (i + 1) + '">';
            html += '<div id="thumbimg_content' + this.uniqkey + '_' + i + '" style="width:' + (this.WID + width_box_add) * width_box_multipl + 'px; height:' + this.HEI + 'px;"></div>';
            html += '</a>';
            html += '</div>';
            html += i + 1;
            html += '</div>';
            html += '<div style="clear:both"></div></div><div style="clear:both"></div></div>';

            html += '<div style="clear:both"></div></div><div style="clear:both"></div><div style="width:100%;">';
            this.array_thumbs[i] = (this.thumb_line - 1) * this.thumb_h;
            this.array_thumbs_load[i] = 0;

            this.lr = 'right';


            i_ligne = 0;


        } else {
            if (this.thumb_line == 0) {
                this.thumb_line++;
            }
            if ((i_ligne) % this.thumb_by_line == 1)
            {
                html += '<div style="clear:both"></div></div><div style="clear:both"></div><div style="width:100%;">';
                this.thumb_line++;
            }


            html += '<div style="width:50%; float:' + float + '; height:' + this.thumb_h + 'px;"><div class="thumbbig" style=" float:' + this.lr + '">';
            html += '<div id="thumbbigpage' + this.uniqkey + '_' + i + '" style="display:none">';
            html += '<div class="thumbbigimg" id="thumbimg' + this.uniqkey + '_' + i + '">';
            html += '<a href="#" class="thumbimg-action"  data="' + (i + 1) + '">';
            html += '<div id="thumbimg_content' + this.uniqkey + '_' + i + '" style="width:' + this.WID + 'px; height:' + this.HEI + 'px;"></div>';
            html += '</a>';
            html += '</div>';
            html += i + 1;
            html += '</div>';
            html += '<div style="clear:both"></div></div><div style="clear:both"></div></div>';

            this.array_thumbs[i] = (this.thumb_line - 1) * this.thumb_h;
            this.array_thumbs_load[i] = 0;

            if (this.lr == 'right')
            {
                this.lr = 'left';
            } else {
                this.lr = 'right';
            }

        }
        this.position_thumb[i] = this.thumb_h * (this.thumb_line - 1);


        i_ligne++;
    }

    this.element.html(html);

    var that = this;
    this.element.scroll(function ()
    {
        clearTimeout($.data(this, 'scrollTimer'));
        $.data(this, 'scrollTimer', setTimeout(function () {
            that.load_thumbnails();
        }, 500));
    });

    //position  
    this.load_thumbnails();
    this.thumbnail_select();
};

// LOAD THUMBNAIL IN BOX
ThumbBig.prototype.load_thumbnails = function () {
    var scroll_thumbs = this.element.scrollTop() + this.element.height();
    for (key in this.array_thumbs)
    {
        if (scroll_thumbs > this.array_thumbs[key] && scroll_thumbs < (this.array_thumbs[key] + this.element.height() + this.thumb_h) && this.array_thumbs_load[key] == 0)
        {
            this.array_thumbs_load[key] = 1;
            $('#thumbbigpage' + this.uniqkey + '_' + key).show();

            var that = this;
            this.dragonviewer = OpenSeadragon({
                id: 'thumbimg_content' + that.uniqkey + "_" + key,
                prefixUrl: "../images/",
                tileSources: that.tileSources[key],
                panHorizontal: false,
                panVertical: false,
                defaultZoomLevel: 0,
                minZoomImageRatio: 1,
                maxZoomPixelRatio: 0,
                visibilityRatio: 1,
                zoomInButton: "none",
                zoomOutButton: "none",
                homeButton: "none",
                fullPageButton: "none",
                nextButton: "none",
                previousButton: "none",
                immediateRender: true
            });
        }
    }
};

ThumbBig.prototype.thumbnail_select = function () {
    $('.thumbbigimg').removeClass('current_thumb');
    $('#thumbimg' + this.uniqkey + '_' + (this.options.page - 1)).addClass('current_thumb');

    if (this.options.page > 1)
    {
        /*
         this.line_div = this.options.page;
         if (this.options.page % 2 == 1)
         {
         this.line_div = this.options.page - 1;
         }
         this.element.scrollTop(this.thumb_h * this.line_div / 2);
         */

        this.element.scrollTop(this.position_thumb[(this.options.page - 1)]);
        //console.log(this.position_thumb[(this.options.page-1)]+' '+this.thumb_h * this.line_div / 2);

    } else
    {
        this.element.scrollTop(0);
    }
};