jQuery(function($)
{
    $.extend($.fn,
            {
                //////////////////////////////////////////
                Search: function(item_id, options)
                {
                    this.item_id = item_id;
                    var that = this;
                    $("#search_bt").click(function(e)
                    {
                        that.Search_init();

                    });
                    $('#search_txt').keypress(function(e) {
                        if (e.which == 13)
                        {
                            that.Search_init();
                        }
                    });

                },
                Search_init: function(options)
                {
                    this.Get_Search_Alto();
                },
                draw_items: function(items) {
                    var html = '';

                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];

                        html += this.template_item(item);
                    }
                    return html;
                },
                template_item: function(item) {
                    var html = '';


                    html += '<div id="search_result_box_ocr">';
                    html += '<div class="search_result_content" onclick="highlight_ocr(' + item.page + ');">';
                    html += '<div class="search_result_content_span">';
                    html += '<div class="search_ocr" id="search_ocr_page_' + item.page + '">....' + item.highlights.content.join('... \n ...') + '....</div>';
                    html += 'View ' + item.page;
                    html += '</div></div></div>';



                    return html;
                }
                ,
                Get_Search_Alto: function()
                {
                    $('#search_wait').show();
                    $('#search_result').hide();

                    var that = this;
                    $.getJSON("/api/altopages",
                            {
                                item_id: this.item_id,
                                q: $("#search_txt").val()
                            },
                    function(data)
                    {
                        var hrml_ocr = '';
                        var count = 0;
                        if (data && data.data) {
                            hrml_ocr = that.draw_items(data.data);
                            count = data.data.length;
                        }
                        var html = '';

                        html += '<div class="search_result_title" id="search_result_title_ocr">';
                        html += '<span style="float:left" class="search_result_title_span">Full text : ' + count + '</span>';
                        html += '<span style="float:right" id="arrowl_ocr"><img src="/assets/images/viewer/mini-arrow-left.png"></span>';
                        html += '<span style="float:right; display:none" id="arrowb_ocr"><img src="/assets/images/viewer/mini-arrow-down.png"></span>';
                        html += '<div style="clear:both"></div>';
                        html += '</div>';

                        html += '<div id="result_ocr">';
                        html += hrml_ocr;
                        html += '</div>';
                        $('#search_result').html(html);

                        $("#search_result_title_ocr").click(function()
                        {
                            that.Hide_Show_Search('ocr');
                        });

                        $(".navi-ocr").click(function()
                        {
                            that.new_page_ocr_search = this.id.substr(5);
                            if (that.new_page_ocr_search == '')
                            {
                                that.new_page_ocr_search = 0;
                            }
                            $("#page_ocr_search").val(that.new_page_ocr_search);
                            that.Get_Search();
                        });


                        $('#search_wait').hide();
                        $('#search_result').show();

                    });
                },
                Hide_Show_Search: function(type)
                {

                    if (type == 'signet')
                    {
                        if (search_signet_open == 0) {
                            search_signet_open = 1;
                        }
                        else {
                            search_signet_open = 0;
                        }
                        this.Hide_Show_Search_Exec(type, search_signet_open);
                    }
                    if (type == 'annotate')
                    {
                        if (search_annotate_open == 0) {
                            search_annotate_open = 1;
                        }
                        else {
                            search_annotate_open = 0;
                        }
                        $("#page_annotation_open_search").val(search_annotate_open);
                        this.Hide_Show_Search_Exec(type, search_annotate_open);
                    }
                    if (type == 'ocr')
                    {
                        if (search_ocr_open == 0) {
                            search_ocr_open = 1;
                        }
                        else {
                            search_ocr_open = 0;
                        }
                        $("#page_ocr_open_search").val(search_ocr_open);
                        this.Hide_Show_Search_Exec(type, search_ocr_open);
                    }
                },
                Hide_Show_Search_Exec: function(type, visible)
                {
                    if (visible == 1)
                    {
                        $('#result_' + type).show();
                        $('#arrowb_' + type).show();
                        $('#arrowl_' + type).hide();
                    }
                    else
                    {
                        $('#result_' + type).hide();
                        $('#arrowb_' + type).hide();
                        $('#arrowl_' + type).show();
                    }

                },
                Highlight_OCR: function(p)
                {
                    var that = this;
                    this.Highlight_Hide();

                    // retourne les textes highlight
                    var search_txt = $("#search_txt").val() + ' ';
                    $("#search_ocr_page_" + p + " .highlight").text(function(index, text)
                    {
                        search_txt += text + ' ';
                    });
                    //

                    $("#prc").unbind();
                    $("#page_hidden").unbind();

                    $("#page_hidden").change(function()
                    {
                        if ($("#page_hidden").val() != p)
                        {
                            that.Highlight_Hide();
                            $("#prc").unbind();
                        }
                    });

                    $("#prc").change(function()
                    {
                        if ($("#page_hidden").val() == p)
                        {
                            that.Highlight_OCR(p);
                        }
                    });


                    $.getJSON("ajax/ajax_highlight_ocr.php",
                            {
                                filemedia_id: filemedia_id,
                                language_id: language_id,
                                user: user,
                                img: images[(p - 1)],
                                txt: search_txt
                            },
                    function(data)
                    {
                        this.elem_wid = $("#grid").width();
                        this.elem_hei = $("#grid").height();

                        var that = this;
                        var html = '';
                        $.each(data, function(key, val)
                        {
                            html += '<div class="ocr_highlight" style="';
                            html += 'top:' + (that.elem_hei * val['PTOP'] / 100) + '; ';
                            html += 'left:' + (that.elem_wid * val['PLEFT'] / 100) + '; ';
                            html += 'width:' + (that.elem_wid * val['PWIDTH'] / 100) + '; ';
                            html += 'height:' + (that.elem_hei * val['PHEIGHT'] / 100) + '; ';
                            html += '"></div>';
                        });
                        $(".ocr_highlight").remove();
                        $('#grid').append(html);
                    });
                },
                Highlight_Annotate: function(id, p)
                {
                    var that = this;
                    this.Highlight_Hide();


                    $("#search_annotate_small_" + id).hide();
                    $("#search_annotate_full_" + id).show();


                    $("#prc").unbind();
                    $("#page_hidden").unbind();

                    $("#page_hidden").change(function()
                    {
                        if ($("#page_hidden").val() != p)
                        {
                            that.Highlight_Hide();
                            $("#prc").unbind();
                        }
                    });

                    $("#prc").change(function()
                    {
                        if ($("#page_hidden").val() == p)
                        {
                            that.Highlight_Annotate(id, p);
                        }
                    });

                    $.getJSON("ajax/ajax_highlight_annotation.php",
                            {
                                filemedia_id: filemedia_id,
                                language_id: language_id,
                                user: user,
                                id: id,
                                img: images[(p - 1)],
                                txt: $("#search_txt_hidden").val()
                            },
                    function(data)
                    {
                        this.elem_wid = $("#grid").width();
                        this.elem_hei = $("#grid").height();

                        var that = this;
                        var html = '';
                        $.each(data, function(key, val)
                        {
                            html += '<div class="annotate_highlight" style="';
                            html += 'top:' + (that.elem_hei * val['PTOP'] / 100) + '; ';
                            html += 'left:' + (that.elem_wid * val['PLEFT'] / 100) + '; ';
                            html += 'width:' + (that.elem_wid * val['PWIDTH'] / 100) + '; ';
                            html += 'height:' + (that.elem_hei * val['PHEIGHT'] / 100) + '; ';
                            html += '"></div>';
                        });
                        $(".annotate_highlight").remove();
                        $('#grid').append(html);
                    });
                },
                Highlight_Hide: function()
                {
                    $(".search_annotate_small").show();
                    $(".search_annotate_full").hide();
                    $(".annotate_highlight").remove();
                    $(".ocr_highlight").remove();
                }
                ///////////////////////////////////////
            })
});