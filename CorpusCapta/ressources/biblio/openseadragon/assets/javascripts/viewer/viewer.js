var Viewer = function (element, options) {

    options = options || {};
    this.element = element;

    // load json
    var that = this;
    if (options.infos == undefined || options.infos == '') {
        options.infos = options.server + '/tiles/infos.json';
    }
    $.getJSON(options.infos, {},
            function (data)
            {
                that.json_data = data || alert("No response from server " + this.server);
                if (that.json_data.item) {

                    that.json_data = that.json_data.item;
                }

                // init ui and apps
                that.ui_bottom = null;
                that.ui_top = null;
                that.thumb = null;
                that.thumbbig = null;
                that.picture = null;
                that.flip = null;
                that.content_left = null;

                // params
                that.options = {};

                that.options.server = options.server || 'limbgallery';
                that.options.http = options.http || '';
                that.options.image = options.image;
                that.options.height_top = options.height_top || 0;
                that.options.height_bottom = options.height_bottom || 50;
                that.options.get_dir_ico = options.get_dir_ico || '';
                that.options.get_css = options.get_css || '';
                that.options.fullscreen = options.fullscreen || 1;
                that.options.return_url = options.return_url || '';
                that.options.bt_page_w = options.bt_page_w || 50;
                that.options.bt_page_h = options.bt_page_h || 100;
                that.options.title = options.title;
                that.options.description = options.description || '';
                that.options.file_count = options.file_count;
                that.options.flipping = options.flipping || 0;
                that.options.show_thumb = options.show_thumb || 0;
                that.options.show_bigthumb = options.show_bigthumb || 0;
                that.options.show_contrast = options.show_contrast || 0;
                that.options.show_comparator = options.show_comparator || 0;
                that.options.show_scale = options.show_scale || 0;
                that.options.show_optimize = options.show_optimize || 1;
                that.options.show_annotate = options.show_annotate || 0;
                that.options.annotate_id = options.annotate_id || 0;
                that.options.show_bookmarks = options.show_bookmarks || 0;
                that.options.show_search = options.show_search || 0;
                that.options.show_ocr = options.show_ocr || 0;
                that.options.is_mobile = options.is_mobile;
                that.options.page = options.page || 1;
                that.options.show_navbox = options.show_navbox || 1;
                that.options.pdf = options.pdf || '';
                that.options.show_download_crop = options.show_download_crop || 0;
                that.options.show_download_pdf_pages = options.show_download_pdf_pages || 0;
                that.options.show_download_full_pdf = options.show_download_full_pdf || 0;
                that.options.show_download_jpg = options.show_download_jpg || 0;
                that.options.show_download_attached_files = options.show_download_attached_files || 0;
                that.options.show_volume = options.show_volume || 0;
                that.options.show_rotate = options.show_rotate || 0;
                that.options.show_invert = options.show_invert || 0;
                that.options.show_share_permalink = options.show_share_permalink || 0;
                that.options.show_share_crop = options.show_share_crop || 0;
                that.options.show_share_embeded = options.show_share_embeded || 0;
                that.options.viewer_type = options.viewer_type || 'picture';
                that.options.item_id = options.item_id || 0;
                that.options.search_q = options.search_q || '';
                that.options.search_limit = options.search_limit || 10;
                that.options.collaborate = options.collaborate || 0;
                that.options.tarcking_id = options.tarcking_id || 0;
                that.options.lg_tracking_id = options.lg_tracking_id || 0;
                that.viewer_page_tracking = 0;
                that.options.direction = options.direction || 'LTR';
                that.options.open_by_default = options.open_by_default || '';
                that.options.ocrnum = options.ocrnum || 0;
                that.options.show_arrow = options.show_arrow || 0;
                that.options.prev_next_search_request = options.prev_next_search_request || '';
                that.options.current_offset = options.current_offset || '';
                that.options.html_page = options.html_page || 0;
                that.options.scale_unit = options.scale_unit || '';
                that.options.scale_unit_num = options.scale_unit_num || 0;
                that.options.scale_px_to_unit = options.scale_px_to_unit || 0;
                that.options.dir_ico = options.dir_ico || '/assets/images/viewer/';
                that.options.bookmarks_collapsed = options.bookmarks_collapsed || false;
                that.options.volume = options.volume;
                that.options.links_prefix = options.links_prefix || '';
                that.options.image_params = options.image_params || '';
                that.options.language = options.language || '';
                that.options.parent_itemtype_id = options.parent_itemtype_id || '';
                if (that.options.image_params != "") {
                    that.options.image_params = $.parseJSON(that.options.image_params);
                }

                that.user_id = options.user || false;

                that.open_type = that.options.open_by_default;
                //

                //params size
                that.viewer_height = that.element.height() - that.options.height_top - that.options.height_bottom;
                that.viewer_width = that.element.width();
                that.viewer_menu_left_width = 0;
                that.menu_left = that.viewer_width * 0.25;
                if (that.menu_left < 300) {
                    that.menu_left = that.viewer_width * 0.33;
                }

                /// webkit
                that.options.webkit = 0;
                var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
                var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
                if (isChrome) {
                    that.options.webkit = 1;
                }
                if (isSafari) {
                    that.options.webkit = 1;
                }
                ///



// action and events
                that.init_ui();
                that.position();
                that.setup_events();

/// links
                that.ui_bottom.set_viewer(that);
                that.ui_bottom.set_thumb(that.thumb);
                that.ui_bottom.set_thumbbig(that.thumbbig);
                that.ui_bottom.set_picture(that.picture);
                that.ui_bottom.set_flip(that.flip);
                that.ui_bottom.set_content_left(that.content_left);

                that.content_left.set_viewer(that);
                that.content_left.set_picture(that.picture);
                that.content_left.set_flip(that.flip);
                that.content_left.set_ui_bottom(that.ui_bottom);

                that.thumb.set_viewer(that);
                that.thumb.set_picture(that.picture);
                that.thumb.set_flip(that.flip);
                that.thumb.set_ui_bottom(that.ui_bottom);

                that.thumbbig.set_viewer(that);
                that.thumbbig.set_picture(that.picture);
                that.thumbbig.set_flip(that.flip);
                that.thumbbig.set_ui_bottom(that.ui_bottom);

                that.flip.set_viewer(that);
                that.flip.set_content_left(that.content_left);

                that.picture.set_viewer(that);
                that.picture.set_content_left(that.content_left);

                that.create_picture = 0;
                if (that.options.viewer_type == 'picture') {
                    that.create_picture = 1;
                    that.picture.create_picture();
                    //that.tracker_mode(__('Picture'));
                    that.hash();
                } else {
                    that.default_load(that.options.viewer_type);
                }
                //that.tracker_page();
                that.open_by_default_fc();
            });

};


// EVENTS TRACKER
Viewer.prototype.default_load = function (type) {
    this.ui_bottom.viewer_control.hide();
    this.ui_bottom.hide_elements();
    this.set_viewer_type(type, 0);

    if (type == 'flip') {
        this.ui_bottom.viewer_control_page.show();
        this.tracker_mode(__('Flipping'));
        this.flip.set_page(this.options.page);
    } else if (type == 'thumb') {
        this.tracker_mode(__('Thumbnails'));
    } else if (type == 'thumbbig') {
        this.tracker_mode(__('Facing pages'));
    } else if (type == 'picture') {
        this.tracker_mode(__('Picture'));
    }

    this.hash();
};

// HASH
Viewer.prototype.hash = function () {
    var hash = 'page=' + this.options.page + '&viewer=' + this.options.viewer_type + '&o=' + this.open_type + '&n=' + this.options.ocrnum + '&q=' + this.options.search_q;
    location.hash = hash;
    $('#hash').val(hash);
};

// EVENTS TRACKER
Viewer.prototype.setup_events = function () {
    var that = this;
    $(window).bind('resize', function (event) {
        that.viewer_height = that.element.height() - that.options.height_top - that.options.height_bottom;
        that.viewer_width = that.element.width();
        that.position();
    });
};


// GA TRACKER
/*Viewer.prototype.tracker_action = function (action) {
    if (this.options.tarcking_id != 0) {
        //ga('send', 'event', 'Viewer : Action', action, this.options.title);
        ga('send', 'event', 'Viewer : Action', this.options.title, action);
    }
    if (this.options.lg_tracking_id != 0) {
        _paq.push(['trackEvent', this.options.title, action, 'Viewer : Action']);
    }

};

Viewer.prototype.tracker_download = function (file) {
    if (this.options.tarcking_id != 0) {
        ga('send', 'event', 'Viewer : Download', this.options.title, file);
    }
    if (this.options.lg_tracking_id != 0) {
        _paq.push(['trackEvent', this.options.title, file, 'Viewer : Download / Full']);
    }
};
Viewer.prototype.tracker_download_page = function (page) {
    if (this.options.tarcking_id != 0) {
        ga('send', 'event', 'Viewer : Download page', this.options.title, page);
    }
    if (this.options.lg_tracking_id != 0) {
        _paq.push(['trackEvent', this.options.title, page, 'Viewer : Download / page : '+(page*1+1)]);
    }
};
Viewer.prototype.tracker_download_file = function (file) {
    if (this.options.tarcking_id != 0) {
        ga('send', 'event', 'Viewer : Download attached file', this.options.title, file);
    }
    if (this.options.lg_tracking_id != 0) {
        _paq.push(['trackEvent', this.options.title, file, 'Viewer : Download / attached file : '+file]);
    }
};
Viewer.prototype.tracker_print = function (page) {
    if (this.options.tarcking_id != 0) {
        ga('send', 'event', 'Viewer : Print full', this.options.title, page);
    }
    if (this.options.lg_tracking_id != 0) {
        _paq.push(['trackEvent', this.options.title, page, 'Viewer : Print full']);
    }
};
Viewer.prototype.tracker_print_screen = function (page) {
    if (this.options.tarcking_id != 0) {
        ga('send', 'event', 'Viewer : Download crop', this.options.title, page);
    }
    if (this.options.lg_tracking_id != 0) {
        _paq.push(['trackEvent', this.options.title, page, 'Viewer : Download crop / page : '+(page*1)]);
    }
};

Viewer.prototype.tracker_mode = function (mode) {
    if (this.options.tarcking_id != 0) {
        ga('send', 'event', 'Viewer : Mode', mode, this.options.title);
    }
    if (this.options.lg_tracking_id != 0) {
        _paq.push(['trackEvent', this.options.title, mode, 'Viewer : Mode']);
    }
};

Viewer.prototype.tracker_page = function () {
    if (this.options.tarcking_id != 0 && this.viewer_page_tracking != this.options.page) {
        this.viewer_page_tracking = this.options.page;
        ga('send', 'event', 'Viewer : Page', this.options.title, 'Page : ' + this.options.page);
    }
    if (this.options.lg_tracking_id != 0) {
        _paq.push(['trackEvent', this.options.title, 'Page : ' + this.options.page, 'Viewer : Page']);
    }
};*/

Viewer.prototype.open_by_default_fc = function () {
    var that = this;
    if (this.options.open_by_default != '') {

        if (that.options.open_by_default == 'search' && that.options.show_search == '1' && that.options.search_q == '') {
            that.content_left.search();
        } else if (that.options.open_by_default == 'bookmarks' && that.options.show_bookmarks == '1') {
            that.content_left.bookmark();
        } else if (that.options.open_by_default == 'description' && that.options.description == '1') {
            that.content_left.info();
        } else if (that.options.open_by_default == 'annotation' && that.options.annotation != 0) {
            that.content_left.annotate();
        } else if (that.options.open_by_default == 'ocr' && that.options.show_ocr != 0) {
            that.content_left.ocr();
        } else if (that.options.open_by_default == 'volume' && that.options.show_ocr != 0) {
            that.content_left.volumes();
        } else if (that.options.open_by_default == 'info' && that.options.description == '1') {
            that.content_left.info();
        } else if (that.options.open_by_default == 'share') {
            that.content_left.share();
        } else if (that.options.open_by_default == 'comparator' && that.options.show_comparator == '1') {
            that.content_left.comparator();
        }
    }
};
// CHANGE PAGE PICTURE
Viewer.prototype.set_page_picture = function (page) {
    var that = this;

    if (that.create_picture == 0) {
        that.create_picture = 1;
        that.picture.create_picture();
    }

    this.options.page = page;
    this.options.viewer_type = 'picture';
    this.ui_bottom.set_page(this.options.page);
    this.thumb.set_page(this.options.page);
    this.thumbbig.set_page(this.options.page);
    this.ui_bottom.viewer_control.show();
    this.ui_bottom.hide_elements();
    this.position();

    setTimeout(function ()
    {
        that.picture.set_page(that.options.page);
    }, 600);

}
// CHANGE PAGE
Viewer.prototype.set_open = function (open_type) {
    if (open_type == 'annotate') {
        open_type = 'annotation';
    }
    this.open_type = open_type;
    this.hash();
}

// CHANGE PAGE
Viewer.prototype.set_page = function (page) {
    /* if (this.options.viewer_type == 'flip' && page > 1)
     {

     if (this.options.page > page)
     {
     if (page % 2 == 1)
     {
     page--;
     }
     }
     else
     {
     if (page % 2 == 1)
     {
     page++;
     }
     }
     }*/

    this.options.page = page;
    this.ui_bottom.set_page(this.options.page);
    //this.thumb.set_page(this.options.page);
    // this.thumbbig.set_page(this.options.page);
    this.content_left.set_page(this.options.page);

    if (this.options.viewer_type == 'flip')
    {
        this.flip.set_page(this.options.page);
    } else if (this.options.viewer_type == 'picture')
    {
        this.picture.set_page(this.options.page);
    } else if (this.options.viewer_type == 'thumb') {
        this.thumb.set_page(this.options.page);
    } else if (this.options.viewer_type == 'thumbbig') {
        this.thumbbig.set_page(this.options.page);
    }


    if (this.options.show_bookmarks == 1)
    {
        $('.class_bookmark').removeClass("hover");
        $('.class_bookmark_' + this.options.page).addClass("hover");
    }


    this.hash();

    var that = this;
    setTimeout(function ()
    {
        that.tracker_page();
    }, 800);

}

// CHANGE ZOOM
Viewer.prototype.set_zoom = function (prc) {
    this.content_left.set_zoom(prc);
}

// VIEWER TYPE
Viewer.prototype.set_viewer_type = function (viewer, speed) {
    this.ui_bottom.set_viewer_type(viewer);
    if (speed == undefined) {
        speed = 'slow';
    }
    if (this.options.viewer_type != viewer) {
        var that = this;

        if (this.options.viewer_type == 'thumb')
        {
            that.thumb.element.fadeOut(speed, function ()
            {
                that.options.viewer_type = viewer;
                that.thumb.set_page(that.options.page);
                that.position();
            });
        } else if (this.options.viewer_type == 'picture')
        {
            that.picture.element.fadeOut(speed, function ()
            {
                that.options.viewer_type = viewer;
                that.position();
            });
        } else if (this.options.viewer_type == 'thumbbig')
        {
            that.thumbbig.element.fadeOut(speed, function ()
            {
                that.options.viewer_type = viewer;
                that.thumbbig.set_page(that.options.page);
                that.position();
            });
        } else if (this.options.viewer_type == 'flip')
        {
            that.flip.element.fadeOut(speed, function ()
            {
                that.flip.set_page(that.options.page);
                that.options.viewer_type = viewer;
                that.position();
            });
        }


        setTimeout(function ()
        {

            if (that.content_left.open_type == 'ocr' && viewer != 'picture') {
                that.content_left.close();
            }

            if (that.options.viewer_type == 'flip')
            {
                //alert(that.options.page);
                that.flip.set_page(that.options.page);
            } else if (that.options.viewer_type == 'picture')
            {
                if (that.create_picture == 0) {
                    that.create_picture = 1;
                    that.picture.create_picture();
                }
                that.picture.set_page(that.options.page);
            } else if (that.options.viewer_type == 'thumb') {
                that.thumb.set_page(that.options.page);

            } else if (that.options.viewer_type == 'thumbbig') {
                that.thumbbig.set_page(that.options.page);
            }
            that.hash();

        }, 800);

    }
}

// CHANGE MENU LEFT WIDTH
Viewer.prototype.set_menu_left_width = function (w) {
    this.viewer_menu_left_width = w;
    this.position();
    this.picture.set_resize(this.viewer_width, this.viewer_height, this.viewer_menu_left_width);
}

// UI AND APPS TRACER
Viewer.prototype.init_ui = function () {

    //top
    var ui_top_element = $('<div class="top_menu"></div>');

    this.element.append(ui_top_element);
    this.ui_top = new UiTop($(ui_top_element), {
        'title': this.options.title,
        'is_mobile': this.options.is_mobile,
        'collaborate': this.options.collaborate,
        'fullscreen': this.options.fullscreen,
        'prev_next_search_request': this.options.prev_next_search_request,
        'current_offset': this.options.current_offset,
        'viewer_type': this.options.viewer_type,
        'return_url': this.options.return_url,
        'get_dir_ico': this.options.get_dir_ico,
        'get_css': this.options.get_css,
        'height_top': this.options.height_top,
        'volume': this.options.volume

    });

    //thumb
    var thumb_element = $('<div class="content_thumb"></div>');
    this.element.append(thumb_element);
    this.thumb = new Thumb($(thumb_element), {
        'server': this.options.server,
        'image': this.options.image,
        'is_mobile': this.options.is_mobile,
        'page': this.options.page
    }, this.json_data);

    //thumbbig
    var thumbbig_element = $('<div class="content_thumbbig"></div>');
    this.element.append(thumbbig_element);
    this.thumbbig = new ThumbBig($(thumbbig_element), {
        'server': this.options.server,
        'image': this.options.image,
        'is_mobile': this.options.is_mobile,
        'direction': this.options.direction,
        'image_params': this.options.image_params,
        'page': this.options.page
    }, this.json_data);

    //content_app
    var content_app_element = $('<div class="content_app">');
    this.element.append(content_app_element);

    //left
    var content_left_element = $('<div class="content_left ui-resizable" style="display:none"></div>');
    //this.element.append(content_left_element);
    content_left_element.appendTo(content_app_element);

    this.content_left = new ContentLeft($(content_left_element), {
        'server': this.options.server,
        'http': this.options.http,
        'image': this.options.image,
        'is_mobile': this.options.is_mobile,
        'menu_left': this.menu_left,
        'viewer_width': this.viewer_width,
        'page': this.options.page,
        'item_id': this.options.item_id,
        'search_q': this.options.search_q,
        'search_limit': this.options.search_limit,
        'show_volume': this.options.show_volume,
        'show_share_permalink': this.options.show_share_permalink,
        'show_share_crop': this.options.show_share_crop,
        'show_share_embeded': this.options.show_share_embeded,
        'show_download_crop': this.options.show_download_crop,
        'show_download_pdf_pages': this.options.show_download_pdf_pages,
        'show_download_full_pdf': this.options.show_download_full_pdf,
        'show_download_jpg': this.options.show_download_jpg,
        'show_download_attached_files': this.options.show_download_attached_files,
        'flipping': this.options.flipping,
        'direction': this.options.direction,
        'html_page': this.options.html_page,
        'dir_ico': this.options.dir_ico,
        'bookmarks_collapsed': this.options.bookmarks_collapsed,
        'show_annotate': this.options.show_annotate,
        'annotate_id': this.options.annotate_id,
        'open_by_default': this.options.open_by_default,
        'user_id': this.user_id,
        'volume': this.options.volume,
        'links_prefix': this.options.links_prefix,
        'language': this.options.language,
        'get_css': this.options.get_css,
        'return_url': this.options.return_url,
        'parent_itemtype_id': this.options.parent_itemtype_id,
    });

    //flip
    var flip_element = $('<div class="content_flip" style="display:none"></div>');
    flip_element.appendTo(content_app_element);

    this.flip = new Flip($(flip_element), {
        'flipping': this.options.flipping,
        'item_id': this.options.item_id,
        'server': this.options.server,
        'image': this.options.image,
        'is_mobile': this.options.is_mobile,
        'page': this.options.page,
        'viewer_height': this.viewer_height,
        'viewer_width': this.viewer_width,
        'viewer_menu_left_width': this.viewer_menu_left_width,
        'bt_page_w': this.options.bt_page_w,
        'bt_page_h': this.options.bt_page_h,
        'dir_ico': this.options.dir_ico
    }, this.json_data);

    //picture
    var picture_element = $('<div class="content_picture" style="display:none"></div>');
    picture_element.appendTo(content_app_element);

    this.picture = new Picture($(picture_element), {
        'server': this.options.server,
        'image': this.options.image,
        'is_mobile': this.options.is_mobile,
        'page': this.options.page,
        'item_id': this.options.item_id,
        'viewer_height': this.viewer_height,
        'viewer_width': this.viewer_width,
        'viewer_menu_left_width': this.viewer_menu_left_width,
        'bt_page_w': this.options.bt_page_w,
        'bt_page_h': this.options.bt_page_h,
        'show_navbox': this.options.show_navbox,
        'direction': this.options.direction,
        'show_scale': this.options.show_scale,
        'show_arrow': this.options.show_arrow,
        'scale_unit': this.options.scale_unit,
        'scale_unit_num': this.options.scale_unit_num,
        'scale_px_to_unit': this.options.scale_px_to_unit,
        'dir_ico': this.options.dir_ico,
        'links_prefix': this.options.links_prefix
    }, this.json_data);

    //bottom
    var ui_bottom_element = $('<div class="bottom_menu"></div>');
    this.element.append(ui_bottom_element);
    this.ui_bottom = new UiBottom($(ui_bottom_element), {
        'description': this.options.description,
        'file_count': this.options.file_count,
        'flipping': this.options.flipping,
        'show_thumb': this.options.show_thumb,
        'show_bigthumb': this.options.show_bigthumb,
        'show_contrast': this.options.show_contrast,
        'show_comparator': this.options.show_comparator,
        'show_scale': this.options.show_scale,
        'show_optimize': this.options.show_optimize,
        'show_annotate': this.options.show_annotate,
        'show_bookmarks': this.options.show_bookmarks,
        'show_ocr': this.options.show_ocr,
        'show_search': this.options.show_search,
        'is_mobile': this.options.is_mobile,
        'webkit': this.options.webkit,
        'page': this.options.page,
        'pdf': this.options.pdf,
        'show_volume': this.options.show_volume,
        'show_download_crop': this.options.show_download_crop,
        'show_download_pdf_pages': this.options.show_download_pdf_pages,
        'show_download_full_pdf': this.options.show_download_full_pdf,
        'show_download_jpg': this.options.show_download_jpg,
        'show_download_attached_files': this.options.show_download_attached_files,
        'show_rotate': this.options.show_rotate,
        'show_invert': this.options.show_invert,
        'show_share_permalink': this.options.show_share_permalink,
        'show_share_crop': this.options.show_share_crop,
        'show_share_embeded': this.options.show_share_embeded,
        'direction': this.options.direction,
        'viewer_type': this.options.viewer_type,
        'dir_ico': this.options.dir_ico,
        'volume': this.options.volume
    });

    //logo
    var logo_element = $('<div class="logo"></div>');
    this.element.append(logo_element);
};


// UI AND APPS POSITION
Viewer.prototype.position = function () {

    // bottom
    this.ui_bottom.element.css('top', this.viewer_height + this.options.height_top - 1 + 'px');
    this.ui_bottom.element.height(this.options.height_top);

    // left
    this.content_left.element.css('top', this.options.height_top + 'px');
    this.content_left.element.height(this.viewer_height);

    // top
    this.ui_top.element.height(this.options.height_top);
    this.ui_top.set_resize(this.viewer_width);

    //thumb
    this.thumb.element.width(this.viewer_width - this.viewer_menu_left_width);
    this.thumb.element.css('left', this.viewer_menu_left_width);
    this.thumb.element.css('top', this.options.height_top + 'px');
    this.thumb.element.height(this.viewer_height);
    this.thumb.element.hide();

    //thumbbig
    this.thumbbig.element.width(this.viewer_width - this.viewer_menu_left_width);
    this.thumbbig.element.css('left', this.viewer_menu_left_width);
    this.thumbbig.element.css('top', this.options.height_top + 'px');
    this.thumbbig.element.height(this.viewer_height);
    this.thumbbig.element.hide();

    //flip
    this.flip.element.width(this.viewer_width - this.viewer_menu_left_width);
    this.flip.element.css('left', this.viewer_menu_left_width);
    this.flip.element.css('top', this.options.height_top);
    this.flip.element.height(this.viewer_height);

    //picture
    this.picture.element.width(this.viewer_width - this.viewer_menu_left_width);
    this.picture.element.css('left', this.viewer_menu_left_width);
    this.picture.element.css('top', this.options.height_top);

    var that = this;
    this.timewait = 200;
    if (this.options.viewer_type == 'picture') {
        this.timewait = 5;
    }
    setTimeout(function ()
    {
        that.content_left.set_resize(that.viewer_width, that.viewer_height);

        if (that.options.viewer_type == 'picture')
        {

            that.picture.element.show();
            that.picture.set_resize(that.viewer_width, that.viewer_height, that.viewer_menu_left_width);

            that.thumb.set_viewer_type('picture');
            that.thumbbig.set_viewer_type('picture');
            that.content_left.set_viewer_type('picture');
        }

        if (that.options.viewer_type == 'thumbbig')
        {
            that.thumbbig.element.show();
            that.thumbbig.recreate_thumbnail();
            that.content_left.set_viewer_type('thumbbig');
        }

        if (that.options.viewer_type == 'thumb')
        {
            that.thumb.element.show();
            that.thumb.recreate_thumbnail();
            that.content_left.set_viewer_type('thumb');
        }

        if (that.options.viewer_type == 'flip')
        {
            that.flip.element.show();
            that.flip.set_resize(that.viewer_width, that.viewer_height, that.viewer_menu_left_width);

            that.thumb.set_viewer_type('flip');
            that.thumbbig.set_viewer_type('flip');
            that.content_left.set_viewer_type('flip');
        }
    }, this.timewait);

};
