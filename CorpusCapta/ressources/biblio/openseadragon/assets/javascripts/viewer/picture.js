var Picture = function (element, options, json_data) {
    options = options || {};
    this.element = element;
    this.json_data = json_data;
    // init ui and apps
    this.viewer = null;
    //browser type
    jQuery.browser = {};
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }

// params
    this.NavigationWindow_load = 0;
    this.mouse_in_viewer = 0; //si la souris sur le viewer

    this.options = options;
    this.options.server = options.server || 'yoolib';
    this.options.image = options.image;
    this.options.viewer_height = options.viewer_height || 500;
    this.options.viewer_width = options.viewer_width || 500;
    this.options.bt_page_w = options.bt_page_w || 50;
    this.options.bt_page_h = options.bt_page_h || 100;
    this.options.viewer_menu_left_width = options.viewer_menu_left_width || 0;
    this.options.is_mobile = options.is_mobile || 0;
    this.options.is_tablet = options.is_tablet || 0;
    this.options.page = options.page || 1;
    this.options.page_current = options.page || 0;
    this.options.show_navbox = options.show_navbox || 1;
    this.options.show_scale = options.show_scale || 0;
    this.options.show_scale_box = 1;
    this.options.navbox_ratio = 0.2;
    this.options.direction = options.direction || 'LTR';
    this.options.show_arrow = options.show_arrow || 0;
    this.options.scale_unit = options.scale_unit || '';
    this.options.scale_unit_num = options.scale_unit_num || 0;
    this.options.scale_px_to_unit = options.scale_px_to_unit || 0;
    this.uniqkey = Math.random() * 0x100000000000000;
    this.bounds = '';
    this.page_action = '';
    this.can_go_page = 1; // to generate a timer to waiting the page have a good coordinates
    this.can_search = '';
    this.can_alto = '';
    this.current_prc = 0;
    this.scale_rotate = 'h';
    this.scale_rotate_click = 0;
    this.options.dir_ico = options.dir_ico || '/assets/images/viewer/';
    this.options.links_prefix = options.links_prefix || '';
    // action and events
    this.init_ui();
    this.position();
};
// PICTURE CREATE
Picture.prototype.create_picture = function () {
    this.element.show();
    this._initialise();
    this._load();
    this.setup_events();
    this._createNavigationWindow();
    this._hide_prev_next();
};
// EVENTS TRACKER
Picture.prototype.setup_events = function () {
    var that = this;
    this.div_picture.mousedown(function () {
        return false;
    });
    //mouse in picture 
    this.div_picture.mouseenter(function ()
    {
        that.mouse_in_viewer = 1;
    }).mouseleave(function ()
    {
        that.mouse_in_viewer = 0;
    });
    //page prev / next
    this.element.on('click', '.page_next', function (e) {
        e.preventDefault();
        that._page_next();
    });
    this.element.on('click', '.page_prev', function (e) {
        e.preventDefault();
        that._page_prev();
    });
    // Zoom in
    this.element.on('click', '.zoomIn', function (e) {
        e.preventDefault();
        that.zoom_in();
    });
    // Zoom out
    this.element.on('click', '.zoomOut', function (e) {
        e.preventDefault();
        that.zoom_out();
    });
    // mouse position
    this.div_picture.mousemove(function (e)
    {
        if (that.options.is_mobile == 0 && that.options.show_arrow == 0) {
// retourne la postion x,y par rapport au bord du viewer
            that.mouse_x_viewer = e.pageX - that.offset_parent_left;
            that.mouse_y_viewer = e.pageY - that.offset_parent_top;
            // $("#pos").html("Position X"+ (e.pageX-that.offset_parent_left) +" Y"+ (e.pageY-that.offset_parent_top)+ " "+that.mouse_pointer_x);

            if (that.mouse_in_viewer == 1 && (that.mouse_x_viewer < 50 || that.mouse_x_viewer > (that.div_picture.width() - 50)))
            {
                that._show_prev_next();
            } else
            {
                that.div_page_next.hide();
                that.div_page_prev.hide();
            }
        } else {
            that._show_prev_next();
        }
    });
    // dragon event
    this.dragonviewer.addHandler('open', function (event) {
        that.rotate = 0;
//        this.old_zoom_max = this.dragonviewer.viewport.getMaxZoom()


        if (that.bounds != "") {

            var current_zoom_max = that.dragonviewer.viewport.getMaxZoom();
            var bound_ratio = that.old_zoom_max / current_zoom_max;
            var bounds_y = that.bounds.y * bound_ratio;
            var bounds_x = that.bounds.x * bound_ratio;
            var bounds_width = that.bounds.width * bound_ratio;
            var bounds_height = that.bounds.height * bound_ratio;
            if (that.page_action == 'prev') {
                that.dragonviewer.viewport.fitBounds(new OpenSeadragon.Rect(1, bounds_y, bounds_width, bounds_height), 1);
            } else if (that.page_action == 'next') {
                that.dragonviewer.viewport.fitBounds(new OpenSeadragon.Rect(-bounds_width, bounds_y, bounds_width, bounds_height), 1);
            }

            setTimeout(function () {
                that.dragonviewer.viewport.fitBounds(new OpenSeadragon.Rect(bounds_x, bounds_y, bounds_width, bounds_height));
                that.dragonviewer.viewport.applyConstraints();
            }, 20);
        }

        setTimeout(function () {
            that.scale();
        }, 20);
    });
    this.dragonviewer.addHandler('zoom', function (event) {
        if (that.dragonviewer.viewport) {

            var prc_max = that.dragonviewer.viewport.getMaxZoom();
            that.prc_view = event.zoom / prc_max * 100;
            that.div_prc.html(Math.round(that.prc_view) + '%');
            that.current_prc = that.prc_view;
            that.viewer.set_zoom(that.current_prc);
        }
    });
    this.dragonviewer.addHandler('animation-finish', function (event) {
        if (that.dragonviewer.viewport) {
            that.dragonviewer.drawer.update();
            that.apply_contrast();
            that.apply_reverse();
            if (that.rotate != 0 && that.rotate != 180) {
                $('.grid_' + that.uniqkey).hide();
            }
            if (that.rotate != 0) {
                $('.picture_highlight_div').hide();
            }
        }
        that.dragonviewer.viewport.applyConstraints(); // debug image not in good position : prev /next
        that.scale();
    });
    this.dragonviewer.addHandler('animation', function (event) {
        if (that.rotate != 0 && that.rotate != 180) {
            $('.grid_' + that.uniqkey).hide();
        }
        if (that.rotate != 0) {
            $('.picture_highlight_div').hide();
        }
    });
    document.addEventListener("mouseleave", function (evt) {
        that.dragonviewer.setMouseNavEnabled(false);
        that.dragonviewer.setMouseNavEnabled(true);
    });
    this.element.on('click', '.picture_boxes', function (e) {
        e.preventDefault();
        var boxes_content = $(this).find(".picture_content").html();
        $('.picture_boxes_selected').removeClass('picture_boxes_selected');
        $(this).addClass('picture_boxes_selected');
        that.content_left.ocr_content(boxes_content);

        that.viewer.options.ocrnum = $(this).attr('data-num');
        that.viewer.set_open('ocr');
    });
    this.element.on('click', '.navscale_rotate', function (e) {
        e.preventDefault();
        if (that.scale_rotate_click == 0) {
            that.scale_rotate_click = 1;
            setTimeout(function () {
                if (that.scale_rotate == 'h') {
                    that.scale_rotate = 'v';
                } else {
                    that.scale_rotate = 'h';
                }
                that.scale();
                that.scale_rotate_click = 0;
            }, 200);
        }
    });
};
// LINK TO VIEWER
Picture.prototype.set_viewer = function (viewer) {
    this.viewer = viewer;
}
Picture.prototype.set_content_left = function (content_left) {
    this.content_left = content_left;
}

// BOXES ALTO
Picture.prototype.boxes_alto = function ()
{
    if (this.content_left.menu_left_visible == true && this.content_left.open_type == 'ocr' && this.can_alto != this.options.page) {

        this.can_alto = this.options.page;
        var that = this;
        $.getJSON(this.options.links_prefix + "/api/altoboxes",
                {
                    item_id: that.options.item_id,
                    medianame: that.options.image[that.options.page - 1],
                    server: that.options.server,
                },
                function (data)
                {
                    if (data && data.items) {
                        setTimeout(function () {
                            if (that.content_left.menu_left_visible == true && that.content_left.open_type == 'ocr') {
                                that.draw_alto_boxes(data.items, data._meta, 'boxes');
                            }
                        }, 1000);
                    }
                });
    }
}

// HIGHLIGHT ALTO
Picture.prototype.highlight_alto = function ()
{
    if (this.content_left.menu_left_visible == true && this.can_search != this.options.page && this.content_left.open_type == 'search') {

        this.can_search = this.options.page;
        var that = this;
        $.getJSON(this.options.links_prefix + "/api/altowords",
                {
                    item_id: that.options.item_id,
                    q: that.content_left.search_q,
                    medianame: that.options.image[that.options.page - 1],
                    server: that.options.server,
                },
                function (data)
                {
                    if (data && data.items) {
                        setTimeout(function () {
                            if (that.content_left.menu_left_visible == true && that.content_left.open_type == 'search') {
                                that.draw_alto_boxes(data.items, data._meta, 'highlight');
                            }
                        }, 1000);
                    }
                });
    }
}

Picture.prototype.draw_alto_boxes = function (items, meta, type) {
    var that = this;

    var page_h = this.dragonviewer.viewport.viewer.source.height;
    var page_w = this.dragonviewer.viewport.viewer.source.width;
    if (meta) {
        if (meta.page_width && meta.page_height) {
            if (meta.page_width > 0 && meta.page_height > 0) {
                page_h = meta.page_height;
                page_w = meta.page_width;
            }
        }
    }
    for (var i = 0; i < items.length; i++) {
        var item = items[i];
        var box_prc_height = item.height / page_h * 100;
        var box_prc_width = item.width / page_w * 100;
        var box_prc_hpos = item.hpos / page_w * 100;
        var box_prc_vpos = item.vpos / page_h * 100;
        var elt = '';
        elt += '<div class="picture_' + type + ' picture_ocr_' + i + '" style="';
        elt += 'height:' + box_prc_height + '%; ';
        elt += 'width:' + box_prc_width + '%; ';
        elt += 'top:' + box_prc_vpos + '%; ';
        elt += 'left:' + box_prc_hpos + '%; ';
        elt += '" data-num="' + i + '">';
        elt += '<div class="picture_content" style="display:none">';
        elt += item.content;
        elt += '</div>';
        elt += '</div>';
        $('.picture_highlight_' + this.uniqkey).append(elt);
        //open first
        if (i == that.viewer.options.ocrnum && that.content_left.open_type == 'ocr') {
            $('.picture_boxes_selected').removeClass('picture_boxes_selected');
            $(".picture_ocr_" + that.viewer.options.ocrnum).addClass('picture_boxes_selected');
            that.content_left.ocr_content(item.content);
        }
    }
};
// REMOVE HIGHLIGHT
Picture.prototype.remove_highlight = function () {
    $('.picture_highlight_' + this.uniqkey).html('');
    this.can_search = '';
    this.can_alto = '';
}

// CHANGE PAGE
Picture.prototype.set_page = function (page) {
    if (this.options.page != page) {
        this.options.page = page;
        //this.page_direct = 1;
        this._page_goto(page);
    }

    this.highlight_alto();
    this.boxes_alto();
}

// RESIZE
Picture.prototype.set_resize = function (w, h, l) {
    this.options.viewer_height = h;
    this.options.viewer_width = w;
    this.options.viewer_menu_left_width = l;
    this.position();
    this._offset();
}

// CHANGE CONTRAST
Picture.prototype.set_contrast = function (contrast) {
    this.contrast = contrast;
    this.dragonviewer.drawer.update();
    this.apply_contrast();
    this.apply_reverse();
}

Picture.prototype.apply_contrast = function () {
    if (this.contrast > 0) {
        var amount = this.contrast;
        canvas = this.dragonviewer.drawer.canvas;
        ctx = canvas.getContext('2d');
        var data = ctx.getImageData(0, 0, canvas.width, canvas.height); //get pixel data
        var pixels = data.data;
        for (var i = 0; i < pixels.length; i += 4) {//loop through all data
            var brightness = (pixels[i] + pixels[i + 1] + pixels[i + 2]) / 3; //get the brightness

            pixels[i] += brightness > 127 ? amount : -amount;
            pixels[i + 1] += brightness > 127 ? amount : -amount;
            pixels[i + 2] += brightness > 127 ? amount : -amount;
        }
        data.data = pixels;
        ctx.putImageData(data, 0, 0); //put the image data back
    }
}

//  REVERSE
Picture.prototype.set_reverse = function () {
    if (this.reverse == 0) {
        this.reverse = 1;
    } else {
        this.reverse = 0;
    }
    this.dragonviewer.drawer.update();
    this.apply_reverse();
    this.apply_contrast();
}

Picture.prototype.apply_reverse = function () {
    if (this.reverse > 0) {
        canvas = this.dragonviewer.drawer.canvas;
        ctx = canvas.getContext('2d');
        var idata = ctx.getImageData(0, 0, canvas.width, canvas.height),
                data = idata.data;
        for (i = 0; i <= data.length - 4; i += 4) {
            data[i] = 255 - data[i]
            data[i + 1] = 255 - data[i + 1];
            data[i + 2] = 255 - data[i + 2];
        }
        ctx.putImageData(idata, 0, 0);
    }
}

// ROTATE
Picture.prototype.set_rotate = function (deg) {
    this.rotate = deg;
    if (this.dragonviewer.viewport) {
        this.dragonviewer.viewport.setRotation(Number(deg));
        /*if (this.dragonviewer.navigator.viewport) {
         this.dragonviewer.navigator.viewport.setRotation(Number(deg));
         }*/
        if (this.rotate != 0 && this.rotate != 180) {
            $('.grid_' + this.uniqkey).hide();
            this.dragonviewer.viewport.visibilityRatio = 1.1 - (this.dragonviewer.viewport.contentSize.x / this.dragonviewer.viewport.contentSize.y);
        } else {
            this.dragonviewer.viewport.visibilityRatio = 1;
        }
        if (this.rotate != 0) {
            this.div_navbox.hide();
            this.options.show_navbox = 0;
            if (this.options.is_mobile == 0) {
                $('.Btnavbox').hide();
            }

            $('.picture_highlight_div').hide();
        } else {
            if (this.options.is_mobile == 0) {
                $('.Btnavbox').show();
            }
        }

        this.apply_contrast();
        this.apply_reverse();
        this.dragonviewer.viewport.applyConstraints();
        this.scale();
    }
}

// UI TRACER
Picture.prototype.init_ui = function () {

// create elements

    if (this.options.direction == 'RTL') {
        this.div_page_next = $('<div class="page_next page_next_rtl"><img src="' + this.options.dir_ico + 'arrow-left.png"></div>');
        this.div_page_prev = $('<div class="page_prev page_prev_rtl"><img src="' + this.options.dir_ico + 'arrow-right.png"></div>');
    } else {
        this.div_page_next = $('<div class="page_next"><img src="' + this.options.dir_ico + 'arrow-right.png"></div>');
        this.div_page_prev = $('<div class="page_prev"><img src="' + this.options.dir_ico + 'arrow-left.png"></div>');
    }

    this.element.append(this.div_page_next);
    this.element.append(this.div_page_prev);
    this.div_picture = $('<div class="picture" id="picture_' + this.uniqkey + '"></div>');
    this.element.append(this.div_picture);
    // nav box
    this.div_navbox = $('<div class="navbox" style="display:none"></div>');
    this.div_navbox.appendTo(this.div_picture);
    this.div_navtoolbar = $('<div class="navtoolbar">');
    this.div_navtoolbar.appendTo(this.div_navbox);
    this.div_navcontent = $('<div class="navcontent">');
    this.div_navcontent.appendTo(this.div_navbox);
    this.div_navcontainer = $('<div  class="navcontainer">');
    this.div_navcontainer.appendTo(this.div_navcontent);
    this.div_navzone = $('<div class="navzone" id="navbox_' + this.uniqkey + '">');
    this.div_navzone.appendTo(this.div_navcontainer);
    this.div_navaction = $('<div class="navaction">');
    this.div_navaction.appendTo(this.div_navcontent);
    this.div_zoomOut = $('<div id="zoomOut_' + this.uniqkey + '" class="zoomOut"><img src="' + this.options.dir_ico + 'zoom-out.png" border="0"></div>');
    this.div_zoomOut.appendTo(this.div_navaction);
    this.div_prc = $('<div class="prc">');
    this.div_prc.appendTo(this.div_navaction);
    this.div_zoomIn = $('<div id="zoomIn_' + this.uniqkey + '" class="zoomIn"><img src="' + this.options.dir_ico + 'zoom-in.png" border="0"></div>');
    this.div_zoomIn.appendTo(this.div_navaction);
    //


    /// hide elements
    if (this.options.is_mobile == 1) {
        this.div_prc.hide();
        this.options.show_navbox == '0';
    }

//scale
    if (this.options.show_scale == 1)
    {
        /*
         /// test point // a finir
         this.div_scale_points = $('<div class="div_scale_points" id="div_scale_points">');
         this.element.append(this.div_scale_points);
         
         this.div_scale_point_A = $('<div class="div_scale_point" id="div_scale_point_A_' + this.uniqkey + '">');
         this.div_scale_point_A.appendTo(this.div_scale_points);
         
         this.div_scale_point_B = $('<div class="div_scale_point" id="div_scale_point_B_' + this.uniqkey + '">');
         this.div_scale_point_B.appendTo(this.div_scale_points)
         ////
         */

        this.div_scale_bigcontainer = $('<div class="navscale_bigcontainer" id="navscale_bigcontainer_' + this.uniqkey + '">');
        this.element.append(this.div_scale_bigcontainer);
        this.div_scale_rotate = $('<div class="navscale_rotate navscale_rotate_h" id="navscale_rotate_' + this.uniqkey + '"><img class="button" title="Rotate" src="' + this.options.dir_ico + 'scale-rotate.png"></div>');
        this.div_scale_rotate.appendTo(this.div_scale_bigcontainer);
        this.div_scale_container = $('<div class="navscale_container navscale_container_h" id="navscale_container_' + this.uniqkey + '">');
        this.div_scale_container.appendTo(this.div_scale_bigcontainer);

        this.div_scale = $('<div class="navscale navscale_h" id="navscale_' + this.uniqkey + '">');
        this.div_scale.appendTo(this.div_scale_container);


        var div_scale_10 = '';
        var navscale_grid_css = '';
        for (i = 0; i < 10; i++) {
            div_scale_10 += '<div class="navscale10h navscale_grid_h' + navscale_grid_css + '" id="navscale10h_' + i + '_' + this.uniqkey + '"></div>';
            div_scale_10 += '<div class="navscale10v navscale_grid_v' + navscale_grid_css + '" id="navscale10v_' + i + '_' + this.uniqkey + '"></div>';

            if (navscale_grid_css == '') {
                navscale_grid_css = '2';
            } else {
                navscale_grid_css = '';
            }
        }

        this.div_scale_10 = $(div_scale_10);
        this.div_scale_10.appendTo(this.div_scale_container);


        this.div_scale_bigcontainer.draggable({containment: ".content_picture", scroll: false});
    }
};


// UI SCALE
Picture.prototype.scale = function () {
    if (this.options.show_scale == 1)
    {
        var u = this.options.scale_unit_num;

        if (this.rotate == '90' || this.rotate == '-90') {
            var current_width = $('.picture > .openseadragon-container > .openseadragon-canvas > div > .grid_' + this.uniqkey).height();
        } else {
            var current_width = $('.picture > .openseadragon-container > .openseadragon-canvas > div > .grid_' + this.uniqkey).width();
        }



        var img_max_width = this.tileSources[this.options.page - 1].width;
        var px_1cm = this.options.scale_px_to_unit * current_width / img_max_width * u;
        if (px_1cm < 30) {
            px_1cm = px_1cm * 10;
            u = u * 10;
        }
        if (px_1cm > this.div_picture.width() && this.scale_rotate == 'h') {
            px_1cm = px_1cm / 10;
            u = u / 10;
        } else if (px_1cm > this.div_picture.height() && this.scale_rotate == 'v') {
            px_1cm = px_1cm / 10;
            u = u / 10;
        }


        var text = u + ' ' + this.options.scale_unit;

        $('.navscale_container').removeClass('navscale_container_v');
        $('.navscale').removeClass('navscale_v');
        $('.navscale_container').removeClass('navscale_container_h');
        $('.navscale').removeClass('navscale_h');
        $('.navscale_rotate').removeClass('navscale_rotate_h');
        if (this.scale_rotate == 'v') {

            $('.navscale10h').hide();
            $('.navscale10v').show();
            $('.navscale_container').addClass('navscale_container_v');
            $('.navscale').addClass('navscale_v');
            this.div_scale.width('auto');
            this.div_scale.animate({height: px_1cm + 'px'}, 200);
            this.div_scale_container.animate({height: px_1cm + 'px'}, 200);
            this.div_scale_bigcontainer.animate({height: px_1cm + 'px'}, 200);
            var new_text = '';
            for (i = 0; i < text.length; i++)
            {
                new_text += '<br>' + text.substr(i, 1);
            }
            text = new_text;

        } else {
            $('.navscale10h').show();
            $('.navscale10v').hide();
            this.div_scale.height('11px');
            this.div_scale_container.height('14px');
            this.div_scale_bigcontainer.height('14px');
            $('.navscale_container').addClass('navscale_container_h');
            $('.navscale').addClass('navscale_h');
            $('.navscale_rotate').addClass('navscale_rotate_h');
            this.div_scale.animate({width: px_1cm + 'px'}, 200);
        }

        this.div_scale.html(text);
        /*
         /// test point // a finir
         var offset_A = this.div_scale_point_A.offset();
         var offset_B = this.div_scale_point_B.offset();
         
         var xs = 0;
         var ys = 0;
         
         xs = offset_B.left - offset_A.left;
         xs = xs * xs;
         
         ys = offset_B.top - offset_A.top;
         ys = ys * ys;
         
         var px_1cm = this.options.scale_px_to_unit * current_width / img_max_width ;
         
         
         console.log(Math.sqrt(xs + ys)/px_1cm);
         ////////
         */
    }
}



Picture.prototype._show_hide_scale = function () {
    if (this.options.show_scale_box == 0)
    {
        this.div_scale_bigcontainer.hide();
        this.options.show_scale_box = 1;
    } else
    {
        this.div_scale_bigcontainer.show();
        this.options.show_scale_box = 0;
    }
}
// UI POSITION
Picture.prototype.position = function () {
    this.div_picture.height(this.options.viewer_height);
    if (this.options.direction == 'RTL') {
        this.div_page_prev.css({top: (this.options.viewer_height - this.options.bt_page_h) / 2, left: this.options.viewer_width - this.options.viewer_menu_left_width - this.options.bt_page_w});
        this.div_page_next.css({top: (this.options.viewer_height - this.options.bt_page_h) / 2});
    } else {
        this.div_page_next.css({top: (this.options.viewer_height - this.options.bt_page_h) / 2, left: this.options.viewer_width - this.options.viewer_menu_left_width - this.options.bt_page_w});
        this.div_page_prev.css({top: (this.options.viewer_height - this.options.bt_page_h) / 2});
    }

};
// PICTURE INITIALISATION
Picture.prototype._initialise = function () {
//
    this._offset();
    if (this.contrast == undefined)
    {
        this.contrast = 0;
    }
    if (this.rotate == undefined)
    {
        this.rotate = 0;
    }
    if (this.reverse == undefined)
    {
        this.reverse = 0;
    }
};
// PICTURE POSITION
Picture.prototype._offset = function () {
// position to picture
    this.offset_parent = this.div_picture.offset();
    this.offset_parent_left = this.offset_parent.left;
    this.offset_parent_top = this.offset_parent.top;
};
// LOAD
Picture.prototype._load = function () {

    var response = this.json_data['tiles'];
    this.tileSources = [];
    this.images_number = 0;
    var that = this;

    $.each(response, function (src, val)
    {

        if (val['@context'] == undefined) {
            that.tileSources[that.images_number] = {
                height: val['height'],
                width: val['width'],
                tileSize: val['tile_size']['w'],
                maxLevel: val['resolutions'],
                getTileUrl: function (level, x, y) {
                    var zoom = (level - val['resolutions']) * -1;
                    if (zoom >= val['resolutions']) {
                        zoom = zoom - 1;
                    }
                    return that.options.server + '/tiles/' + src + '/' + zoom + "/" + x + "/" + y + "." + val['extension'];
                },
                overlays: [
                    {
                        px: 0,
                        py: 0,
                        height: val['height'],
                        width: val['width'],
                        className: 'picture_highlight_div picture_highlight_' + that.uniqkey
                    }, {
                        px: 0,
                        py: 0,
                        height: val['height'],
                        width: val['width'],
                        className: 'grid grid_' + that.uniqkey
                    }],
            }
        } else if (val['@context'] == 'dzi') {


            that.tileSources[that.images_number] =
                    {
                        "Image": {
                            "xmlns": val['xmlns'],
                            "Url": val['Url'],
                            "Format": val['Format'],
                            "Overlap": val['Overlap'],
                            "TileSize": val['TileSize'],
                            "Size": {
                                "Height": val['Size']['Height'],
                                "Width": val['Size']['Width']
                            }
                        }
                    };


        } else {
            that.tileSources[that.images_number] = {
                "@context": val['@context'],
                "@id": val['@id'],
                //"identifier": val['identifier'],
                "height": val['height'],
                "width": val['width'],
                "profile": val['profile'],
                "protocol": val['protocol'],
                // "tilesUrl": val['tilesUrl'],
                "tiles": [{
                        "scaleFactors": val['tiles']['scaleFactors'],
                        "width": val['tiles']['width']
                    }],
                overlays: [
                    {
                        px: 0,
                        py: 0,
                        height: val['height'],
                        width: val['width'],
                        className: 'picture_highlight_div picture_highlight_' + that.uniqkey
                    }, {
                        px: 0,
                        py: 0,
                        height: val['height'],
                        width: val['width'],
                        className: 'grid grid_' + that.uniqkey
                    }],
            };
        }

        that.images_number++;
    });


    /*
     that.tileSources[0] = {
     "@context": "http://iiif.io/api/image/2/context.json",
     "@id": "http://libimages.princeton.edu/loris2/pudl0001%2F4609321%2Fs42%2F00000001.jp2",
     "identifier": "http://libimages.princeton.edu/loris2/pudl0001%2F4609321%2Fs42%2F00000001.jp2",
     "height": 7200,
     "width": 5233,
     "profile": [ "http://library.stanford.edu/iiif/image-api/compliance.html#level1" ],
     "protocol": "http://iiif.io/api/image",
     "tilesUrl":"http://libimages.princeton.edu/loris2/pudl0001%2F4609321%2Fs42%2F00000001.jp2",
     "tiles": [{
     "scaleFactors": [ 1, 2, 4, 8, 16, 32 ],
     "width": 256
     }]
     };
     
     that.tileSources[1] = {
     "@context": "http://iiif.io/api/image/2/context.json",
     "@id": "http://libimages.princeton.edu/loris2/pudl0001%2F4609321%2Fs42%2F00000002.jp2",
     "height": 7200,
     "width": 5093,
     "profile": [ "http://iiif.io/api/image/2/level2.json" ],
     "protocol": "http://iiif.io/api/image",
     "tiles": [{
     "scaleFactors": [ 1, 2, 4, 8, 16, 32 ],
     "width": 256
     }]
     };
     that.tileSources[2] = {
     "@context": "http://iiif.io/api/image/2/context.json",
     "@id": "http://libimages.princeton.edu/loris2/pudl0001%2F4609321%2Fs42%2F00000003.jp2",
     "height": 7200,
     "width": 5387,
     "profile": [ "http://iiif.io/api/image/2/level2.json" ],
     "protocol": "http://iiif.io/api/image",
     "tiles": [{
     "scaleFactors": [ 1, 2, 4, 8, 16, 32 ],
     "width": 256
     }]
     };
     that.tileSources[3] = {
     "@context": "http://iiif.io/api/image/2/context.json",
     "@id": "http://libimages.princeton.edu/loris2/pudl0001%2F4609321%2Fs42%2F00000004.jp2",
     "height": 7200,
     "width": 5434,
     "profile": [ "http://iiif.io/api/image/2/level2.json" ],
     "protocol": "http://iiif.io/api/image",
     "tiles": [{
     "scaleFactors": [ 1, 2, 4, 8, 16, 32 ],
     "width": 256
     }]
     };
     that.images_number=4;
     */

    /* 
     that.tileSources[0] = 
     {
     "Image": {
     "xmlns":"http://schemas.microsoft.com/deepzoom/2008",
     "Url":"http://ufdc.ufl.edu/iipimage/iipsrv.fcgi?DeepZoom=//flvc.fs.osg.ufl.edu/flvc-ufdc/resources/UF/00/00/00/81/00001/00003.jp2_files/",
     "Format":   "jpg", 
     "Overlap":  "2", 
     "TileSize": "256",
     "Size": {
     "Height": "9221",
     "Width":  "7026"
     }
     }
     };
     */

//console.log(that.tileSources);


    this.dragonviewer = OpenSeadragon({
        id: 'picture_' + this.uniqkey,
        prefixUrl: "../assets/images/viewer/",
        zoomInButton: "zoomIn_" + this.uniqkey,
        zoomOutButton: "zoomOut_" + this.uniqkey,
        homeButton: "home",
        fullPageButton: "full-page",
        nextButton: "next",
        previousButton: "previous",
        showNavigator: true,
        navigatorId: "navbox_" + this.uniqkey,
        navigatorSizeRatio: this.options.navbox_ratio,
        tileSources: that.tileSources,
        immediateRender: true,
        visibilityRatio: 1.0,
        constrainDuringPan: true,
        zoomPerClick: 1,
        initialPage: this.options.page - 1
    });
    setTimeout(function () {
//delete overlay href
        $('.picture_highlight_' + that.uniqkey).removeAttr('href');
        $('.grid_' + that.uniqkey).removeAttr('href');
        /*
         /// test point // a finir
         that.svgscale = $('<div class="svgscale" id="svgscale">');
         $('.picture_highlight_' + that.uniqkey).append(that.svgscale);
         
         that.div_scale_point_A.draggable({containment: ".content_picture", scroll: false});
         that.div_scale_point_B.draggable({containment: ".content_picture", scroll: false});
         var paper = Raphael("svgscale");
         
         that.div_scale_point_A.on("drag", function(event, ui) {
         paper.clear();
         var offset_A = that.div_scale_point_A.offset();
         var offset_B = that.div_scale_point_B.offset();
         
         var offset_svgscale = that.svgscale.offset();
         
         
         var offset_A_TOP = offset_A.top - offset_svgscale.top;
         var offset_A_LEFT = offset_A.left - offset_svgscale.left;
         
         var offset_B_TOP = offset_B.top - offset_svgscale.top;
         var offset_B_LEFT = offset_B.left - offset_svgscale.left;
         
         var path = paper.path("M" + offset_A_LEFT + " " + offset_A_TOP + " L" + offset_B_LEFT + " " + offset_B_TOP);
         });
         */
    }, 600);
};
// ZOOM OPTIMIZE
Picture.prototype.zoom_optimize = function () {
    this.dragonviewer.viewport.goHome();
}

// ZOOM IN
Picture.prototype.zoom_in = function () {
    this.dragonviewer.viewport.zoomBy(
            this.dragonviewer.zoomPerClick / 0.5
            );
    this.dragonviewer.viewport.applyConstraints();
}

// ZOOM OUT
Picture.prototype.zoom_out = function () {
    this.dragonviewer.viewport.zoomBy(
            0.5 / this.dragonviewer.zoomPerClick
            );
    this.dragonviewer.viewport.applyConstraints();
}


// PAGE PREV
Picture.prototype._page_prev = function () {
    if (this.options.page > 1)
    {
        var prev = this.options.page - 1;
        this._page_goto(prev);
    }
};
// PAGE NEXT
Picture.prototype._page_next = function () {
    if (this.options.page < this.images_number)
    {
        var next = this.options.page + 1;
        this._page_goto(next);
    }
};
// PAGE GO TO...
Picture.prototype._page_goto = function (page) {

    if (this.can_go_page == 1) {
        this.can_go_page = 0;
        page = Number(page);
        this.bounds = '';
        var that = this;
        if (this.dragonviewer.viewport != null) {
            this.old_zoom_max = this.dragonviewer.viewport.getMaxZoom();
            if ((page < this.options.page_current && this.options.direction == 'LTR') || (page > this.options.page_current && this.options.direction == 'RTL'))
            {
                that.bounds = that.dragonviewer.viewport.getBounds();
                that.dragonviewer.viewport.fitBounds(new OpenSeadragon.Rect(-that.bounds.width - 0.2, that.bounds.y, that.bounds.width, that.bounds.height));
                setTimeout(function () {
                    that.dragonviewer.goToPage(page - 1);
                }, 500);
                that.page_action = 'prev';
            } else
            {
                that.bounds = that.dragonviewer.viewport.getBounds();
                that.dragonviewer.viewport.fitBounds(new OpenSeadragon.Rect(1.2, that.bounds.y, that.bounds.width, that.bounds.height));
                setTimeout(function () {
                    that.dragonviewer.goToPage(page - 1);
                }, 500);
                that.page_action = 'next';
            }
        }
        this.options.page_current = page;
        this.options.page = page;
        that.viewer.set_page(page);
        this._show_prev_next();
    }
    var that = this;
    setTimeout(function () {
        that.can_go_page = 1;
        //delete overlay href
        $('.picture_highlight_' + that.uniqkey).removeAttr('href');
        $('.grid_' + that.uniqkey).removeAttr('href');
    }, 600);
};
// CREATE NAVIGATION WINDOW
Picture.prototype._createNavigationWindow = function () {

    if (this.options.is_mobile == 0)
    {
//div_navzone
        var navzone_w = this.options.navbox_ratio * this.div_picture.width();
        var navzone_h = this.options.navbox_ratio * this.div_picture.height();
        this.div_navbox.width(navzone_w);
        this.div_navtoolbar.width(navzone_w);
        this.div_navzone.width(navzone_w);
        this.div_navzone.height(160);
        var prc_w = navzone_w - 40;
        this.div_prc.width(prc_w);
        var that = this;
        // position

        if (this.NavigationWindow_load == 0)
        {
//var l = this.div_picture.width() - this.div_navbox.width();
            this.div_navbox.css({top: 0, right: 0});
            // position de navbox par rapport au viewer
            that.offset_navbox = this.div_navbox.offset();
            that.offset_navbox_left = that.offset_navbox.left;
            that.offset_navbox_top = that.offset_navbox.top;
            if (this.options.show_navbox == '0')
            {
                this.options.show_navbox = 1;
                this._show_hide_navbox();
            } else
            {
                this.options.show_navbox = 0;
                this._show_hide_navbox();
            }


            this.NavigationWindow_load = 1;
        }
/// drag

        if ($.browser.msie == false) // not ie
        {
            this.div_navbox.draggable(
                    {
                        handle: that.div_navtoolbar, scroll: false,
                        containment: that.div_picture,
                        stop: function ()
                        {
// position de navbox par rapport au viewer
                            that.offset_navbox = that.div_navbox.offset();
                            that.offset_navbox_left = that.offset_navbox.left;
                            that.offset_navbox_top = that.offset_navbox.top;
                        }
                    });
        }
    }
};
// NAVIGATION OPEN
Picture.prototype._open_navbox = function () {
    this.div_navbox.height(this.nav_height + this.toolbox_height + this.toolbox_action_height);
    var that = this;
    setTimeout(function () {
        that.bounds = that.dragonviewer.viewport.getBounds();
        that.dragonviewer.viewport.fitBounds(new OpenSeadragon.Rect(that.bounds.x + .0000001, that.bounds.y, that.bounds.width, that.bounds.height), 1);
    }, 20);
};
// NAVIGATION CLOSE
Picture.prototype._show_hide_navbox = function () {
    if (this.rotate == 0) {

        if (this.options.show_navbox == 1)
        {
            this.div_navbox.hide();
            this.options.show_navbox = 0;
        } else
        {
            this.div_navbox.show();
            this.options.show_navbox = 1;
            this._open_navbox();
        }
    }
};
// HIDE PREV/NEXT BT
Picture.prototype._hide_prev_next = function () {
    if (this.options.is_mobile == 0 && this.options.show_arrow == 0) {
        this.div_page_next.hide();
        this.div_page_prev.hide();
    } else {
        this._show_prev_next();
    }
};
// SHOW PREV/NEXT BT
Picture.prototype._show_prev_next = function () {
    if (this.options.page == 1)
    {
        this.div_page_prev.hide();
    } else
    {
        this.div_page_prev.show();
    }
    if (this.options.page == this.images_number)
    {
        this.div_page_next.hide();
    } else
    {
        this.div_page_next.show();
    }
};
// PRINT ALL
Picture.prototype._print_all = function () {

    var print_img = this.options.image[this.options.page - 1];
    window.open('/viewer/' + this.options.item_id + '/img_print?img=' + print_img + '&CNT=' + this.contrast);
};
// PRINT SCREEN
Picture.prototype._print_screen = function (crop) {
    var print_img = this.options.image[this.options.page - 1];
    var img_max_width = this.tileSources[this.options.page - 1].width;
    var img_max_height = this.tileSources[this.options.page - 1].height;

    this.print_canvas = $('.picture > .openseadragon-container > .openseadragon-canvas > div > canvas');
    this.canvas_width = Number(this.print_canvas.css("width").slice(0, -2));
    this.canvas_height = Number(this.print_canvas.css("height").slice(0, -2));
    this.print_grid = $('.picture > .openseadragon-container > .openseadragon-canvas > div > .grid');
    this.print_top = Number(this.print_grid.css("top").slice(0, -2)) + 1;
    this.print_left = Number(this.print_grid.css("left").slice(0, -2)) + 1;
    this.print_width = Number(this.print_grid.css("width").slice(0, -2));
    this.print_height = Number(this.print_grid.css("height").slice(0, -2));
    var ratio_w = img_max_width / this.print_width;
    var ratio_h = img_max_height / this.print_height;

    if (this.print_width > this.canvas_width) {
        this.print_width = this.canvas_width;
    }
    if (this.print_height > this.canvas_height) {
        this.print_height = this.canvas_height;
    }

    var p_prc = Math.round(this.print_width / img_max_width * 100);
    var p_w = Math.round(this.print_width * ratio_w);
    var p_h = Math.round(this.print_height * ratio_h);
    var p_x = 0;
    if (this.print_left < 0) {
        p_x = Math.round(this.print_left * ratio_w * -1);
    }

    var p_y = 0;
    if (this.print_top < 0) {
        p_y = Math.round(this.print_top * ratio_h * -1);
    }

     var p_x = p_x+(crop.x1*ratio_w);
     var p_y = p_y+(crop.y1*ratio_h);
     var p_w = crop.width*ratio_w;
     var p_h = crop.height*ratio_h;
     

     return print_img + '&crop=' + p_x + ',' + p_y + ',' + p_w + ',' + p_h + '&CNT=' + this.contrast;
};
// PRINT SCREEN
Picture.prototype._crop_screen = function (crop) {
    var print_img = this.tileSources[this.options.page - 1]['@id'];
    var img_max_width = this.tileSources[this.options.page - 1].width;
    var img_max_height = this.tileSources[this.options.page - 1].height;

    this.print_canvas = $('.picture > .openseadragon-container > .openseadragon-canvas > div > canvas');
    this.canvas_width = Number(this.print_canvas.css("width").slice(0, -2));
    this.canvas_height = Number(this.print_canvas.css("height").slice(0, -2));
    this.print_grid = $('.picture > .openseadragon-container > .openseadragon-canvas > div > .grid');
    this.print_top = Number(this.print_grid.css("top").slice(0, -2)) + 1;
    this.print_left = Number(this.print_grid.css("left").slice(0, -2)) + 1;
    this.print_width = Number(this.print_grid.css("width").slice(0, -2));
    this.print_height = Number(this.print_grid.css("height").slice(0, -2));
    var ratio_w = img_max_width / this.print_width;
    var ratio_h = img_max_height / this.print_height;

    if (this.print_width > this.canvas_width) {
        this.print_width = this.canvas_width;
    }
    if (this.print_height > this.canvas_height) {
        this.print_height = this.canvas_height;
    }

    var p_prc = Math.round(this.print_width / img_max_width * 100);
    var p_w = Math.round(this.print_width * ratio_w);
    var p_h = Math.round(this.print_height * ratio_h);
    var p_x = 0;
    if (this.print_left < 0) {
        p_x = Math.round(this.print_left * ratio_w * -1);
    }

    var p_y = 0;
    if (this.print_top < 0) {
        p_y = Math.round(this.print_top * ratio_h * -1);
    }

     var p_x = p_x+(crop.x1*ratio_w);
     var p_y = p_y+(crop.y1*ratio_h);
     var p_w = crop.width*ratio_w;
     var p_h = crop.height*ratio_h;
     

     return print_img + '/' + p_x + ',' + p_y + ',' + p_w + ',' + p_h+'/!'+crop.width+','+crop.height+'/0/default.jpg';
};
