var Thumb = function(element, options, json_data) {
    options = options || {};
    this.element = element;
    this.json_data = json_data;


    // init ui and apps
    this.viewer = null;
    this.picture = null;
    this.ui_bottom = null;

    // params
    this.options = options;
    this.options.viewer_type = options.viewer_type || 'picture';
    this.uniqkey = Math.random() * 0x100000000000000;
    this.thumb_create = 0;

    // action and events
    this.setup_events();
};

// EVENTS TRACKER
Thumb.prototype.setup_events = function() {
    var that = this;

    // change page
    this.element.on('click', '.thumbimg-action', function(e) {
        e.preventDefault();
        var page = $(this).attr('data');

        that.viewer.set_viewer_type(that.options.viewer_type);
        that.timewait = 1200;

        if (that.options.viewer_type == 'picture')
        {
            that.ui_bottom.viewer_control.show();
            that.timewait = 1000;
        }
        else
        {
            that.ui_bottom.viewer_control_page.show();
        }

        that.ui_bottom.hide_elements();

        setTimeout(function()
        {
            that.viewer.set_page(page);
        }, that.timewait);
    });
};

// LINK TO VIEWER
Thumb.prototype.set_viewer = function(viewer) {
    this.viewer = viewer;
}
// LINK TO PICTURE
Thumb.prototype.set_picture = function(picture) {
    this.picture = picture;
}
// LINK TO FLIP
Thumb.prototype.set_flip = function(flip) {
    this.flip = flip;
}
// LINK TO BOTTOM
Thumb.prototype.set_ui_bottom = function(ui_bottom) {
    this.ui_bottom = ui_bottom;
}

// CHANGE PAGE
Thumb.prototype.set_page = function(page) {
    if (this.options.page != page) {
        this.options.page = page;
        this.thumbnail_select();
    }
}

// CHANGE VIEWER TYPE
Thumb.prototype.set_viewer_type = function(viewer) {
    this.options.viewer_type = viewer;
}

// CREATE THUMBNAIL LIST 
Thumb.prototype.create_thumbnail = function() {
    if (this.thumb_create == 0) {
        this.thumb_create = 1;

        this.images_number = 0;
        this.images = new Array();
        this.extension = new Array();

        var response = this.json_data['tiles'];
        var that = this;

        $.each(response, function(src, val)
        {
            if (val['@context'] == undefined) {
                that.images[that.images_number] = src;
                that.extension[that.images_number] = val['extension'];
            }
            else if (val['@context'] == 'dzi') {
                that.images[that.images_number] = val['Url'];
                that.extension[that.images_number] = 'dzi';
            }
            else {
                that.images[that.images_number] = val['@id'];
                that.extension[that.images_number] = 'iiif';
            }
            that.images_number++;
        });

        this.view_thumbnail();
    }
};

// RECREATE THUMBNAIL LIST 
Thumb.prototype.recreate_thumbnail = function() {
    this.thumb_create = 0;
    this.create_thumbnail();
};


// CREATE THUMBNAIL BOX
Thumb.prototype.view_thumbnail = function() {

    var html = '';
    var select = '';
    this.mobile_class = '';
    this.WID = '140';
    this.HEI = '160';

    this.array_thumbs = new Array();
    this.array_thumbs_load = new Array();

    this.content_thumb_w = this.element.width();
    this.thumb_w = 202;
    this.thumb_h = 247;

    if (this.options.is_mobile == 1)
    {
        this.mobile_class = 'thumb_mobile';
        this.WID = '90';
        this.HEI = '110';
        this.thumb_w = 112;
        this.thumb_h = 152;
    }

    this.thumb_by_line = Math.floor(this.content_thumb_w / this.thumb_w);
    this.thumb_line = 0;

    for (var i = 0; i < this.images_number; i++)
    {
        if (i % this.thumb_by_line == 0)
        {
            this.thumb_line++;
        }

        html += '<div class="thumb ' + this.mobile_class + '">';
        html += '<div class="thumbimg ' + select + '" id="thumbimg' + this.uniqkey + '_' + i + '">';
        html += '<a href="#" class="thumbimg-action" data="' + (i + 1) + '">';
        html += '<div id="thumbimg_content' + this.uniqkey + '_' + i + '" style="width:' + this.WID + '; height:' + this.HEI + ';"></div>';
        html += '</a>';
        html += '</div>';
        html += i + 1;
        html += '<div style="clear:both"></div></div>';

        this.array_thumbs[i] = (this.thumb_line - 1) * this.thumb_h;
        this.array_thumbs_load[i] = 0;
    }

    this.element.html(html);

    var that = this;
    this.element.scroll(function()
    {
        clearTimeout($.data(this, 'scrollTimer'));
        $.data(this, 'scrollTimer', setTimeout(function () {
            that.load_thumbnails();
        }, 500));
    });

    this.load_thumbnails();
    this.thumbnail_select();
};

// LOAD THUMBNAIL IN BOX
Thumb.prototype.load_thumbnails = function() {
    var scroll_thumbs = this.element.scrollTop() + this.element.height();
    for (key in this.array_thumbs)
    {
        if (scroll_thumbs > this.array_thumbs[key] && scroll_thumbs < (this.array_thumbs[key] + this.element.height() + this.thumb_h) && this.array_thumbs_load[key] == 0)
        {
            this.array_thumbs_load[key] = 1;


            if (this.extension[key] == 'dzi') {
                $('#thumbimg_content' + this.uniqkey + '_' + key).html('<img src="' + this.images[key] + 'full/full" style="max-width:' + this.WID + 'px; max-height:' + this.HEI + 'px" alt="" border="0"/>');
            }
            else if (this.extension[key] != 'iiif') {
                $('#thumbimg_content' + this.uniqkey + '_' + key).html('<img src="' + this.options.server + '/tiles/' + this.images[key] + '/' + (this.json_data['tiles'][this.images[key]]['resolutions'] - 1) + '/0/0.' + this.extension[key] + '" style="max-width:' + this.WID + 'px; max-height:' + this.HEI + 'px" alt="" border="0"/>');
            }
            else {
                $('#thumbimg_content' + this.uniqkey + '_' + key).html('<img src="' + this.images[key] + '/full/' + this.WID + ',/0/native.jpg" style="max-width:' + this.WID + 'px; max-height:' + this.HEI + 'px" alt="" border="0"/>');
            }

        }
    }
};

Thumb.prototype.thumbnail_select = function() {
    $('.thumbimg').removeClass('current_thumb');
    $('#thumbimg' + this.uniqkey + '_' + (this.options.page - 1)).addClass('current_thumb');

    if (this.options.page > 1)
    {
        this.line_div = Math.floor((this.options.page - 1) / this.thumb_by_line);
        this.element.scrollTop(this.thumb_h * this.line_div);
    }
    else
    {
        this.element.scrollTop(0);
    }
};