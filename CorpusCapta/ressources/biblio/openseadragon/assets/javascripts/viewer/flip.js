var Flip = function(element, options, json_data) {
    options = options || {};
    this.element = element;
    this.json_data = json_data;

    // init ui and apps
    this.viewer = null;
    this.ui_bottom = null;

    // params
    this.options = options;

    this.options.server = options.server || 'yoolib';
    this.options.image = options.image;

    this.options.viewer_height = options.viewer_height || 500;
    this.options.viewer_width = options.viewer_width || 500;
    this.options.bt_page_w = options.bt_page_w || 50;
    this.options.bt_page_h = options.bt_page_h || 100;
    this.options.viewer_menu_left_width = options.viewer_menu_left_width || 0;
    this.options.is_mobile = options.is_mobile || 0;
    this.options.is_tablet = options.is_tablet || 0;
    this.options.page = options.page || 1;
    this.options.dir_ico = options.dir_ico || '/assets/images/viewer/';

    this.can_search = '';
    this.uniqkey = Math.random() * 0x100000000000000;

    if (this.options.flipping != 0) {
        // action and events
        this.setup_events();

        this.init_flip();
        this.position();
        this.flip();
    }
};

// EVENTS TRACKER
Flip.prototype.setup_events = function() {
    var that = this;

    // change page
    this.element.on('dblclick', '.pageflip', function(e) {
        e.preventDefault();
        var page = $(this).attr('data');

        that.viewer.set_page_picture(page);
    });


};

// LINK TO VIEWER
Flip.prototype.set_viewer = function(viewer) {
    this.viewer = viewer;
}
Flip.prototype.set_content_left = function(content_left) {
    this.content_left = content_left;
}

// LINK TO BOTTOM
Flip.prototype.set_ui_bottom = function(ui_bottom) {
    this.ui_bottom = ui_bottom;
}


// RESIZE
Flip.prototype.set_resize = function(w, h, l) {
    this.options.viewer_height = h - 50;
    this.options.viewer_width = w - l;
    this.options.viewer_menu_left_width = l;

    this.position();
    this.flip();
}



// CREATE IMAGES LIST 
Flip.prototype.init_flip = function() {


    this.div_page_next = $('<div class="page_next"><img src="' + this.options.dir_ico + 'arrow-right.png"></div>');
    this.element.append(this.div_page_next);

    this.div_page_prev = $('<div class="page_prev"><img src="' + this.options.dir_ico + 'arrow-left.png"></div>');
    this.element.append(this.div_page_prev);

    this.div_mybook = $('<div class="mybook"></div>');
    this.element.append(this.div_mybook);

    var img_list = '';


    this.images_number = 0;
    var response = this.json_data['tiles'];
    var that = this;
    this.tileSources = [];
    this.loaded = [];
    this.images = new Array();
    this.images_w = new Array();
    this.images_h = new Array();

    this.image_width = false;
    this.image_height = false;
    this.image_ratio = false;




    $.each(response, function(src, val)
    {
        that.images[that.images_number] = src;
        that.images_w[that.images_number] = val['width'];
        that.images_h[that.images_number] = val['height'];
        that.images_number++;


        img_list += '<div id="page_' + that.uniqkey + '_' + that.images_number + '" class="pageflip pageflip_page" data="' + (that.images_number) + '" ></div>';

        that.loaded[that.images_number] = 0;

        if (val['@context'] == undefined) {
            that.tileSources[that.images_number] = {
                height: val['height'],
                width: val['width'],
                tileSize: val['tile_size']['w'],
                maxLevel: val['resolutions'],
                getTileUrl: function(level, x, y) {
                    var zoom = (level - val['resolutions']) * -1;
                    if (zoom >= val['resolutions']) {
                        zoom = zoom - 1;
                    }
                    return that.options.server + '/tiles/' + src + '/' + zoom + "/" + x + "/" + y + "." + val['extension'];
                }
            }
        } else {
            that.tileSources[that.images_number] = {
                "@context": val['@context'],
                "@id": val['@id'],
                //"identifier": val['identifier'],
                "height": val['height'],
                "width": val['width'],
                "profile": val['profile'],
                "protocol": val['protocol'],
                // "tilesUrl": val['tilesUrl'],
                "tiles": [{
                        "scaleFactors": val['tiles']['scaleFactors'],
                        "width": val['tiles']['width']
                    }]
            };
        }

        // beter image size
        if (that.image_ratio == false) {

            if (val['height'] >= val['width']) {
                that.image_ratio = 1;
            }
            else {
                that.image_ratio = 0;
            }

        }
        /*
         if (that.image_ratio == 1) {
         if (that.image_width == false || that.image_width < val['width']) {
         that.image_width = val['width'];
         }
         if (that.image_height == false || that.image_height > val['height']) {
         that.image_height = val['height'];
         }
         } else {
         if (that.image_width == false || that.image_width > val['width']) {
         that.image_width = val['width'];
         }
         if (that.image_height == false || that.image_height < val['height']) {
         that.image_height = val['height'];
         }
         }
         ///
         */
        if (that.image_width == false || that.image_width > val['width']) {
            that.image_width = val['width'];
        }
        if (that.image_height == false || that.image_height > val['height']) {
            that.image_height = val['height'];
        }

    });

    this.div_load = $('<div class="b-load">' + img_list + '</div>');
    this.div_load.appendTo(this.div_mybook);
};

// UI POSITION
Flip.prototype.position = function() {
    this.div_mybook.height(this.options.viewer_height);
    this.div_page_next.css({top: (this.options.viewer_height - this.options.bt_page_h) / 2, left: this.options.viewer_width - this.options.bt_page_w});
    this.div_page_prev.css({top: (this.options.viewer_height - this.options.bt_page_h) / 2});
};



// HIGHLIGHT ALTO
Flip.prototype.highlight_alto = function()
{
    $('.picture_highlight_' + this.uniqkey).html('');

    if (this.options.flipping != 0 && this.content_left.menu_left_visible == true && this.can_search != this.options.page && this.content_left.open_type == 'search') {

        this.can_search = this.options.page;


        var that = this;
        var ipage = this.options.page;
        $.getJSON("/api/altowords",
                {
                    flip: 1,
                    item_id: that.options.item_id,
                    q: that.content_left.search_q,
                    medianame: that.options.image[(ipage - 1)],
                    server: that.options.server,
                },
                function(data)
                {
                    if (data && data.items) {

                        if (that.content_left.menu_left_visible == true && that.content_left.open_type == 'search') {
                            that.draw_alto_boxes(data.items, ipage);
                        }



                        ipage = ipage + 1;
                        if (ipage >= 0 && ipage < that.images_number)
                        {
                            $.getJSON("/api/altowords",
                                    {
                                        item_id: that.options.item_id,
                                        q: that.content_left.search_q,
                                        medianame: that.options.image[(ipage - 1)],
                                        server: that.options.server,
                                    },
                                    function(data)
                                    {
                                        if (data && data.items) {

                                            if (that.content_left.menu_left_visible == true && that.content_left.open_type == 'search') {
                                                that.draw_alto_boxes(data.items, ipage);
                                            }

                                        }
                                    });
                        }



                    }
                });

    }
}

Flip.prototype.draw_alto_boxes = function(items, page) {
    if (this.dragonviewer) {
        var that = this;
        for (var i = 0; i < items.length; i++) {
            var item = items[i];

            var box_prc_height = item.height / this.dragonviewer.viewport.viewer.source.height * 100;
            var box_prc_width = item.width / this.dragonviewer.viewport.viewer.source.width * 100;
            var box_prc_hpos = item.hpos / this.dragonviewer.viewport.viewer.source.width * 100;
            var box_prc_vpos = item.vpos / this.dragonviewer.viewport.viewer.source.height * 100;

            var elt = '';
            elt += '<div class="picture_highlight" style="';
            elt += 'height:' + box_prc_height + '%; ';
            elt += 'width:' + box_prc_width + '%; ';
            elt += 'top:' + box_prc_vpos + '%; ';
            elt += 'left:' + box_prc_hpos + '%; ';
            elt += '">';
            elt += '<div class="picture_content" style="display:none">';
            elt += item.content;
            elt += '</div>';
            elt += '</div>';

            $('.picture_highlight_' + page + '_' + this.uniqkey).append(elt);
        }
    }
};
// REMOVE HIGHLIGHT
Flip.prototype.remove_highlight = function() {
    $('.picture_highlight_' + this.uniqkey).html('');
    this.can_search = '';
}



// CHANGE PAGE
Flip.prototype.set_page = function(page) {


    if (page < 1) {
        page = 1;
    }

    this.options.page = page;

    // this._page_goto(page);
    this.div_mybook.booklet("gotopage", this.options.page);
    this._load_page(page);

    this.remove_highlight();

    var that = this;
    setTimeout(function()
    {
        that.highlight_alto();
    }, 500);

}

// HIDE PREV/NEXT BT
Flip.prototype._hide_prev_next = function() {
    this.div_page_next.hide();
    this.div_page_prev.hide();
};
// SHOW PREV/NEXT BT
Flip.prototype._show_prev_next = function() {
    this.div_page_next.show();
    this.div_page_prev.show();

};


// FLIP
Flip.prototype.flip = function() {
    this.book_width_img = this.image_width;
    this.book_height_img = this.image_height;
    this.ratio_img = this.book_width_img / this.book_height_img;

    this.content_flip_w = this.options.viewer_width - 200;
    this.content_flip_h = this.options.viewer_height - 100;
    this.ratio_flip = this.content_flip_w / this.content_flip_h;

    // plus large que haut
    if (this.ratio_flip > 1.3)
    {
        this.book_height_img = this.content_flip_h;
        this.book_width_img = this.ratio_img * this.book_height_img;
    }
    else
    {
        this.book_width_img = this.content_flip_w / 2;
        this.book_height_img = this.book_width_img / this.ratio_img;
    }

    this.book_width = this.book_width_img * 2;
    this.book_height = this.book_height_img;

    this.div_mybook.css({"top": (this.element.height() - this.book_height) / 2 + 'px'});

    if (this.book_width > this.element.width()) {

        if (this.ratio_flip > 1.3)
        {
            this.book_height = this.element.height() - 100;
            this.book_width = this.ratio_img * this.book_height - 300;
        }

    }


    this.page = this.options.page;

    //page
    var that = this;




    this.div_mybook.booklet({
        closed: true,
        shadows: true,
        autoCenter: true,
        startingPage: this.page,
        width: this.book_width,
        height: this.book_height,
        pagePadding: 0,
        arrows: false,
        pageNumbers: false,
        next: this.div_page_next,
        prev: this.div_page_prev,
        create: function(event, data)
        {
            //   that._load_page(that.options.page);
        },
        start: function(event, data)
        {
            var turn_page = 0;
            if (data.index < turn_page && data.index < 5)
            {
                turn_page = data.index;
            }

            if (data.index > turn_page && data.index > 5)
            {
                turn_page = data.index;
            }
            that._shadow_page(turn_page);
        },
        change: function(event, data)
        {
            if (data.index < 1) {
                data.index = 1;
            }


            that.viewer.set_page(data.index);
        }
    });

    //this.div_mybook.booklet("gotopage", 1);
};
// SHADOW PAGE
Flip.prototype._shadow_page = function(page) {
    if (page < 1)
    {
        this.div_mybook.removeClass("mybook_shadow");
    }
    else if (page >= (this.images_number))
    {
        this.div_mybook.removeClass("mybook_shadow");
    }
    else
    {
        this.div_mybook.addClass("mybook_shadow");
    }
};
// LOAD PAGE
Flip.prototype._load_page = function(page) {

    this._shadow_page(page);

    if (page <= 1)
    {
        this.div_page_prev.hide();
    }
    else
    {
        this.div_page_prev.show();
    }

    if (page >= this.images_number)
    {
        this.div_page_next.hide();
    }
    else
    {
        this.div_page_next.show();
    }


    // 2 pages suivantes
    for (ipage = page; ipage < (page * 1 + 4); ipage++)
    {
        if (ipage >= 0 && ipage < this.images_number && this.loaded[ipage] == 0)
        {
            this.loaded[ipage] = 1;
            var that = this;

            this.dragonviewer = OpenSeadragon({
                id: 'page_' + this.uniqkey + "_" + ipage,
                prefixUrl: "../assets/images/viewer/",
                tileSources: that.tileSources[ipage],
                panHorizontal: false,
                panVertical: false,
                defaultZoomLevel: 1,
                minZoomImageRatio: 1,
                maxZoomPixelRatio: 0,
                visibilityRatio: 1,
                zoomInButton: "none",
                zoomOutButton: "none",
                homeButton: "none",
                fullPageButton: "none",
                nextButton: "none",
                previousButton: "none",
                immediateRender: true,
                overlays: [
                    {
                        px: 0,
                        py: 0,
                        height: that.images_h[ipage],
                        width: that.images_w[ipage],
                        className: 'picture_highlight_div picture_highlight_' + that.uniqkey + ' picture_highlight_' + ipage + '_' + that.uniqkey
                    }]
            });
        }
    }
    // 2 pages precedentes
    for (ipage = page; ipage > (page * 1 - 4); ipage--)
    {
        if (ipage >= 0 && this.loaded[ipage] == 0)
        {
            this.loaded[ipage] = 1;
            var that = this;
            this.dragonviewer = OpenSeadragon({
                id: 'page_' + this.uniqkey + "_" + ipage,
                prefixUrl: "../assets/images/viewer/",
                tileSources: that.tileSources[ipage],
                panHorizontal: false,
                panVertical: false,
                defaultZoomLevel: 1,
                minZoomImageRatio: 1,
                maxZoomPixelRatio: 0,
                visibilityRatio: 1,
                zoomInButton: "none",
                zoomOutButton: "none",
                homeButton: "none",
                fullPageButton: "none",
                nextButton: "none",
                previousButton: "none",
                immediateRender: true,
                overlays: [
                    {
                        px: 0,
                        py: 0,
                        height: that.images_h[ipage],
                        width: that.images_w[ipage],
                        className: 'picture_highlight_div picture_highlight_' + that.uniqkey + ' picture_highlight_' + ipage + '_' + that.uniqkey
                    }]
            });
        }
    }

};
