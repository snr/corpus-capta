<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:tei="http://www.tei-c.org/ns/1.0"
  exclude-result-prefixes="#all"
  version="3.0">

  <xsl:output indent="yes" method="xhtml" encoding="UTF-8" omit-xml-declaration="true" />
  <xsl:preserve-space elements="*"/>

  <xsl:variable name="BASEURL">/PENSE/LesPapiersBarye</xsl:variable>

  <xsl:template match="/">

    <div class="piece-transcription">
      <xsl:apply-templates select="./tei:TEI/tei:text//tei:pb"/>
    </div>

  </xsl:template>

  <!-- template pages (pb) -->

  <xsl:template match="tei:pb" priority="3">
    <!--div class="debut-page text-right">
      <span class="d-inline-block bg-white texte-debut-page">page <xsl:value-of select="@n"/></span>
    </div-->
    <div class="bloc-page-id">
      <span class="texte-page-id">
        <a class="" target="int" href="{$BASEURL}/Corpus/Document/{//tei:teiHeader//tei:publicationStmt//tei:idno}/{@n}">p.<xsl:value-of select="@n"/></a>
      </span>
    </div>
    <div class="bloc-page-transcription bg-white">

      <xsl:variable name="n" select="@n" />
      <xsl:variable name="suivants" select="./following-sibling::*[not(local-name()='pb')][preceding-sibling::tei:pb[1][@n=$n]]" />

      <xsl:if test="count($suivants) = 0">
        <div class="p-3 mt-3 non-transcrit">Aucune transcription</div>
      </xsl:if>

      <xsl:apply-templates select="$suivants"/>
      <xsl:text>&#x0A;</xsl:text>

    </div>
  </xsl:template>

  <!-- templates table, row, cell -->

  <xsl:template match="//*:text/*:body/*:div/*:table" priority="3">
    <table class="table table-bordered tableau-transcription">
      <xsl:apply-templates/>
    </table>
  </xsl:template>

  <xsl:template match="tei:row" priority="3">
    <tr>
      <xsl:apply-templates/>
    </tr>
  </xsl:template>

  <xsl:template match="tei:cell" priority="3">
    <xsl:element name="td">
      <xsl:attribute name="class">p-1</xsl:attribute>
      <xsl:if test="./@cols">
        <xsl:attribute name="colspan"><xsl:value-of select="@cols" /></xsl:attribute>
      </xsl:if>
      <xsl:if test="./@rows">
        <xsl:attribute name="rowspan"><xsl:value-of select="@rows" /></xsl:attribute>
      </xsl:if>
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <!-- template blocs (p, ab, fw...) -->

  <xsl:template match="//*:text/*:body/*:div/*" priority="2">
    <xsl:variable name="region">
      <xsl:choose>
        <xsl:when test="local-name()='fw'"><xsl:value-of select="@type"/></xsl:when>
        <xsl:when test="local-name()='ab'"><xsl:value-of select="@type"/></xsl:when>
        <xsl:when test="local-name()='p'">paragraph</xsl:when>
        <xsl:otherwise><xsl:value-of select="local-name()"/></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <div class="bloc-{local-name()} bloc-region bloc-region-TextRegion bloc-typeregion-{$region}">
      <div class="infos-region">
        <span class="nom-region d-none">TextRegion</span>
        <span class="type-region"><xsl:value-of select="$region"/></span>
      </div>
      <xsl:apply-templates/>
      <xsl:text>&#x0A;</xsl:text>
    </div>
  </xsl:template>

  <!-- template saut de ligne -->

  <xsl:template match="//tei:lb[count(./preceding-sibling::*) > 0]" priority="3">
    <div class="lb sep-ligne"><span class="d-none">- - -</span></div>
    <!--xsl:text>&#x0A;</xsl:text><br class="lb" /><xsl:text>&#x0A;</xsl:text-->
  </xsl:template>

  <xsl:variable name="entites">
      <entry key="persName">personne</entry>
      <entry key="orgName">organisation</entry>
      <entry key="event">événement</entry>
      <entry key="placeName">lieu</entry>
      <entry key="objectName">œuvre</entry>
      <entry key="sum">somme</entry>
      <entry key="Address">adresse</entry>
      <entry key="work">œuvre</entry>
    </xsl:variable>

  <!-- template tags enfants directs de blocs -->

  <xsl:template match="*//*:text/*:body/*:div/*/*" priority="2">

    <xsl:variable name="element" select="local-name()" />

    <xsl:variable name="nomEntite">
      <xsl:choose>
        <xsl:when test="$entites/entry[@key=$element]">
          <xsl:value-of select="$entites/entry[@key=$element]" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$element" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!--xsl:choose>
      <xsl:when test="@key or (@continued and preceding-sibling::*[local-name()='{local-name(.)}']/@key)">
        <xsl:element name="a">
          <xsl:attribute name="href">javascript:void(0);</xsl:attribute>
          <xsl:attribute name="class">tag-textuel tag-popover text-info tag-<xsl:value-of select="local-name()" /></xsl:attribute>
          <xsl:attribute name="data-toggle">popover</xsl:attribute>
          <xsl:attribute name="data-title"><xsl:value-of select="@key" /></xsl:attribute>
          <xsl:attribute name="data-type"><xsl:value-of select="local-name()" /></xsl:attribute>
          <xsl:attribute name="data-wikidata"><xsl:value-of select="@ref-wikidata" /></xsl:attribute>
          <span data-toggle="tooltip" data-placement="bottom" title="{$entites/entry[@key=$element]}" data-test="{local-name()}"><xsl:apply-templates/></span>
        </xsl:element>
      </xsl:when>
      <xsl:otherwise-->
        <xsl:element name="a">
          <xsl:attribute name="href">javascript:void(0);</xsl:attribute>
          <xsl:attribute name="class">tag-textuel text-info tag-<xsl:value-of select="local-name()" /></xsl:attribute>
          <xsl:attribute name="data-toggle">tooltip</xsl:attribute>
          <xsl:attribute name="data-placement">bottom</xsl:attribute>
          <xsl:attribute name="title"><xsl:value-of select="$nomEntite" /></xsl:attribute>
          <xsl:apply-templates/>
        </xsl:element>
      <!--/xsl:otherwise>
    </xsl:choose-->

  </xsl:template>

  <!-- template tags enfants de tags -->

  <xsl:template match="*//*:text/*:body/*:div/*/*/*" priority="2">

    <!--xsl:choose>
      <xsl:when test="@key"-->
        <xsl:element name="span">
          <xsl:attribute name="class">tag-textuel tag-popover text-info tag-<xsl:value-of select="local-name()" /></xsl:attribute>
          <xsl:attribute name="data-toggle">popover</xsl:attribute>
          <xsl:attribute name="title"><xsl:value-of select="@key" /></xsl:attribute>
          <xsl:attribute name="data-type"><xsl:value-of select="local-name()" /></xsl:attribute>
          <xsl:attribute name="data-wikidata"><xsl:value-of select="@ref-wikidata" /></xsl:attribute>
          <xsl:apply-templates/>
        </xsl:element>
      <!--/xsl:when>
      <xsl:when test="not(@key)">
        <xsl:element name="span">
          <xsl:attribute name="class">tag-textuel text-info tag-<xsl:value-of select="local-name()" /></xsl:attribute>
          <xsl:attribute name="title"><xsl:value-of select="local-name()" /></xsl:attribute>
          <xsl:apply-templates/>
        </xsl:element>
      </xsl:when>
    </xsl:choose-->

  </xsl:template>

  <!-- template tags Date -->

  <xsl:template match="tei:date" priority="3">
    <span class="tag-date" data-toggle="tooltip" data-placement="bottom" title="date"><xsl:value-of select="."/></span>
  </xsl:template>

  <!-- template tags Gap -->

  <xsl:template match="tei:gap" priority="3">
    <span class="tag-gap" data-toggle="tooltip" data-placement="bottom" title="espacement"><xsl:value-of select="."/></span>
  </xsl:template>

  <!-- template tags Unclear -->

  <xsl:template match="tei:unclear" priority="3">
    <span class="tag-unclear" data-toggle="tooltip" data-placement="bottom" title="incertain"><xsl:value-of select="replace(.,'xxx','&#160;...&#160;','i')"/></span>
  </xsl:template>

  <!-- template tags Choice -->

  <xsl:template match="tei:choice" priority="3">
    <span class="tag-sic"><xsl:value-of select="./tei:sic"/> <span class="ml-1" style="font-size: 85%;" data-toggle="tooltip" data-placement="bottom" title="correction:{./tei:corr}">(sic)</span></span>
  </xsl:template>

  <!-- template tags textStyle -->

  <xsl:template match="tei:hi" priority="3">
    <xsl:variable name="classes">
      <xsl:for-each select="tokenize(replace(@rend,'\s*(kerning:0;|fontSize:0.0;)\s*',''),';')"><xsl:if test="string-length(.)"><xsl:text> </xsl:text>tag-<xsl:value-of select="replace(.,'(:true|\s)','')"/></xsl:if></xsl:for-each>
    </xsl:variable>
    <span class="tag-textStyle{$classes}" data-toggle="popover" title="{local-name()}"><xsl:apply-templates/></span>
  </xsl:template>

  <!-- template autres -->

  <xsl:template match="*" priority="1">
    <span class="{local-name()}"><xsl:apply-templates/></span>
  </xsl:template>



</xsl:stylesheet>
