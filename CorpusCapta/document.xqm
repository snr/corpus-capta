(:~
 : Page document | Document page
 :
 : @author Jean-Christophe Carius, INHA https://www.inha.fr, License à venir
 :)
module namespace document = 'Corpus-Capta/documents';

import module namespace config = 'Corpus-Capta/config' at 'config.xqm';
import module namespace app = 'Corpus-Capta/app' at 'app.xqm';

(:~
 : Page d'un document | Page of a document
 : @param  $bdd : Nom de la base de donnnée | Database name
 : @param  $document : Nom du document | Name of the document
 : @param  $mode mode d'affichage (ajax ou non) | Display mode (ajax or not)
 : @return HTML
 :)

declare
  %rest:path("CorpusCapta/{$bdd}/document/{$document}")
  %rest:GET
  %rest:query-param("mode", "{$mode}")
  %output:method('html')
  function document:Interface($bdd,$document,$mode) {
    let $tei := collection($bdd)[//*:sourceDesc//*:idno = replace($document,'\.tei\.xml','')]/*:TEI
    return
    if(count($tei) = 0) then 'document introuvable.'||'/'||$bdd||' / '||$document
    else
    (:suivant/precedent:)
    let $ids := for $d at $p in collection($bdd)//*:sourceDesc//*:idno/text()
        order by $d
        return $d
    let $p := index-of($ids,replace($document,'^ARCHIVES_166_','166_'))
    let $suivant := $ids[position()=($p+1)]
    let $precedent := $ids[position()=(number($p)-1)]
    return
    <html lang="fr">
     { app:Entetes() }
     <body>
     <div class="container" style="max-width: 100%;">
      { app:Menu($bdd,'listing') }
      <div class="d-flex justify-content-center"></div>
     </div>
     <div class="container page-document {if($mode = 'ajax') then 'inclus' else ''}" style="max-width: 100%;">
         <div class="row mx-0 titre-document">
           <div class="col border-bottom px-0 pt-2">
             <div class="d-flex align-items-start justify-content-between pb-1">
               <h5 class="m-0 border-left pl-2 mb-1" style="max-width: 50%; font-size: 18px; border-color: #ccc !important; border-width: 2px !important; ">{$tei//*:teiHeader//*:titleStmt//*:title/text()}</h5>
               <div class="d-flex align-items-center menu-document" style="white-space: nowrap;">
                 <a class="text-decoration-none mx-2 ml-4" style="font-size: 120%" href="{$precedent}">←</a>
                 <span style="font-variant: all-small-caps;"> {$tei//*:sourceDesc//*:idno/text()} </span>
                 <a class="text-decoration-none mx-2" style="font-size: 120%" href="{$suivant}">→</a>
               </div>
             </div>
           </div>
       </div>
       <div class="row mx-0 bg-light">
         <div class="col-4" style="">
           <nav class="">
             <div class="nav nav-tabs menu-pages-doc mt-2 border-0 d-flex justify-content-start" style="font-size: 12px;"></div>
           </nav>
           <div class="tab-content bg-white pagination-transcription mb-1" style=""></div>
           <div class="transcription pagination-transcription bloc-transcription bg-light text-center" style="min-height: 300px;  margin: 0 -15px; font-size: 14px;">
             <div class="d-inline-block m-4 p-3 text-left">
             { xslt:transform($tei,$config:Local||"/ressources/xsl/Transcription-Tei-vers-HTML-2.xsl") }
             </div>
           </div>
           <div class="mt-2 mb-4" style="max-width: 70%;">
             <a class="badge bg-white border text-primary p-1 px-2 font-weight-normal" href="{$config:Serveur}/{$bdd}/TEI/{$document}.tei.xml" target="tei">TEI</a>
           </div>
         </div>
         <div class="col-8">
           <div class="numerisation" id="visio" style="background: #262b2e; min-height: 800px; height: 800px;  margin: 0 -15px; border: 10px solid #262b2e; ">
           </div>
         </div>
       </div>
       <div class="data-sources d-none" data-sources="{string-join($tei//*:surface/*:graphic/@url,',')}"></div>
     </div>
     <script src="{$config:Serveur}/ressources/biblio/openseadragon/openseadragon.min.js"></script>
     <script src="{$config:Serveur}/ressources/visionneuse.js"></script>
     <script type="text/javascript">
          var BASEURL, PageDoc, PENSE_BASEURL;
          PENSE_BASEURL = '{$config:Serveur}';
          BASEURL = '{$config:Serveur}';
          PageDoc = 1;
          var Sources = [
            {
              let $images := for $g in $tei//*:surface/*:graphic
                  let $u := replace(data($g/@url),'fileType=thumb','fileType=view')
                  return '{ type: "image", url: "'||$u||'"}'
              return string-join($images,',')
            }
          ];
          $(document).ready(function(){{
            InitVisionneuse(Sources,PageDoc);
            PaginationTranscription(PageDoc);
            InitBulleInfo('body');
          }});
       </script>
       { app:Modal() }
     </body>
  </html>
};

(:~
 : Page XML-TEI d'un document | XML-TEI page of a document
 : @param  $bdd : Nom de la base de donnnée | Database name
 : @param  $document : Nom du document | Name of the document
 : @return XML-TEI
 :)

declare
  %rest:path("CorpusCapta/{$bdd}/tei/{$document}.tei.xml")
  %rest:GET
  %output:method('basex')
  function document:fichierTei($bdd,$document) {
    let $tei := collection($bdd)[//*:sourceDesc//*:idno = replace($document,'\.tei\.xml','')]/*:TEI
    return $tei
};

(:configuration spéciale:)

declare
  %rest:path('/CorpusCapta/LesPapiersBarye/document/{$file=[_\d]+}')
  %rest:GET
  %output:method('html')
  %perm:allow('public')
function document:Redirection1(
  $file
) as item()+ {
  web:redirect($config:Serveur||'/LesPapiersBarye/document/ARCHIVES_'||$file)
};
