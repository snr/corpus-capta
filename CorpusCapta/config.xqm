(:~
 : Configuration file
 :
 : @author Jean-Christophe Carius, INHA https://www.inha.fr, Licence à venir
 :)

module namespace config = 'Corpus-Capta/config';

(:~ : REST's url :)
declare variable $config:REST := request:scheme()||'://'||request:hostname()||':'||request:port();

(:~ : Server's url :)
declare variable $config:Serveur := request:scheme()||'://'||request:hostname()||':'||request:port()||'/CorpusCapta';

(:~ : Server's local path :)
declare variable $config:Local := 'CHEMIN_LOCAL\basex\webapp\CorpusCapta';

(:~ : Transkribus account username :)
declare variable $config:Utilisateur := '';

(:~ : Transkribus account password :)
declare variable $config:MotDePasse := '';

(:~ : Mapping Transkribus collections ids with Basex databases names (optional) :)
declare variable $config:Projets := map {
  'Projet1' : [10001,10002]
};

(:~ : Transkribus username and password for Basex user :)
declare variable $config:Utilisateurs := map {
  'utilisateur' : map { 'u_tkb' : 'compteTranskribus', 'p_tkb' : 'motDePasseTranskribus' }
};

(:~ Session key. :)
declare variable $config:SESSION-KEY := 'corpus-capta';
