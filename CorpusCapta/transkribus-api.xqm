(:~
 : Fonctions d'API de Transkribus | Transkribus API functions
 :
 : @author Jean-Christophe Carius, INHA https://www.inha.fr, ? License
 :)

module namespace transkribus = 'Corpus-Capta/transkribus';

import module namespace config = 'Corpus-Capta/config' at 'config.xqm';
import module namespace compte = 'Corpus-Capta/compte' at 'compte.xqm';

(:~
 : Requête données Transkribus | Transkribus data query
 : @param  $url Adresse API Transkribus | Transkribus API address
 : @return reponse xml
 :)
declare function transkribus:RequeteTranskribus($url as xs:string,$reconnecter as xs:boolean,$utilisateur){
    let $fichier := $config:Local||'/.session-'||$utilisateur
    let $idSession := if(file:exists($fichier)) then file:read-text($fichier)
        else transkribus:ConnecterTranskribus($utilisateur)
    let $connect := http:send-request(
      <http:request method='get'>
        <http:header name="Cookie" value="JSESSIONID={$idSession}" />
      </http:request>,
      $url
    )
    return
        if(data($connect[1]/@status) = '200')
        then <reponse>{$connect[2]}</reponse>
        else if ($reconnecter = true()) then (
          let $idSession := transkribus:ConnecterTranskribus($utilisateur)
          return transkribus:RequeteTranskribus($url,false(),$utilisateur)
        )
        else <reponse>Connexion impossible (2 tentatives) ! </reponse>
};

(:~
 : Connecter Transkribus avec identifiants | Connect to Transkribus with credential
 : @return text Id session
 :)
declare function transkribus:ConnecterTranskribus($compte){
    let $admin := compte:Compte($compte)
    let $connect := http:send-request(
      <http:request method='post'>
        <http:header name="Content-Type" value="application/x-www-form-urlencoded"/>
      </http:request>,
      'https://transkribus.eu/TrpServer/rest/auth/login?user='||$admin/@compte||'&amp;pw='||$admin/@mdp
    )
    let $idSession := $connect//sessionId/text()
    let $enregistrer := file:write($config:Local||'/.session-'||$compte,$idSession)
    return $idSession
};

(:~
 : Reconnecter à Transkribus | Connect again to Transkribus
 : @return page
 :)
declare function transkribus:ReconnecterTranskribus($utilisateur){
    let $connect := transkribus:ConnecterTranskribus($utilisateur)
    return web:redirect($config:Serveur)
};
